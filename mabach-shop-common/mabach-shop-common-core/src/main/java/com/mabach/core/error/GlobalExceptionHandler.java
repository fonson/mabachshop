package com.mabach.core.error;

import com.alibaba.fastjson.JSONObject;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseWeb {
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ResponseWeb exceptionHandler(HttpServletRequest request,Exception e) {
        log.info("URL : {}, error : {}", request.getRequestURL(), e);
        return setResultError(500,"错误:"+e);
    }
}

