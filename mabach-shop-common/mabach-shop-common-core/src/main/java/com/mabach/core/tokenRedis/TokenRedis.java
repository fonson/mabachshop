package com.mabach.core.tokenRedis;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;


@Component
public class TokenRedis {
	@Autowired
	private RedisTemplate redisTemplate;

	/**
	 * 生成令牌
	 * 

	 */
	public String createToken(String keyPrefix, String value) {

		if (StringUtils.isEmpty(value)) {
			new Exception("redisValue Not nul");
		}
		String token = keyPrefix + UUID.randomUUID().toString().replace("-", "");
		redisTemplate.boundValueOps(token).set(value);
		return token;
	}

	/**
	 * 生成令牌
	 * 

	 */
	public String createToken(String keyPrefix, String value, Long time) {
		if (StringUtils.isEmpty(value)) {
			new Exception("redisValue Not nul");
		}
		String token = keyPrefix + UUID.randomUUID().toString().replace("-", "");
		redisTemplate.boundValueOps(token).set(value,time, TimeUnit.SECONDS);
		return token;
	}

	/**
	 * 根据token获取redis中的value值
	 * 
	 * @param token
	 * @return
	 */
	public String getToken(String token) {
		if (StringUtils.isEmpty(token)) {
			return null;
		}

		return (String)redisTemplate.boundValueOps(token).get();
	}

	/**
	 * 移除token
	 * 
	 * @param token
	 * @return
	 */
	public Boolean removeToken(String token) {
		if (StringUtils.isEmpty(token)) {
			return null;
		}
		return redisTemplate.delete(token);

	}

}
