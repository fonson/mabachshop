package com.mabach.responseBase;

import lombok.Data;

import java.io.Serializable;

// 服务接口有响应  统一规范响应服务接口信息
@Data
public class ResponseBase<T> implements Serializable {

	private Integer code;
	private String msg;
	private T data;

	public ResponseBase() {

	}

	public ResponseBase(Integer code, String msg, T data) {

		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	

	public ResponseBase<T> setResultError(Integer code, String msg) {
		return new ResponseBase<T>(code, msg, null);
	}

	// 返回错误，可以传msg
	public ResponseBase<T> setResultError(String msg) {
		return new ResponseBase<T>(500, msg, null);
	}

	// 返回成功，可以传data值
	public ResponseBase<T> setResultSuccess(T data) {
		return new ResponseBase<T>(200, "处理成功", data);
	}

	// 返回成功，沒有data值
	public ResponseBase<T> setResultSuccess() {
		return new ResponseBase<T>(200, "处理成功", null);
	}

	// 返回成功，沒有data值
	public ResponseBase<T> setResultSuccess(String msg) {
		return new ResponseBase<T>(200, msg, null);
	}

	//成功通用封装
	public ResponseBase<T> setResultSuccess(Integer code,String msg,T data) {

		return new ResponseBase<T>(code, msg, data);
	}
	//失败通用封装
	public ResponseBase<T> setResultError(Integer code,String msg,T data) {

		return new ResponseBase<T>(code, msg, data);
	}

	public ResponseBase<T> setResultSuccess(String msg,T data) {

		return new ResponseBase<T>(200, msg, data);
	}

	public ResponseBase<T> setResultError() {

		return new ResponseBase<T>(500, "处理失败", null);
	}


}
