package com.mabach.responseBase;

import lombok.Data;

import java.io.Serializable;

/**
 * 返回前端的消息封装
 */
@Data
public class ResponseWeb implements Serializable {

    private Integer code;
    private String msg;


    public ResponseWeb() {

    }

    public ResponseWeb(Integer code, String msg) {

        this.code = code;
        this.msg = msg;

    }

    //通用返回错误
    public ResponseWeb setResultError(Integer code, String msg) {

        return new ResponseWeb(code, msg);
    }

    // 返回错误，可以传msg
    public ResponseWeb setResultError(String msg) {

        return new ResponseWeb(500,msg);
    }
    //通用返回成功
    public ResponseWeb setResultError() {

        return new ResponseWeb(200,"处理成功");
    }



    // 返回成功
    public ResponseWeb setResultSuccess() {

        return new ResponseWeb(200, "处理成功");
    }

    // 返回成功，可以传msg
    public ResponseWeb setResultSuccess(String msg) {

        return new ResponseWeb(200, msg);
    }

    //通用返回成功封装
    public ResponseWeb setResultSuccess(Integer code,String msg) {

        return new ResponseWeb(code, msg);
    }






}
