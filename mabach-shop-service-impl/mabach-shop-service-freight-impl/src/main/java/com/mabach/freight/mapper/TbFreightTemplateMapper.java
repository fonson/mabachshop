package com.mabach.freight.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.freight.service.entity.TbFreightTemplate;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */
public interface TbFreightTemplateMapper extends BaseMapper<TbFreightTemplate> {

}
