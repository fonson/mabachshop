package com.mabach.freight.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.freight.mapper.TbFreightTemplateMapper;
import com.mabach.freight.service.entity.TbFreightTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FreightTemplateDao {
    @Autowired
    private TbFreightTemplateMapper tbFreightTemplateMapper;

    public PageResult<TbFreightTemplate> findBrandBypage(Integer currentPage, Integer size, TbFreightTemplate tbBrand) throws Exception {
        Page<TbFreightTemplate> page = new Page<>(currentPage, size);
        QueryWrapper<TbFreightTemplate> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbFreightTemplate> tbBrandIPage = tbFreightTemplateMapper.selectPage(page, wrapper);

        PageResult<TbFreightTemplate> pageResult = new PageResult<TbFreightTemplate>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbFreightTemplate tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbFreightTemplate findById(Integer id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbFreightTemplate tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbFreightTemplate> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }
}
