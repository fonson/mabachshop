package com.mabach.freight.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.freight.dao.FreightTemplateDao;
import com.mabach.freight.service.FreightService;
import com.mabach.freight.service.entity.TbFreightTemplate;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FreightServiceImpl extends ResponseBase implements FreightService {

    @Autowired
    private FreightTemplateDao freightTemplateDao;

    @Override
    public ResponseBase<PageResult<TbFreightTemplate>> findBrandBypage(@RequestParam("page")  Integer page,
                                                             @RequestParam("size") Integer size,
                                                                       @RequestBody TbFreightTemplate tbBrand) {

        PageResult<TbFreightTemplate> brandBypage = null;
        try {
            brandBypage = freightTemplateDao.findBrandBypage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (brandBypage==null){
            return setResultError("分页查询为空");
        }

        return setResultSuccess(brandBypage);
    }

    @Override
    public ResponseBase add(@RequestBody TbFreightTemplate tbBrand) {
        boolean add = freightTemplateDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody  TbFreightTemplate tbBrand) {
        boolean update = freightTemplateDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbFreightTemplate> findById(@RequestParam("id") Integer id) {
        TbFreightTemplate byId = freightTemplateDao.findById(id);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  Integer id) {
        boolean delete = freightTemplateDao.delete(id);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbFreightTemplate>> findAll() {
        List<TbFreightTemplate> all = freightTemplateDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }
}
