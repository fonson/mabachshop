package com.mabach.weixin.feign;

import com.mabach.member.service.MemberService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-member")
public interface MemberServiceFeign extends MemberService {
}
