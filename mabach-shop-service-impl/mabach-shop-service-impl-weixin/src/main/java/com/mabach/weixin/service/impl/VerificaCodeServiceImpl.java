package com.mabach.weixin.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.mabach.constants.Constants;
import com.mabach.responseBase.ResponseBase;
import com.mabach.weixin.service.VerificaCodeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
/*
* 微信公众号发送手机号获取验证码
* */
@RestController
public class VerificaCodeServiceImpl extends ResponseBase implements VerificaCodeService {
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public ResponseBase<JSONObject> verificaWeixinCode(@RequestParam("phone") String phone, @RequestParam("weixinCode") String weixinCode) {

        if (StringUtils.isEmpty(phone)){
            return setResultError("手机号不能为空");
        }
        if (StringUtils.isEmpty(weixinCode)){
            return setResultError("验证码不能为空");
        }

        String code = (String) redisTemplate.boundValueOps(Constants.WEIXINCODE_KEY + phone).get();

        if (StringUtils.isEmpty(code)){
            return setResultError("请获取请注册码");
        }

        if (!weixinCode.equals(code)){
            return setResultError("验证码不一致，请重新输入验证码");
            
        }
        return setResultSuccess("验证成功");
    }

}
