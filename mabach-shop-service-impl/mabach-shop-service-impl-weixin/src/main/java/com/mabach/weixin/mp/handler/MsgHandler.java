package com.mabach.weixin.mp.handler;

import com.mabach.constants.Constants;
import com.mabach.core.utils.RegexUtils;
import com.mabach.weixin.feign.MemberServiceFeign;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts.XmlMsgType;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;


import com.mabach.weixin.mp.builder.TextBuilder;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
@Component
@Slf4j
public class MsgHandler extends AbstractHandler {

	@Value("${mabach.weixin.registration.code.message}")
	private String resMsg;
	@Value("${mabach.weixin.default.registration.code.message}")
	private String defaultMsg;

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private MemberServiceFeign memberExistServiceFeign;
	@Override
	public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService weixinService,
			WxSessionManager sessionManager) {
		if (!wxMessage.getMsgType().equals(XmlMsgType.EVENT)) {
			// TODO 可以选择将消息保存到本地
		}

		// 当用户输入关键词如“你好”，“客服”等，并且有客服在线时，把消息转发给在线客服
		try {
			if (StringUtils.startsWithAny(wxMessage.getContent(), "你好", "客服")
					&& weixinService.getKefuService().kfOnlineList().getKfOnlineList().size() > 0) {
				return WxMpXmlOutMessage.TRANSFER_CUSTOMER_SERVICE().fromUser(wxMessage.getToUser())
						.toUser(wxMessage.getFromUser()).build();
			}
		} catch (WxErrorException e) {
			e.printStackTrace();
		}


		// TODO 组装回复消息
        String forContent=wxMessage.getContent();
		String resContent=Constants.WEIXINCODE_KEY+forContent;

        if(RegexUtils.checkPhone(forContent)){
			Object obj = redisTemplate.boundValueOps(resContent).get();

			//判断手机号是否已注册

			if(memberExistServiceFeign.isExist(forContent)){
				return new TextBuilder().build("该手机已注册", wxMessage, weixinService);
			}

			if (obj!=null){
				return new TextBuilder().build("请勿重复提交", wxMessage, weixinService);
			}


			String registCode = registCode()+"";


			redisTemplate.boundValueOps(resContent).
						set(registCode,Constants.WEIXINCODE_TIMEOUT, TimeUnit.SECONDS);


			return new TextBuilder().build(resMsg.replace("%s",registCode), wxMessage, weixinService);


        }


		return new TextBuilder().build(defaultMsg, wxMessage, weixinService);
	}

    private int registCode() {
        int registCode = (int) (Math.random() * 9000 + 1000);
        return registCode;
    }



}
