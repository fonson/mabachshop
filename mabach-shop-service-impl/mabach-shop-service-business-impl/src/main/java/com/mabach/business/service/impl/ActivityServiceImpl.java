package com.mabach.business.service.impl;

import com.mabach.business.dao.ActivityDao;
import com.mabach.business.mapper.TbActivityMapper;
import com.mabach.business.service.ActivityService;
import com.mabach.business.service.entity.TbActivity;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ActivityServiceImpl extends ResponseBase implements ActivityService {

    @Autowired
    private ActivityDao freightTemplateDao;

    @Override
    public ResponseBase<PageResult<TbActivity>> findBrandBypage(@RequestParam("page")  Integer page,
                                                                @RequestParam("size") Integer size,
                                                                @RequestBody TbActivity tbBrand)  {

        PageResult<TbActivity> page1 = null;
        try {
            page1 = freightTemplateDao.findPage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return setResultSuccess(page1);
    }

    @Override
    public ResponseBase add(@RequestBody TbActivity tbBrand) {
        boolean add = freightTemplateDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody  TbActivity tbBrand) {
        boolean update = freightTemplateDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbActivity> findById(@RequestParam("id") Integer id) {
        TbActivity byId = freightTemplateDao.findById(id);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  Integer id) {
        boolean delete = freightTemplateDao.delete(id);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbActivity>> findAll() {
        List<TbActivity> all = freightTemplateDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }
}
