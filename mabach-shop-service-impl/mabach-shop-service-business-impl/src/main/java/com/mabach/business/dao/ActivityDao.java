package com.mabach.business.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.business.mapper.TbActivityMapper;
import com.mabach.business.service.entity.TbActivity;
import com.mabach.common.outputDTO.PageResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ActivityDao {
    @Autowired
    private TbActivityMapper tbFreightTemplateMapper;

    public PageResult<TbActivity> findPage(Integer currentPage, Integer size, TbActivity tbBrand) throws Exception {
        Page<TbActivity> page = new Page<>(currentPage, size);
        QueryWrapper<TbActivity> wrapper = new QueryWrapper<>();

        wrapper.like(tbBrand.getTitle()!=null,"title",tbBrand.getTitle()).
                eq(tbBrand.getStartTime()!=null,"start_time",tbBrand.getStartTime()).
                eq(tbBrand.getEndTime()!=null,"end_time",tbBrand.getEndTime()).
                like(tbBrand.getContent()!=null,"content",tbBrand.getContent()).
                eq(tbBrand.getStatus()!=null,"status",tbBrand.getStatus());
        IPage<TbActivity> tbBrandIPage = tbFreightTemplateMapper.selectPage(page, wrapper);

        PageResult<TbActivity> pageResult = new PageResult<TbActivity>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbActivity tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbActivity findById(Integer id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbActivity tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbActivity> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }
}
