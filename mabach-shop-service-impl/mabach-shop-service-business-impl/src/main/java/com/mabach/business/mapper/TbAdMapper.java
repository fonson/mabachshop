package com.mabach.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.business.service.entity.TbAd;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
public interface TbAdMapper extends BaseMapper<TbAd> {

}
