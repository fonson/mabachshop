package com.mabach.business.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.business.mapper.TbActivityMapper;
import com.mabach.business.mapper.TbAdMapper;
import com.mabach.business.service.entity.TbAd;
import com.mabach.common.outputDTO.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AdDao {
    @Autowired
    private TbAdMapper tbFreightTemplateMapper;

    public PageResult<TbAd> findPage(Integer currentPage, Integer size, TbAd tbBrand) throws Exception {
        Page<TbAd> page = new Page<>(currentPage, size);
        QueryWrapper<TbAd> wrapper = new QueryWrapper<>();

        wrapper.like(tbBrand.getName()!=null,"name",tbBrand.getName()).
                like(tbBrand.getPosition()!=null,"position", tbBrand.getPosition()).
                eq(tbBrand.getStartTime()!=null,"start_time",tbBrand.getStartTime()).
                eq(tbBrand.getEndTime()!=null,"end_time", tbBrand.getEndTime()).
                eq(tbBrand.getStatus()!=null,"status",tbBrand.getStatus()).
                like(tbBrand.getImage()!=null,"image",tbBrand.getImage()).
                like(tbBrand.getUrl()!=null,"url",tbBrand.getUrl()).
                like(tbBrand.getRemarks()!=null,"remarks", tbBrand.getRemarks());

        IPage<TbAd> tbBrandIPage = tbFreightTemplateMapper.selectPage(page,wrapper);

        PageResult<TbAd> pageResult = new PageResult<TbAd>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbAd tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbAd findById(Integer id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbAd tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbAd> findAll(){
        QueryWrapper<TbAd> wrapper = new QueryWrapper<>();
        wrapper.eq("status","1");

        return tbFreightTemplateMapper.selectList(wrapper);
    }
}
