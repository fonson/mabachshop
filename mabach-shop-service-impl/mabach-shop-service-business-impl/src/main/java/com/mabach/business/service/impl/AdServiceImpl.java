package com.mabach.business.service.impl;

import com.mabach.business.dao.AdDao;
import com.mabach.business.service.AdService;
import com.mabach.business.service.entity.TbAd;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class AdServiceImpl extends ResponseBase implements AdService {

    @Autowired
    private AdDao freightTemplateDao;

    @Override
    public ResponseBase<PageResult<TbAd>> findBrandBypage(@RequestParam("page")  Integer page,
                                                          @RequestParam("size") Integer size,
                                                          @RequestBody TbAd tbBrand)  {

        PageResult<TbAd> page1 = null;
        try {
            page1 = freightTemplateDao.findPage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return setResultSuccess(page1);
    }

    @Override
    public ResponseBase add(@RequestBody TbAd tbBrand) {
        boolean add = freightTemplateDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody  TbAd tbBrand) {
        boolean update = freightTemplateDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbAd> findById(@RequestParam("id") Integer id) {
        TbAd byId = freightTemplateDao.findById(id);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  Integer id) {
        boolean delete = freightTemplateDao.delete(id);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbAd>> findAll() {
        List<TbAd> all = freightTemplateDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }
}
