package com.mabach.order.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.order.mapper.TbOrderItemMapper;
import com.mabach.order.service.entity.TbOrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderItemDao {
    @Autowired
    private TbOrderItemMapper tbFreightTemplateMapper;

    public PageResult<TbOrderItem> findBrandBypage(Integer currentPage, Integer size, TbOrderItem tbBrand) throws Exception {
        Page<TbOrderItem> page = new Page<>(currentPage, size);
        QueryWrapper<TbOrderItem> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbOrderItem> tbBrandIPage = tbFreightTemplateMapper.selectPage(page, wrapper);

        PageResult<TbOrderItem> pageResult = new PageResult<TbOrderItem>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbOrderItem tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbOrderItem findById(Long id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbOrderItem tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Long id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbOrderItem> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }
}
