package com.mabach.order.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;

import com.mabach.order.mapper.TbReturnCauseMapper;
import com.mabach.order.service.entity.TbReturnCause;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReturnCauseDao {
    @Autowired
    private TbReturnCauseMapper tbFreightTemplateMapper;

    public PageResult<TbReturnCause> findBrandBypage(Integer currentPage, Integer size, TbReturnCause tbBrand) throws Exception {
        Page<TbReturnCause> page = new Page<>(currentPage, size);
        QueryWrapper<TbReturnCause> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbReturnCause> tbBrandIPage = tbFreightTemplateMapper.selectPage(page, wrapper);

        PageResult<TbReturnCause> pageResult = new PageResult<TbReturnCause>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbReturnCause tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbReturnCause findById(Integer id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbReturnCause tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbReturnCause> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }
}
