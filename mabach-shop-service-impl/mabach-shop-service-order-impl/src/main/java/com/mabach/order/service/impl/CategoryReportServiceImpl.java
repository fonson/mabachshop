package com.mabach.order.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.order.dao.CategoryReportDao;
import com.mabach.order.service.CategoryReportService;
import com.mabach.order.service.entity.TbCategoryReport;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
public class CategoryReportServiceImpl extends ResponseBase implements CategoryReportService {

@Autowired
private CategoryReportDao categoryReportDao;
    @Override
    public ResponseBase<List<TbCategoryReport>> find(@RequestParam("date1") LocalDate date1,
                                                                @RequestParam("date2") LocalDate date2) {

        List<TbCategoryReport> reportList = categoryReportDao.find(date1, date2);
        if (reportList==null){
            return setResultError();
        }
        return setResultSuccess(reportList);
    }
}
