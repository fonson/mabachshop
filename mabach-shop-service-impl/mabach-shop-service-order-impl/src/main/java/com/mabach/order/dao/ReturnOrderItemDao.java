package com.mabach.order.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.order.mapper.TbReturnOrderItemMapper;

import com.mabach.order.service.entity.TbReturnOrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReturnOrderItemDao {
    @Autowired
    private TbReturnOrderItemMapper tbFreightTemplateMapper;

    public PageResult<TbReturnOrderItem> findBrandBypage(Integer currentPage, Integer size, TbReturnOrderItem tbBrand) throws Exception {
        Page<TbReturnOrderItem> page = new Page<>(currentPage, size);
        QueryWrapper<TbReturnOrderItem> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbReturnOrderItem> tbBrandIPage = tbFreightTemplateMapper.selectPage(page, wrapper);

        PageResult<TbReturnOrderItem> pageResult = new PageResult<TbReturnOrderItem>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbReturnOrderItem tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbReturnOrderItem findById(Long id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbReturnOrderItem tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Long id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbReturnOrderItem> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }
}
