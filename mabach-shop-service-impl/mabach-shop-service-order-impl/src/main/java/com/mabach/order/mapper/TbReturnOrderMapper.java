package com.mabach.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.order.service.entity.TbReturnOrder;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
public interface TbReturnOrderMapper extends BaseMapper<TbReturnOrder> {

}
