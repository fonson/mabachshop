package com.mabach.order.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mabach.order.mapper.TbCategoryReportMapper;
import com.mabach.order.service.entity.TbCategoryReport;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;

@Repository
public class CategoryReportDao {

    @Autowired
    private TbCategoryReportMapper tbCategoryReportMapper;

    public List<TbCategoryReport> find(@RequestParam("date1") LocalDate date1,
                                                                @RequestParam("date2") LocalDate date2){
        QueryWrapper<TbCategoryReport> wrapper = new QueryWrapper<>();
        wrapper.between("count_date",date1,date2);
        List<TbCategoryReport> reportList = tbCategoryReportMapper.selectList(wrapper);
        return reportList;
    }
}
