package com.mabach.order.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.order.mapper.TbOrderConfigMapper;
import com.mabach.order.service.entity.TbOrderConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderConfigDao {
    @Autowired
    private TbOrderConfigMapper tbFreightTemplateMapper;

    public PageResult<TbOrderConfig> findBrandBypage(Integer currentPage, Integer size, TbOrderConfig tbBrand) throws Exception {
        Page<TbOrderConfig> page = new Page<>(currentPage, size);
        QueryWrapper<TbOrderConfig> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbOrderConfig> tbBrandIPage = tbFreightTemplateMapper.selectPage(page, wrapper);

        PageResult<TbOrderConfig> pageResult = new PageResult<TbOrderConfig>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbOrderConfig tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbOrderConfig findById(Long id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbOrderConfig tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Long id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbOrderConfig> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }
}
