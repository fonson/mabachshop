package com.mabach.order.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;

import com.mabach.order.mapper.TbReturnOrderMapper;
import com.mabach.order.service.entity.TbReturnOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReturnOrderDao {
    @Autowired
    private TbReturnOrderMapper tbFreightTemplateMapper;

    public PageResult<TbReturnOrder> findBrandBypage(Integer currentPage, Integer size, TbReturnOrder tbBrand) throws Exception {
        Page<TbReturnOrder> page = new Page<>(currentPage, size);
        QueryWrapper<TbReturnOrder> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbReturnOrder> tbBrandIPage = tbFreightTemplateMapper.selectPage(page, wrapper);

        PageResult<TbReturnOrder> pageResult = new PageResult<TbReturnOrder>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbReturnOrder tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbReturnOrder findById(Long id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbReturnOrder tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Long id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbReturnOrder> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }
}
