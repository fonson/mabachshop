package com.mabach.order.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.order.dao.ReturnOrderDao;
import com.mabach.order.service.ReturnOrderService;
import com.mabach.order.service.entity.TbReturnOrder;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ReturnOrderServiceImpl extends ResponseBase implements ReturnOrderService {

    @Autowired
    private ReturnOrderDao freightTemplateDao;

    @Override
    public ResponseBase<PageResult<TbReturnOrder>> findBrandBypage(@RequestParam("page")  Integer page,
                                                                   @RequestParam("size") Integer size,
                                                                   @RequestBody TbReturnOrder tbBrand) {

        PageResult<TbReturnOrder> brandBypage = null;
        try {
            brandBypage = freightTemplateDao.findBrandBypage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (brandBypage==null){
            return setResultError("分页查询为空");
        }

        return setResultSuccess(brandBypage);
    }

    @Override
    public ResponseBase add(@RequestBody TbReturnOrder tbBrand) {
        boolean add = freightTemplateDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody  TbReturnOrder tbBrand) {
        boolean update = freightTemplateDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbReturnOrder> findById(@RequestParam("id") Long id) {
        TbReturnOrder byId = freightTemplateDao.findById(id);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  Long id) {
        boolean delete = freightTemplateDao.delete(id);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbReturnOrder>> findAll() {
        List<TbReturnOrder> all = freightTemplateDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }
}
