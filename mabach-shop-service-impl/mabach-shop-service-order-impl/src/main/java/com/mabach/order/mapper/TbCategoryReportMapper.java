package com.mabach.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.order.service.entity.TbCategoryReport;
import org.springframework.stereotype.Component;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
@Component
public interface TbCategoryReportMapper extends BaseMapper<TbCategoryReport> {

}
