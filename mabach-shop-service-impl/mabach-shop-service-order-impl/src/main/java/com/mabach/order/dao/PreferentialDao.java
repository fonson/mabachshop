package com.mabach.order.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;

import com.mabach.order.mapper.TbPreferentialMapper;
import com.mabach.order.service.entity.TbPreferential;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PreferentialDao {
    @Autowired
    private TbPreferentialMapper tbFreightTemplateMapper;

    public PageResult<TbPreferential> findBrandBypage(Integer currentPage, Integer size, TbPreferential tbBrand) throws Exception {
        Page<TbPreferential> page = new Page<>(currentPage, size);
        QueryWrapper<TbPreferential> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbPreferential> tbBrandIPage = tbFreightTemplateMapper.selectPage(page, wrapper);

        PageResult<TbPreferential> pageResult = new PageResult<TbPreferential>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbPreferential tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbPreferential findById(Integer id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbPreferential tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbPreferential> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }
}
