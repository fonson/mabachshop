package com.mabach.order.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;

import com.mabach.order.mapper.TbOrderLogMapper;
import com.mabach.order.service.entity.TbOrderLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderLogDao {
    @Autowired
    private TbOrderLogMapper tbFreightTemplateMapper;

    public PageResult<TbOrderLog> findBrandBypage(Integer currentPage, Integer size, TbOrderLog tbBrand) throws Exception {
        Page<TbOrderLog> page = new Page<>(currentPage, size);
        QueryWrapper<TbOrderLog> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbOrderLog> tbBrandIPage = tbFreightTemplateMapper.selectPage(page, wrapper);

        PageResult<TbOrderLog> pageResult = new PageResult<TbOrderLog>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbOrderLog tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbOrderLog findById(Long id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbOrderLog tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Long id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbOrderLog> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }
}
