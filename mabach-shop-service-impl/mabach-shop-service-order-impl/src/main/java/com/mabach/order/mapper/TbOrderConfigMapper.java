package com.mabach.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.order.service.entity.TbOrderConfig;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-27
 */
public interface TbOrderConfigMapper extends BaseMapper<TbOrderConfig> {

}
