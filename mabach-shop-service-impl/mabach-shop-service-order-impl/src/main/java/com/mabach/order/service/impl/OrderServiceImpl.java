package com.mabach.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mabach.common.outputDTO.PageResult;

import com.mabach.order.dao.OrderDao;
import com.mabach.order.service.OrderService;
import com.mabach.order.service.entity.TbOrder;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class OrderServiceImpl extends ResponseBase implements OrderService {

    @Autowired
    private OrderDao freightTemplateDao;

    @Override
    public ResponseBase<PageResult<TbOrder>> findBrandBypage(@RequestParam("page")  Integer page,
                                                             @RequestParam("size") Integer size,
                                                             @RequestBody TbOrder tbBrand) {

        PageResult<TbOrder> brandBypage = null;
        try {
            brandBypage = freightTemplateDao.findBrandBypage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (brandBypage==null){
            return setResultError("分页查询为空");
        }

        return setResultSuccess(brandBypage);
    }

    @Override
    public ResponseBase add(@RequestBody TbOrder tbBrand) {
        boolean add = freightTemplateDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update( @RequestBody  TbOrder tbBrand ) {

        tbBrand.setConsignStatus("1");
        tbBrand.setConsignTime(LocalDateTime.now());
        boolean update = freightTemplateDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbOrder> findById(@RequestParam("id") String id) {
        TbOrder byId = freightTemplateDao.findById(id);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  String id) {
        TbOrder tbOrder = new TbOrder();
        tbOrder.setId(id);
        tbOrder.setIsDelete("1");

        boolean delete = freightTemplateDao.update(tbOrder);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbOrder>> findAll() {
        List<TbOrder> all = freightTemplateDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }

    @Override
    @Transactional
    public ResponseBase deliverManyOrder(@RequestBody List<TbOrder> domain) {
        for (TbOrder tbOrder : domain) {
            tbOrder.setConsignStatus("1");
            tbOrder.setConsignTime(LocalDateTime.now());
            boolean b = freightTemplateDao.update(tbOrder);
            if (!b){
                return setResultError();
            }
        }

        return setResultSuccess();
    }
}
