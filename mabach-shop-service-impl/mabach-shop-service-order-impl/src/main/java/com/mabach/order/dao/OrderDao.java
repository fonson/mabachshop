package com.mabach.order.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.order.mapper.TbOrderMapper;
import com.mabach.order.service.entity.TbOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderDao {
    @Autowired
    private TbOrderMapper tbFreightTemplateMapper;

    public PageResult<TbOrder> findBrandBypage(Integer currentPage, Integer size, TbOrder tbBrand) throws Exception {
        Page<TbOrder> page = new Page<>(currentPage, size);
        QueryWrapper<TbOrder> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbOrder> tbBrandIPage = tbFreightTemplateMapper.selectPage(page, wrapper);

        PageResult<TbOrder> pageResult = new PageResult<TbOrder>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbOrder tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbOrder findById(String id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbOrder tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(String id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbOrder> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }
}
