package com.mabach.goods.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mabach.goods.mapper.TbCategoryMapper;
import com.mabach.goods.service.entity.TbCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CategoryDao {
    @Autowired
    private TbCategoryMapper tbCategoryMapper;

    public List<TbCategory> findCategory(TbCategory tbCategory){
        QueryWrapper<TbCategory> wrapper = new QueryWrapper<>();
        wrapper.setEntity(tbCategory);
        return tbCategoryMapper.selectList(wrapper);

    }

    public boolean add(TbCategory tbCategory){
        int i = tbCategoryMapper.insert(tbCategory);
        return i>0?true:false;
    }

    public boolean update(TbCategory tbCategory){
        int i = tbCategoryMapper.updateById(tbCategory);
        return i>0?true:false;
    }

    public TbCategory findById(Integer id){

        return tbCategoryMapper.selectById(id);
    }

    public boolean delete(Integer id){
        int i = tbCategoryMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<Map> findCategoryTree(){
        QueryWrapper<TbCategory> wrapper = new QueryWrapper<>();
        wrapper.eq("is_show","1").orderByAsc("seq");
        List<TbCategory> tbCategories = tbCategoryMapper.selectList(wrapper);
        return findByPid(tbCategories,0);
    }

    public List<Map> findByPid(List<TbCategory> tbCategoryList,Integer pid){
        ArrayList<Map> list = new ArrayList<>();
        for (TbCategory category : tbCategoryList) {
            if (pid.equals(category.getParentId())){
                Map map = new HashMap<>();
                map.put("name",category.getName());
                map.put("url",category.getUrl());
                map.put("menus",findByPid(tbCategoryList,category.getId()));
                list.add(map);
            }

        }
        return list;
    }

}
