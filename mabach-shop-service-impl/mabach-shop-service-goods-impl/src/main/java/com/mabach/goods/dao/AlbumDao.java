package com.mabach.goods.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.mapper.TbAlbumMapper;
import com.mabach.goods.service.entity.TbAlbum;
import com.mabach.goods.service.entity.TbBrand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Repository
public class AlbumDao {
    @Autowired
    private TbAlbumMapper tbAlbumMapper;

    public PageResult<TbAlbum> findByPage(Integer page, Integer size, TbAlbum tbAlbum) {
        Page<TbAlbum> tbAlbumPage = new Page<>(page, size);
        QueryWrapper<TbAlbum> wrapper = new QueryWrapper<>();


        wrapper.setEntity(tbAlbum);

        IPage<TbAlbum> tbAlbumIPage = tbAlbumMapper.selectPage(tbAlbumPage, wrapper);
        PageResult<TbAlbum> pageResult = new PageResult<TbAlbum>();
        pageResult.setRows(tbAlbumIPage.getRecords());
        pageResult.setTotal(tbAlbumIPage.getTotal());
        return pageResult;
    }

    public Boolean add(TbAlbum tbAlbum){
        int i = tbAlbumMapper.insert(tbAlbum);
        return i>0?true:false;
    }

    public boolean update(TbAlbum tbAlbum){
        int i = tbAlbumMapper.updateById(tbAlbum);
        return i>0?true:false;
    }

    public TbAlbum findById(Long id){
        return tbAlbumMapper.selectById(id);
    }
    public boolean delete(Long id){
        int i = tbAlbumMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbAlbum> findAll(){
      return   tbAlbumMapper.selectList(null);
    }
}
