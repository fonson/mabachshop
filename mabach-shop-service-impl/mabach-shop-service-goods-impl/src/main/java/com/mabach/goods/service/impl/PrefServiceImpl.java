package com.mabach.goods.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.dao.PrefDao;
import com.mabach.goods.service.PrefService;
import com.mabach.goods.service.entity.TbPref;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrefServiceImpl extends ResponseBase implements PrefService {
    @Autowired
    private PrefDao prefDao;
    @Override
    public ResponseBase<PageResult<TbPref>> findPage(@RequestParam("page")  Integer page,
                                                     @RequestParam("size") Integer size,
                                                     @RequestBody TbPref tbPref) {
        PageResult<TbPref> pageResult = prefDao.findPage(page, size, tbPref);
        if (pageResult==null){
            return setResultError();
        }
        return setResultSuccess(pageResult);
    }

    @Override
    public ResponseBase add(@RequestBody TbPref tbPref) {
        boolean add = prefDao.add(tbPref);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody TbPref tbPref) {
        boolean add = prefDao.update(tbPref);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbPref> findById(@RequestParam("id")  Integer id) {
        TbPref byId = prefDao.findById(id);
        if (byId==null){
            return setResultError();
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  Integer id) {
        boolean b = prefDao.delete(id);
        if (!b){
            return setResultError();
        }
        return setResultSuccess();
    }
}
