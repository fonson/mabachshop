package com.mabach.goods.dao;




import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.MapObject;
import com.mabach.goods.mapper.TbBrandMapper;
import com.mabach.goods.service.entity.TbBrand;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class BrandDao {
    @Autowired
    private TbBrandMapper tbBrandMapper;

    public PageResult<TbBrand> findBrandBypage(Integer currentPage, Integer size,TbBrand tbBrand) throws Exception {
        Page<TbBrand> page = new Page<>(currentPage, size);
        QueryWrapper<TbBrand> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbBrand> tbBrandIPage = tbBrandMapper.selectPage(page, wrapper);

        PageResult<TbBrand> pageResult = new PageResult<TbBrand>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbBrand tbBrand){
        int i = tbBrandMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbBrand findById(Integer id){

        return tbBrandMapper.selectById(id);
    }

    public boolean update(TbBrand tbBrand){
        int i = tbBrandMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbBrandMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbBrand> findAll(){
        return tbBrandMapper.selectList(null);
    }


}
