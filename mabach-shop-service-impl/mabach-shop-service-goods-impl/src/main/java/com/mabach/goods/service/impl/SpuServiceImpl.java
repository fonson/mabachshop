package com.mabach.goods.service.impl;

import com.alibaba.fastjson.JSON;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.SnowflakeIdWorker;
import com.mabach.goods.dao.CategoryDao;
import com.mabach.goods.dao.SkuDao;
import com.mabach.goods.dao.SpuDao;
import com.mabach.goods.dao.TbCategoryBrandDao;
import com.mabach.goods.service.SpuService;
import com.mabach.goods.service.entity.*;
import com.mabach.responseBase.ResponseBase;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@RestController
public class SpuServiceImpl extends ResponseBase implements SpuService {
    @Autowired
    private SpuDao spuDao;
    @Autowired
    private SkuDao skuDao;
    @Autowired
    private SnowflakeIdWorker snowflakeIdWorker;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private TbCategoryBrandDao tbCategoryBrandDao;

    @Override
    public ResponseBase<PageResult<TbSpu>> findPage(@RequestParam("page")  Integer page,
                                                    @RequestParam("size") Integer size,
                                                    @RequestBody TbSpu tbPref) {
        PageResult<TbSpu> pageResult = spuDao.findPage(page, size, tbPref);
        if (pageResult==null){
            return setResultError();
        }
        return setResultSuccess(pageResult);
    }

    @Override
    public ResponseBase add(@RequestBody TbSpu tbPref) {
        boolean add = spuDao.add(tbPref);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody TbSpu tbPref) {
        boolean add = spuDao.update(tbPref);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbSpu> findById(@RequestParam("id")  String id) {
        TbSpu byId = spuDao.findById(id);
        if (byId==null){
            return setResultError();
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  String id) {
        boolean b = spuDao.delete(id);
        if (!b){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    @Transactional
    public ResponseBase save(@RequestBody  TbGoods tbGoods) {
        TbSpu tbSpu = tbGoods.getTbSpu();
        tbSpu.setId(snowflakeIdWorker.nextId()+"");
        boolean add = spuDao.add(tbSpu);
        if (!add){
            return setResultError();
        }

        TbCategory category = categoryDao.findById(tbSpu.getCategory3Id());

        for (TbSku tbSku : tbGoods.getTbSkuList()) {

            tbSku.setSpuId(tbSpu.getId());
            String name = tbSpu.getName();
            if (StringUtils.isEmpty(tbSku.getSpec())){
                tbSku.setSpec("{}");
            }
            Map<String,String>  map= JSON.parseObject(tbSku.getSpec(), Map.class);
            for (String value : map.values()) {
                name+=" "+value;
            }
            tbSku.setName(name);
            tbSku.setCreateTime(LocalDateTime.now());
            tbSku.setUpdateTime(LocalDateTime.now());
            tbSku.setCategoryId(tbSpu.getCategory3Id());
            tbSku.setCategoryName(category.getName());
            tbSku.setCommentNum(0);
            tbSku.setSaleNum(0);
            boolean add1 = skuDao.add(tbSku);
            if (!add1){
                return setResultError();
            }


        }
        TbCategoryBrand tbCategoryBrand = new TbCategoryBrand();
        tbCategoryBrand.setBrandId(tbSpu.getBrandId());
        tbCategoryBrand.setCategoryId(tbSpu.getCategory3Id());
        if (!tbCategoryBrandDao.selectCount(tbCategoryBrand)){
            tbCategoryBrandDao.add(tbCategoryBrand);
        }

        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbGoods> findGoods(@RequestParam("id")String id) {
//        TbSku tbSku = skuDao.findById(id);
        TbSpu tbSpu = spuDao.findById(id);
        TbSku sku = new TbSku();
        sku.setSpuId(tbSpu.getId());
        List<TbSku> skuList = skuDao.findList(sku);
        TbGoods goods = new TbGoods();
        goods.setTbSpu(tbSpu);
        goods.setTbSkuList(skuList);
        return setResultSuccess(goods);
    }

    @Override
    public ResponseBase<Integer> examineManySpu(@RequestBody List<String> ids) {
        int i = spuDao.examineManySpu(ids);
        if (i<=0){
            return setResultError();
        }
        return setResultSuccess(i);
    }

    @Override
    public ResponseBase<Integer> putManySpu(@RequestBody List<String> ids) {
        int i = spuDao.putManySpu(ids);
        if (i<=0){
            return setResultError();
        }
        return setResultSuccess(i);
    }

    @Override
    public ResponseBase<Integer> pullManySpu(@RequestBody List<String> ids) {
        int i = spuDao.pullManySpu(ids);
        if (i<=0){
            return setResultError();
        }
        return setResultSuccess(i);
    }

}
