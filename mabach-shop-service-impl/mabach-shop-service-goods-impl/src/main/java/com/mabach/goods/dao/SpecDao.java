package com.mabach.goods.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.mapper.TbSpecMapper;
import com.mabach.goods.mapper.TbTemplateMapper;
import com.mabach.goods.service.entity.TbSpec;
import com.mabach.goods.service.entity.TbTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class SpecDao {
    @Autowired
    private TbSpecMapper tbSpecMapper;
    @Autowired
    private TbTemplateMapper tbTemplateMapper;

    public PageResult<TbSpec> findPage(Integer page, Integer size, TbSpec tbPara){
        Page<TbSpec> paraPage = new Page<>(page, size);
        QueryWrapper<TbSpec> wrapper = new QueryWrapper<>();
        wrapper.setEntity(tbPara);
        IPage<TbSpec> tbParaIPage = tbSpecMapper.selectPage(paraPage, wrapper);

        PageResult<TbSpec> pageResult = new PageResult<TbSpec>();
        pageResult.setRows(tbParaIPage.getRecords());
        pageResult.setTotal(tbParaIPage.getTotal());
        return pageResult;
    }

    @Transactional
    public boolean add(TbSpec tbSpec){
        int i = tbSpecMapper.insert(tbSpec);

        TbTemplate tbTemplate = tbTemplateMapper.selectById(tbSpec.getTemplateId());
        tbTemplate.setSpecNum(tbTemplate.getSpecNum()+1);
        tbTemplateMapper.updateById(tbTemplate);
        return i>0?true:false;
    }

    public TbSpec findById(Integer id){
        return tbSpecMapper.selectById(id);
    }

    public boolean update(TbSpec tbPara){
        int i = tbSpecMapper.updateById(tbPara);
        return i>0?true:false;
    }

    @Transactional
    public boolean delete(Integer id){
        TbSpec tbSpec = tbSpecMapper.selectById(id);
        TbTemplate tbTemplate = tbTemplateMapper.selectById(tbSpec.getTemplateId());
        tbTemplate.setSpecNum(tbTemplate.getSpecNum()-1);
        tbTemplateMapper.updateById(tbTemplate);

        int i = tbSpecMapper.deleteById(id);

        return i>0?true:false;
    }

    public List<TbSpec> findList(TbSpec tbSpec){
        QueryWrapper<TbSpec> wrapper = new QueryWrapper<>();
        wrapper.setEntity(tbSpec);
        List<TbSpec> tbSpecList = tbSpecMapper.selectList(wrapper);
        return tbSpecList;
    }
}
