package com.mabach.goods.service.impl;

import com.mabach.goods.dao.CategoryDao;
import com.mabach.goods.dao.TemplateDao;
import com.mabach.goods.service.CategoryService;
import com.mabach.goods.service.entity.TbAlbum;
import com.mabach.goods.service.entity.TbCategory;
import com.mabach.goods.service.entity.TbTemplate;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class CategoryServiceImpl extends ResponseBase implements CategoryService {
    @Autowired
    private CategoryDao categoryDao;

    @Override
    public ResponseBase<List<TbCategory>> findList(@RequestBody  TbCategory tbCategory) {
        List<TbCategory> category = categoryDao.findCategory(tbCategory);
        if (category==null){
            return setResultError();
        }
        return setResultSuccess(category);
    }

    @Override
    public ResponseBase add(@RequestBody  TbCategory tbCategory) {
        boolean add = categoryDao.add(tbCategory);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody TbCategory tbCategory) {
        boolean b = categoryDao.update(tbCategory);
        if (!b){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbCategory> findById(@RequestParam("id") Integer id) {
        TbCategory byId = categoryDao.findById(id);
        if (byId==null){
            return setResultError();
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id") Integer id) {
        boolean b = categoryDao.delete(id);
        if (!b){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<Map>> findCategoryTree() {

        List<Map> categoryTree = categoryDao.findCategoryTree();
        if (categoryTree==null||categoryTree.size()==0){
            return setResultError();
        }
        return setResultSuccess(categoryTree);
    }
}
