package com.mabach.goods.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.dao.AlbumDao;
import com.mabach.goods.service.AlbumService;
import com.mabach.goods.service.entity.TbAlbum;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AlbumServiceImpl extends ResponseBase implements AlbumService {
    @Autowired
    private AlbumDao albumDao;
    @Override
    public ResponseBase<PageResult<TbAlbum>> findByPage(@RequestParam("page") Integer page, @RequestParam("size") Integer size,
                                          @RequestBody TbAlbum tbAlbum) {
        PageResult<TbAlbum> byPage = albumDao.findByPage(page, size, tbAlbum);
        if (byPage==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byPage);
    }

    @Override
    public ResponseBase add(@RequestBody TbAlbum tbAlbum) {
        Boolean add = albumDao.add(tbAlbum);
        if (!add){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody TbAlbum tbAlbum) {
        boolean update = albumDao.update(tbAlbum);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbAlbum> findById(@RequestParam("id") Long id) {
        TbAlbum byId = albumDao.findById(id);
        if (byId==null){
            return setResultError();
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id") Long id) {
        boolean delete = albumDao.delete(id);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbAlbum>> findAllTbAlbum() {
        List<TbAlbum> albumList = albumDao.findAll();
        if (albumList==null){
            return setResultError();
        }
        return setResultSuccess(albumList);
    }


}
