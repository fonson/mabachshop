package com.mabach.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.goods.service.entity.TbCategoryBrand;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-12-01
 */
public interface TbCategoryBrandMapper extends BaseMapper<TbCategoryBrand> {

}
