package com.mabach.goods.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.dao.SkuDao;
import com.mabach.goods.service.SkuService;
import com.mabach.goods.service.entity.TbSku;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SkuServiceImpl extends ResponseBase implements SkuService {
    @Autowired
    private SkuDao skuDao;
    @Override
    public ResponseBase<PageResult<TbSku>> findPage(@RequestParam("page")  Integer page,
                                                     @RequestParam("size") Integer size,
                                                     @RequestBody TbSku tbPref) {
        PageResult<TbSku> pageResult = skuDao.findPage(page, size, tbPref);
        if (pageResult==null){
            return setResultError();
        }
        return setResultSuccess(pageResult);
    }

    @Override
    public ResponseBase add(@RequestBody TbSku tbPref) {
        boolean add = skuDao.add(tbPref);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody TbSku tbPref) {
        boolean add = skuDao.update(tbPref);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbSku> findById(@RequestParam("id")  String id) {
        TbSku byId = skuDao.findById(id);
        if (byId==null){
            return setResultError();
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  String id) {
        boolean b = skuDao.delete(id);
        if (!b){
            return setResultError();
        }
        return setResultSuccess();
    }
}
