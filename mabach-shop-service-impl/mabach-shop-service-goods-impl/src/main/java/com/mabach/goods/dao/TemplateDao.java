package com.mabach.goods.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.mapper.TbTemplateMapper;
import com.mabach.goods.service.entity.TbTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TemplateDao {
    @Autowired
    private TbTemplateMapper tbTemplateMapper;

    public List<TbTemplate> findAll(){
        return  tbTemplateMapper.selectList(null);

    }

    public PageResult<TbTemplate> findPage(Integer page, Integer size, TbTemplate tbPara){
        Page<TbTemplate> paraPage = new Page<>(page, size);
        QueryWrapper<TbTemplate> wrapper = new QueryWrapper<>();
        wrapper.setEntity(tbPara);
        IPage<TbTemplate> tbParaIPage = tbTemplateMapper.selectPage(paraPage, wrapper);

        PageResult<TbTemplate> pageResult = new PageResult<TbTemplate>();
        pageResult.setRows(tbParaIPage.getRecords());
        pageResult.setTotal(tbParaIPage.getTotal());
        return pageResult;
    }

    public boolean add(TbTemplate tbPara){
        int i = tbTemplateMapper.insert(tbPara);
        return i>0?true:false;
    }

    public TbTemplate findById(Integer id){
        return tbTemplateMapper.selectById(id);
    }

    public boolean update(TbTemplate tbPara){
        int i = tbTemplateMapper.updateById(tbPara);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbTemplateMapper.deleteById(id);
        return i>0?true:false;
    }


}
