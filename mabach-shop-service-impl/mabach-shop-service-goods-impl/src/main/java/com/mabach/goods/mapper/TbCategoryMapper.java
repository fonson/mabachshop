package com.mabach.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.goods.service.entity.TbCategory;


/**
 * <p>
 * 商品类目 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-23
 */
public interface TbCategoryMapper extends BaseMapper<TbCategory> {

}
