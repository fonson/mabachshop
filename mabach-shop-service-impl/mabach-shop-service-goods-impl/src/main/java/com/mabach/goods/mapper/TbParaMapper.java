package com.mabach.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.goods.service.entity.TbPara;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-23
 */
public interface TbParaMapper extends BaseMapper<TbPara> {

}
