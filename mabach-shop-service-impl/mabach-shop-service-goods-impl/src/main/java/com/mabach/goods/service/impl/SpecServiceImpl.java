package com.mabach.goods.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.dao.SpecDao;
import com.mabach.goods.service.SpecService;
import com.mabach.goods.service.entity.TbSpec;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SpecServiceImpl extends ResponseBase implements SpecService {
    @Autowired
    private SpecDao specDao;
    @Override
    public ResponseBase<PageResult<TbSpec>> findPage(@RequestParam("page")  Integer page,
                                                    @RequestParam("size") Integer size,
                                                    @RequestBody TbSpec tbPref) {
        PageResult<TbSpec> pageResult = specDao.findPage(page, size, tbPref);
        if (pageResult==null){
            return setResultError();
        }
        return setResultSuccess(pageResult);
    }

    @Override
    public ResponseBase add(@RequestBody TbSpec tbPref) {
        boolean add = specDao.add(tbPref);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody TbSpec tbPref) {
        boolean add = specDao.update(tbPref);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbSpec> findById(@RequestParam("id")  Integer id) {
        TbSpec byId = specDao.findById(id);
        if (byId==null){
            return setResultError();
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  Integer id) {
        boolean b = specDao.delete(id);
        if (!b){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbSpec>> findList(@RequestBody TbSpec tbSpec) {
        List<TbSpec> tbSpecList = specDao.findList(tbSpec);
        if (tbSpecList==null){
            return setResultError();
        }
        return setResultSuccess(tbSpecList);
    }
}
