package com.mabach.goods.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.mapper.TbParaMapper;
import com.mabach.goods.mapper.TbPrefMapper;
import com.mabach.goods.service.entity.TbPref;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PrefDao {

    @Autowired
    private TbPrefMapper tbPrefMapper;

    public PageResult<TbPref> findPage(Integer page, Integer size, TbPref tbPara){
        Page<TbPref> paraPage = new Page<>(page, size);
        QueryWrapper<TbPref> wrapper = new QueryWrapper<>();
        wrapper.setEntity(tbPara);
        IPage<TbPref> tbParaIPage = tbPrefMapper.selectPage(paraPage, wrapper);

        PageResult<TbPref> pageResult = new PageResult<TbPref>();
        pageResult.setRows(tbParaIPage.getRecords());
        pageResult.setTotal(tbParaIPage.getTotal());
        return pageResult;
    }

    public boolean add(TbPref tbPara){
        int i = tbPrefMapper.insert(tbPara);
        return i>0?true:false;
    }

    public TbPref findById(Integer id){
        return tbPrefMapper.selectById(id);
    }

    public boolean update(TbPref tbPara){
        int i = tbPrefMapper.updateById(tbPara);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbPrefMapper.deleteById(id);
        return i>0?true:false;
    }
}
