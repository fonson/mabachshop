package com.mabach.goods.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.mapper.TbSkuMapper;
import com.mabach.goods.mapper.TbSpuMapper;
import com.mabach.goods.service.entity.TbSku;
import com.mabach.goods.service.entity.TbSpu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class SpuDao {
    @Autowired
    private TbSpuMapper tbSpuMapper;
    @Autowired
    private TbSkuMapper tbSkuMapper;
    public PageResult<TbSpu> findPage(Integer page, Integer size, TbSpu tbPara){
        Page<TbSpu> paraPage = new Page<>(page, size);
        QueryWrapper<TbSpu> wrapper = new QueryWrapper<>();
        wrapper.setEntity(tbPara);
        IPage<TbSpu> tbParaIPage = tbSpuMapper.selectPage(paraPage, wrapper);
        for (TbSpu spu : tbParaIPage.getRecords()) {
            TbSku tbSku = new TbSku();
            tbSku.setSpuId(spu.getId());
            QueryWrapper<TbSku> queryWrapper = new QueryWrapper<>();
            queryWrapper.setEntity(tbSku);
            List<TbSku> skuList = tbSkuMapper.selectList(queryWrapper);
            spu.setImage(skuList.get(0).getImage());
        }




        PageResult<TbSpu> pageResult = new PageResult<TbSpu>();
        pageResult.setRows(tbParaIPage.getRecords());
        pageResult.setTotal(tbParaIPage.getTotal());
        return pageResult;
    }

    public boolean add(TbSpu tbPara){
        int i = tbSpuMapper.insert(tbPara);
        return i>0?true:false;
    }

    public TbSpu findById(String id){
        return tbSpuMapper.selectById(id);
    }

    public boolean update(TbSpu tbPara){
        int i = tbSpuMapper.updateById(tbPara);
        return i>0?true:false;
    }
    public boolean delete(String id){
        TbSpu tbSpu = new TbSpu();
        tbSpu.setId(id);
        tbSpu.setIsDelete("1");
        QueryWrapper<TbSpu> wrapper = new QueryWrapper<>();
        wrapper.setEntity(tbSpu);
        int i = tbSpuMapper.updateById(tbSpu);
        return i>0?true:false;
    }
    public int examineManySpu(List<String> ids){
        TbSpu tbSpu = new TbSpu();
        tbSpu.setIsMarketable("1");
        tbSpu.setStatus("1");
        QueryWrapper<TbSpu> wrapper = new QueryWrapper<>();
        wrapper.in("id", ids).eq("status","0")
                .eq("is_delete","0");
        int i = tbSpuMapper.update(tbSpu, wrapper);
        return i;
    }

    public int putManySpu(List<String> ids){
        TbSpu tbSpu = new TbSpu();
        tbSpu.setIsMarketable("1");
        QueryWrapper<TbSpu> wrapper = new QueryWrapper<>();
        wrapper.in("id", ids).eq("status","1")
                .eq("is_delete","0");
        int i = tbSpuMapper.update(tbSpu, wrapper);
        return i;
    }

    public int pullManySpu(List<String> ids){
        TbSpu tbSpu = new TbSpu();
        tbSpu.setIsMarketable("0");
        QueryWrapper<TbSpu> wrapper = new QueryWrapper<>();
        wrapper.in("id", ids).eq("status","1")
                .eq("is_marketable","1")
                .eq("is_delete","0");
        int i = tbSpuMapper.update(tbSpu, wrapper);
        return i;
    }
}
