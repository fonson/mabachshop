package com.mabach.goods.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mabach.goods.mapper.TbCategoryBrandMapper;
import com.mabach.goods.service.entity.TbCategoryBrand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TbCategoryBrandDao {
    @Autowired
    private TbCategoryBrandMapper tbCategoryBrandMapper;

    public boolean add(TbCategoryBrand tbCategoryBrand){
        int insert = tbCategoryBrandMapper.insert(tbCategoryBrand);
        return insert>0?true:false;
    }

    public boolean selectCount(TbCategoryBrand tbCategoryBrand){
        QueryWrapper<TbCategoryBrand> wrapper = new QueryWrapper<>();
        wrapper.setEntity(tbCategoryBrand);
        Integer integer = tbCategoryBrandMapper.selectCount(wrapper);
        return integer>0?true:false;
    }
}
