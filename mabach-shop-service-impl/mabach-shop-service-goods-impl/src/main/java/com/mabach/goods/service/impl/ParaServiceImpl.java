package com.mabach.goods.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.dao.ParaDao;
import com.mabach.goods.service.ParaService;
import com.mabach.goods.service.entity.TbPara;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ParaServiceImpl extends ResponseBase implements ParaService {
    @Autowired
    private ParaDao paraDao;
    @Override
    public ResponseBase<PageResult<TbPara>> findPage(@RequestParam("page") Integer page, @RequestParam("size") Integer size,
                                                     @RequestBody TbPara tbPara) {
        PageResult<TbPara> pageResult = paraDao.findPage(page, size, tbPara);
        if (pageResult==null){
            return setResultError();
        }
        return setResultSuccess(pageResult);
    }

    @Override
    public ResponseBase add(@RequestBody TbPara tbPara) {
        boolean add = paraDao.add(tbPara);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody TbPara tbPara) {
        boolean add = paraDao.update(tbPara);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbPara> findById(@RequestParam("id") Integer id) {
        TbPara byId = paraDao.findById(id);
        if (byId==null){
            return setResultError();
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id") Integer id) {
        boolean b = paraDao.delete(id);
        if (!b){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbPara>> findAllPara() {
        List<TbPara> all = paraDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }
}
