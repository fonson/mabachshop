package com.mabach.goods.dao;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.mapper.TbSkuMapper;
import com.mabach.goods.service.entity.TbSku;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SkuDao {

    @Autowired
    private TbSkuMapper tbSkuMapper;

    public PageResult<TbSku> findPage(Integer page, Integer size, TbSku tbPara){
        Page<TbSku> paraPage = new Page<>(page, size);
        QueryWrapper<TbSku> wrapper = new QueryWrapper<>();
        wrapper.setEntity(tbPara);
        IPage<TbSku> tbParaIPage = tbSkuMapper.selectPage(paraPage, wrapper);

        PageResult<TbSku> pageResult = new PageResult<TbSku>();
        pageResult.setRows(tbParaIPage.getRecords());
        pageResult.setTotal(tbParaIPage.getTotal());
        return pageResult;
    }

    public boolean add(TbSku tbPara){
        int i = tbSkuMapper.insert(tbPara);
        return i>0?true:false;
    }

    public TbSku findById(String id){
        return tbSkuMapper.selectById(id);
    }

    public boolean update(TbSku tbPara){
        int i = tbSkuMapper.updateById(tbPara);
        return i>0?true:false;
    }
    public boolean delete(String id){
        int i = tbSkuMapper.deleteById(id);
        return i>0?true:false;
    }
    public List<TbSku> findList(TbSku tbSku){
        QueryWrapper<TbSku> wrapper = new QueryWrapper<>();
        wrapper.setEntity(tbSku);
        List<TbSku> tbSkuList = tbSkuMapper.selectList(wrapper);
        return tbSkuList;
    }
}
