package com.mabach.goods.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.dao.TemplateDao;
import com.mabach.goods.service.TemplateService;
import com.mabach.goods.service.entity.TbTemplate;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TemplateServiceImpl extends ResponseBase implements TemplateService {
    @Autowired
    private TemplateDao templateDao;
    @Override
    public ResponseBase<List<TbTemplate>> findAll() {
        List<TbTemplate> all = templateDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }

    @Override
    public ResponseBase<PageResult<TbTemplate>> findPageTemplate(@RequestParam("page")  Integer page,
                                                           @RequestParam("size") Integer size,
                                                           @RequestBody TbTemplate tbTemplate) {
        PageResult<TbTemplate> page1 = templateDao.findPage(page, size, tbTemplate);
        if (page1==null){
            return setResultError();
        }
        return setResultSuccess(page1);
    }

    @Override
    public ResponseBase add(@RequestBody TbTemplate tbTemplate) {
        tbTemplate.setParaNum(0);
        tbTemplate.setSpecNum(0);
        boolean b = templateDao.add(tbTemplate);
        if (!b){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody TbTemplate tbTemplate) {
        boolean b = templateDao.update(tbTemplate);
        if (!b){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbTemplate> findById(@RequestParam("id") Integer id) {
        TbTemplate byId = templateDao.findById(id);
        if (byId==null){
            return setResultError();
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id") Integer id) {
        boolean delete = templateDao.delete(id);
        if (!delete){
            return setResultError();
        }
        return setResultSuccess();
    }
}
