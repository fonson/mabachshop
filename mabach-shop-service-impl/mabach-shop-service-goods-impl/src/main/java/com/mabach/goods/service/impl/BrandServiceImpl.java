package com.mabach.goods.service.impl;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.dao.BrandDao;


import com.mabach.goods.service.BrandService;
import com.mabach.goods.service.entity.TbBrand;
import com.mabach.responseBase.ResponseBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BrandServiceImpl extends ResponseBase implements BrandService {
    @Autowired
    private BrandDao brandDao;


    @Override
    public ResponseBase<PageResult<TbBrand>> findBrandBypage(@RequestParam("page")  Integer page,
                                        @RequestParam("size") Integer size,@RequestBody TbBrand tbBrand) {

        PageResult<TbBrand> brandBypage = null;
        try {
            brandBypage = brandDao.findBrandBypage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (brandBypage==null){
            return setResultError("分页查询为空");
        }

        return setResultSuccess(brandBypage);
    }

    @Override
    public ResponseBase add(@RequestBody TbBrand tbBrand) {
        boolean add = brandDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody  TbBrand tbBrand) {
        boolean update = brandDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbBrand> findById(@RequestParam("id") Integer id) {
        TbBrand byId = brandDao.findById(id);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  Integer id) {
        boolean delete = brandDao.delete(id);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbBrand>> findAllBrand() {
        List<TbBrand> all = brandDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }
}



