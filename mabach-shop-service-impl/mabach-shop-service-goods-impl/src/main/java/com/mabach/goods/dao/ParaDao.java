package com.mabach.goods.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.mapper.TbParaMapper;
import com.mabach.goods.mapper.TbTemplateMapper;
import com.mabach.goods.service.entity.TbBrand;
import com.mabach.goods.service.entity.TbPara;
import com.mabach.goods.service.entity.TbTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.lang.reflect.Parameter;
import java.util.List;

@Repository
public class ParaDao {
    @Autowired
    private TbParaMapper tbParaMapper;
    @Autowired
    private TbTemplateMapper tbTemplateMapper;


    public PageResult<TbPara> findPage(Integer page, Integer size, TbPara tbPara){
        Page<TbPara> paraPage = new Page<>(page, size);
        QueryWrapper<TbPara> wrapper = new QueryWrapper<>();
        wrapper.setEntity(tbPara);
        IPage<TbPara> tbParaIPage = tbParaMapper.selectPage(paraPage, wrapper);

        PageResult<TbPara> pageResult = new PageResult<TbPara>();
        pageResult.setRows(tbParaIPage.getRecords());
        pageResult.setTotal(tbParaIPage.getTotal());
        return pageResult;
    }

    public boolean add(TbPara tbPara){
        TbTemplate tbTemplate = tbTemplateMapper.selectById(tbPara.getTemplateId());
        tbTemplate.setParaNum(tbTemplate.getParaNum()+1);
        tbTemplateMapper.updateById(tbTemplate);
        int i = tbParaMapper.insert(tbPara);
        return i>0?true:false;
    }

    public TbPara findById(Integer id){
        return tbParaMapper.selectById(id);
    }

    public boolean update(TbPara tbPara){
        int i = tbParaMapper.updateById(tbPara);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        TbPara tbPara = tbParaMapper.selectById(id);
        TbTemplate tbTemplate = tbTemplateMapper.selectById(tbPara.getTemplateId());
        tbTemplate.setParaNum(tbTemplate.getParaNum()-1);
        tbTemplateMapper.updateById(tbTemplate);
        int i = tbParaMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbPara> findAll(){
        return tbParaMapper.selectList(null);
    }
}
