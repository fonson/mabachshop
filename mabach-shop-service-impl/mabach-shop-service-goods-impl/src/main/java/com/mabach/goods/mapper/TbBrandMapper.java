package com.mabach.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.goods.service.entity.TbBrand;


/**
 * <p>
 * 品牌表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-22
 */

public interface TbBrandMapper extends BaseMapper<TbBrand> {

}
