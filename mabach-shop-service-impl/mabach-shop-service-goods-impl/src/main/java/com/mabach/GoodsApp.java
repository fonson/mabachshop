package com.mabach;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import com.mabach.core.utils.SnowflakeIdWorker;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;


/**
 * 
 * 
 * @description: 会员服务的实现

 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableSwagger2Doc
@MapperScan("com.mabach.goods.mapper")
public class GoodsApp {

	@Bean
	public PaginationInterceptor paginationInterceptor() {
		return new PaginationInterceptor();
	}
	@Bean
	public SnowflakeIdWorker getIdWorker(){
		return new SnowflakeIdWorker(1,1);
	}

	public static void main(String[] args) {
		SpringApplication.run(GoodsApp.class, args);
	}

}
