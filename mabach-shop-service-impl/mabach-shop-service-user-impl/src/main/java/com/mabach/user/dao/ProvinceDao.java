package com.mabach.user.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;


import com.mabach.common.outputDTO.PageResult;
import com.mabach.user.mapper.TbProvincesMapper;
import com.mabach.user.service.entity.TbProvinces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class ProvinceDao {
    @Autowired
    private TbProvincesMapper tbAddressMapper;

    public PageResult<TbProvinces> findBrandBypage(Integer currentPage, Integer size, TbProvinces tbBrand) throws Exception {
        Page<TbProvinces> page = new Page<TbProvinces>(currentPage, size);
        QueryWrapper<TbProvinces> wrapper = new QueryWrapper<>();
//        if (tbBrand.getProvince()==null){
//            wrapper.setEntity(tbBrand);
//            wrapper.like(tbBrand.getProvince()==""||tbBrand.getProvince()==null,"province",tbBrand.getProvince());
//        }else {
            wrapper.like(tbBrand.getProvince()!=null,"province",tbBrand.getProvince());
//        }

        IPage<TbProvinces> tbBrandIPage = tbAddressMapper.selectPage(page,wrapper);

        PageResult<TbProvinces> pageResult = new PageResult<TbProvinces>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbProvinces tbBrand){
        int i = tbAddressMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbProvinces findById(Integer id){

        return tbAddressMapper.selectById(id);
    }

    public boolean update(TbProvinces tbBrand){
        int i = tbAddressMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbAddressMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbProvinces> findAll(){
        return tbAddressMapper.selectList(null);
    }
}
