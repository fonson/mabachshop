package com.mabach.user.service.impl;

import com.mabach.common.outputDTO.PageResult;

import com.mabach.responseBase.ResponseBase;
import com.mabach.user.dao.AddressDao;
import com.mabach.user.service.AddressService;
import com.mabach.user.service.entity.TbAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AddressServiceImpl extends ResponseBase implements AddressService {

    @Autowired
    private AddressDao addressDao;

    @Override
    public ResponseBase<PageResult<TbAddress>> findBrandBypage(@RequestParam("page")  Integer page,
                                                               @RequestParam("size") Integer size,
                                                               @RequestBody TbAddress tbBrand) {

        PageResult<TbAddress> brandBypage = null;
        try {
            brandBypage = addressDao.findBrandBypage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (brandBypage==null){
            return setResultError("分页查询为空");
        }

        return setResultSuccess(brandBypage);
    }

    @Override
    public ResponseBase add(@RequestBody TbAddress tbBrand) {
        boolean add = addressDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody  TbAddress tbBrand) {
        boolean update = addressDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbAddress> findById(@RequestParam("id") Integer id) {
        TbAddress byId = addressDao.findById(id);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  Integer id) {
        boolean delete = addressDao.delete(id);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbAddress>> findAll() {
        List<TbAddress> all = addressDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }
}
