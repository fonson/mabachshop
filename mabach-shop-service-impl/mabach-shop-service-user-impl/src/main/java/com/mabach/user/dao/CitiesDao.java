package com.mabach.user.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.user.mapper.TbAreasMapper;
import com.mabach.user.mapper.TbCitiesMapper;
import com.mabach.user.service.entity.TbCities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CitiesDao {
    @Autowired
    private TbCitiesMapper tbAddressMapper;

    public PageResult<TbCities> findBrandBypage(Integer currentPage, Integer size, TbCities tbBrand) throws Exception {
        Page<TbCities> page = new Page<>(currentPage, size);
        QueryWrapper<TbCities> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbCities> tbBrandIPage = tbAddressMapper.selectPage(page, wrapper);

        PageResult<TbCities> pageResult = new PageResult<TbCities>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbCities tbBrand){
        int i = tbAddressMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbCities findById(Integer id){

        return tbAddressMapper.selectById(id);
    }

    public boolean update(TbCities tbBrand){
        int i = tbAddressMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbAddressMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbCities> findAll(){
        return tbAddressMapper.selectList(null);
    }
}