package com.mabach.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.user.service.entity.TbUser;


/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */
public interface TbUserMapper extends BaseMapper<TbUser> {

}
