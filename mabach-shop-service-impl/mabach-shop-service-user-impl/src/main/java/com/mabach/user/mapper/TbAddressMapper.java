package com.mabach.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.user.service.entity.TbAddress;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */
public interface TbAddressMapper extends BaseMapper<TbAddress> {

}
