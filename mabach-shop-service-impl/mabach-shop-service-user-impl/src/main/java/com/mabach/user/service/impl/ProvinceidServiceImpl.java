package com.mabach.user.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.user.dao.ProvinceDao;
import com.mabach.user.service.ProvinceService;
import com.mabach.user.service.entity.TbProvinces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class ProvinceidServiceImpl extends ResponseBase implements ProvinceService {

    @Autowired
    private ProvinceDao addressDao;

    @Override
    public ResponseBase<PageResult<TbProvinces>> findBrandBypage(@RequestParam("page")  Integer page,
                                                                 @RequestParam("size") Integer size,
                                                                 @RequestBody TbProvinces tbBrand) {

        PageResult<TbProvinces> brandBypage = null;
        try {
            brandBypage = addressDao.findBrandBypage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (brandBypage==null){
            return setResultError("分页查询为空");
        }

        return setResultSuccess(brandBypage);
    }

    @Override
    public ResponseBase add(@RequestBody TbProvinces tbBrand) {
        boolean add = addressDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody  TbProvinces tbBrand) {
        boolean update = addressDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbProvinces> findById(@RequestParam("provinceid") Integer provinceid) {
        TbProvinces byId = addressDao.findById(provinceid);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("provinceid")  Integer provinceid) {
        boolean delete = addressDao.delete(provinceid);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbProvinces>> findAll() {
        List<TbProvinces> all = addressDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }
}
