package com.mabach.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.user.service.entity.TbCities;


/**
 * <p>
 * 行政区域地州市信息表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */
public interface TbCitiesMapper extends BaseMapper<TbCities> {

}
