package com.mabach.user.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.user.mapper.TbUserMapper;
import com.mabach.user.service.entity.TbUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class UserDao {
    @Autowired
    private TbUserMapper tbAddressMapper;

    public PageResult<TbUser> findBrandBypage(Integer currentPage, Integer size, TbUser tbBrand) throws Exception {
        Page<TbUser> page = new Page<>(currentPage, size);
        QueryWrapper<TbUser> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbUser> tbBrandIPage = tbAddressMapper.selectPage(page, wrapper);

        PageResult<TbUser> pageResult = new PageResult<TbUser>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbUser tbBrand){
        int i = tbAddressMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbUser findById(String username){
        QueryWrapper<TbUser> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);

        return tbAddressMapper.selectOne(wrapper);
    }

    public boolean update(TbUser tbBrand){
        int i = tbAddressMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(String username){
        QueryWrapper<TbUser> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        int i = tbAddressMapper.delete(wrapper);
        return i>0?true:false;
    }

    public List<TbUser> findAll(){
        return tbAddressMapper.selectList(null);
    }
}
