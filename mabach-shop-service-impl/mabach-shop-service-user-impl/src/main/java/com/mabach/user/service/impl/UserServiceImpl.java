package com.mabach.user.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.user.dao.UserDao;
import com.mabach.user.service.UserService;
import com.mabach.user.service.entity.TbUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class UserServiceImpl extends ResponseBase implements UserService {

    @Autowired
    private UserDao addressDao;

    @Override
    public ResponseBase<PageResult<TbUser>> findBrandBypage(@RequestParam("page")  Integer page,
                                                            @RequestParam("size") Integer size,
                                                            @RequestBody TbUser tbBrand) {

        PageResult<TbUser> brandBypage = null;
        try {
            brandBypage = addressDao.findBrandBypage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (brandBypage==null){
            return setResultError("分页查询为空");
        }

        return setResultSuccess(brandBypage);
    }

    @Override
    public ResponseBase add(@RequestBody TbUser tbBrand) {
        boolean add = addressDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody  TbUser tbBrand) {
        boolean update = addressDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbUser> findById(@RequestParam("username") String username) {
        TbUser byId = addressDao.findById(username);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("username")  String username) {
        boolean delete = addressDao.delete(username);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbUser>> findAll() {
        List<TbUser> all = addressDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }
}