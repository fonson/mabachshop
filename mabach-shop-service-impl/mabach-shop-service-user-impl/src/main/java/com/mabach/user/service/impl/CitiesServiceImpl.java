package com.mabach.user.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.user.dao.CitiesDao;
import com.mabach.user.service.AreasService;
import com.mabach.user.service.CitiesService;
import com.mabach.user.service.entity.TbCities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;



@RestController
public class CitiesServiceImpl extends ResponseBase implements CitiesService {

    @Autowired
    private CitiesDao addressDao;

    @Override
    public ResponseBase<PageResult<TbCities>> findBrandBypage(@RequestParam("page")  Integer page,
                                                              @RequestParam("size") Integer size,
                                                              @RequestBody TbCities tbBrand) {

        PageResult<TbCities> brandBypage = null;
        try {
            brandBypage = addressDao.findBrandBypage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (brandBypage==null){
            return setResultError("分页查询为空");
        }

        return setResultSuccess(brandBypage);
    }

    @Override
    public ResponseBase add(@RequestBody TbCities tbBrand) {
        boolean add = addressDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody  TbCities tbBrand) {
        boolean update = addressDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbCities> findById(@RequestParam("cityid") Integer cityid) {
        TbCities byId = addressDao.findById(cityid);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("cityid")  Integer cityid) {
        boolean delete = addressDao.delete(cityid);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbCities>> findAll() {
        List<TbCities> all = addressDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }
}

