package com.mabach.user.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.user.dao.AreasDao;
import com.mabach.user.service.AreasService;
import com.mabach.user.service.entity.TbAreas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class AreasServiceImpl extends ResponseBase implements AreasService {

    @Autowired
    private AreasDao addressDao;

    @Override
    public ResponseBase<PageResult<TbAreas>> findBrandBypage(@RequestParam("page")  Integer page,
                                                             @RequestParam("size") Integer size,
                                                             @RequestBody TbAreas tbBrand) {

        PageResult<TbAreas> brandBypage = null;
        try {
            brandBypage = addressDao.findBrandBypage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (brandBypage==null){
            return setResultError("分页查询为空");
        }

        return setResultSuccess(brandBypage);
    }

    @Override
    public ResponseBase add(@RequestBody TbAreas tbBrand) {
        boolean add = addressDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody  TbAreas tbBrand) {
        boolean update = addressDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbAreas> findById(@RequestParam("areaid") Integer areaid) {
        TbAreas byId = addressDao.findById(areaid);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("areaid")  Integer areaid) {
        boolean delete = addressDao.delete(areaid);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbAreas>> findAll() {
        List<TbAreas> all = addressDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }
}
