package com.mabach.user.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;

import com.mabach.user.mapper.TbAreasMapper;

import com.mabach.user.service.entity.TbAreas;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class AreasDao {
    @Autowired
    private TbAreasMapper tbAddressMapper;

    public PageResult<TbAreas> findBrandBypage(Integer currentPage, Integer size, TbAreas tbBrand) throws Exception {
        Page<TbAreas> page = new Page<>(currentPage, size);
        QueryWrapper<TbAreas> wrapper = new QueryWrapper<>();

        wrapper.eq(tbBrand.getCityid()!=null,"cityid",tbBrand.getCityid()).
        like(StringUtils.isNotEmpty(tbBrand.getArea()),"area",tbBrand.getArea());

        IPage<TbAreas> tbBrandIPage = tbAddressMapper.selectPage(page, wrapper);

        PageResult<TbAreas> pageResult = new PageResult<TbAreas>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbAreas tbBrand){
        int i = tbAddressMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbAreas findById(Integer id){

        return tbAddressMapper.selectById(id);
    }

    public boolean update(TbAreas tbBrand){
        int i = tbAddressMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbAddressMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbAreas> findAll(){
        return tbAddressMapper.selectList(null);
    }
}
