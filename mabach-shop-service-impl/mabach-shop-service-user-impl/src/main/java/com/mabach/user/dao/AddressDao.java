package com.mabach.user.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;

import com.mabach.user.mapper.TbAddressMapper;
import com.mabach.user.service.entity.TbAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AddressDao {
    @Autowired
    private TbAddressMapper tbAddressMapper;

    public PageResult<TbAddress> findBrandBypage(Integer currentPage, Integer size, TbAddress tbBrand) throws Exception {
        Page<TbAddress> page = new Page<>(currentPage, size);
        QueryWrapper<TbAddress> wrapper = new QueryWrapper<>();

        wrapper.setEntity(tbBrand);
        IPage<TbAddress> tbBrandIPage = tbAddressMapper.selectPage(page, wrapper);

        PageResult<TbAddress> pageResult = new PageResult<TbAddress>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbAddress tbBrand){
        int i = tbAddressMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbAddress findById(Integer id){

        return tbAddressMapper.selectById(id);
    }

    public boolean update(TbAddress tbBrand){
        int i = tbAddressMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbAddressMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbAddress> findAll(){
        return tbAddressMapper.selectList(null);
    }
}
