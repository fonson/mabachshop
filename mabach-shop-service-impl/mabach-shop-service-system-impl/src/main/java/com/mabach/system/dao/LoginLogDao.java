package com.mabach.system.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.service.system.entity.TbLoginLog;
import com.mabach.system.mapper.TbLoginLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


    @Repository
    public class LoginLogDao {
        @Autowired
        private TbLoginLogMapper tbFreightTemplateMapper;

        public PageResult<TbLoginLog> findPage(Integer currentPage, Integer size) throws Exception {
            Page<TbLoginLog> page = new Page<>(currentPage, size);


            IPage<TbLoginLog> tbBrandIPage = tbFreightTemplateMapper.selectPage(page,null);

            PageResult<TbLoginLog> pageResult = new PageResult<TbLoginLog>();
            pageResult.setRows(tbBrandIPage.getRecords());
            pageResult.setTotal(tbBrandIPage.getTotal());


            return pageResult;
        }


    }