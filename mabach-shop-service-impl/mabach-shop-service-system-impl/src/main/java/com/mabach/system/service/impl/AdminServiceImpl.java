package com.mabach.system.service.impl;


import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.service.system.AdminService;
import com.mabach.service.system.entity.TbAdmin;
import com.mabach.system.dao.AdminDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class AdminServiceImpl extends ResponseBase implements AdminService {

    @Autowired
    private AdminDao freightTemplateDao;

    @Override
    public ResponseBase<PageResult<TbAdmin>> findBrandBypage(@RequestParam("page")  Integer page,
                                                             @RequestParam("size") Integer size,
                                                             @RequestBody TbAdmin tbBrand)  {

        PageResult<TbAdmin> page1 = null;
        try {
            page1 = freightTemplateDao.findPage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return setResultSuccess(page1);
    }

    @Override
    public ResponseBase add(@RequestBody TbAdmin tbBrand) {
        boolean add = freightTemplateDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody  TbAdmin tbBrand) {
        boolean update = freightTemplateDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbAdmin> findById(@RequestParam("id") Integer id) {
        TbAdmin byId = freightTemplateDao.findById(id);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  Integer id) {
        boolean delete = freightTemplateDao.delete(id);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbAdmin>> findAll() {
        List<TbAdmin> all = freightTemplateDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }
}
