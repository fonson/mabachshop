package com.mabach.system.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.service.system.MenuService;
import com.mabach.service.system.entity.TbMenu;
import com.mabach.system.dao.MenuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
public class MenuServiceImpl extends ResponseBase implements MenuService {

    @Autowired
    private MenuDao freightTemplateDao;

    @Override
    public ResponseBase<PageResult<TbMenu>> findBrandBypage(@RequestParam("page")  Integer page,
                                                            @RequestParam("size") Integer size,
                                                            @RequestBody TbMenu tbBrand)  {

        PageResult<TbMenu> page1 = null;
        try {
            page1 = freightTemplateDao.findPage(page, size, tbBrand);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return setResultSuccess(page1);
    }

    @Override
    public ResponseBase add(@RequestBody TbMenu tbBrand) {
        boolean add = freightTemplateDao.add(tbBrand);
        if (!add){
            return setResultError();
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase update(@RequestBody  TbMenu tbBrand) {
        boolean update = freightTemplateDao.update(tbBrand);
        if (!update){
            return setResultError("更改失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<TbMenu> findById(@RequestParam("id") String id) {
        TbMenu byId = freightTemplateDao.findById(id);
        if (byId==null){
            return setResultError("查询失败");
        }
        return setResultSuccess(byId);
    }

    @Override
    public ResponseBase delete(@RequestParam("id")  String id) {
        boolean delete = freightTemplateDao.delete(id);
        if (!delete){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @Override
    public ResponseBase<List<TbMenu>> findAll() {
        List<TbMenu> all = freightTemplateDao.findAll();
        if (all==null){
            return setResultError();
        }
        return setResultSuccess(all);
    }

    @Override
    public ResponseBase<List<Map>> findMenu( @RequestParam("parentId") String parentId) {
        List<TbMenu> menuList = freightTemplateDao.findAll();
        List<Map> mapList = findMenuRecursion(menuList,parentId);
        System.out.println(mapList);
        return setResultSuccess(mapList);
    }





    private List<Map> findMenuRecursion(List<TbMenu> tbMenuList,String parentId){

        ArrayList<Map> list = new ArrayList<>();
        for (TbMenu tbMenu : tbMenuList) {
            if (tbMenu.getParentId().equals(parentId)){
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("path",tbMenu.getId());
                hashMap.put("title",tbMenu.getName());
                hashMap.put("icon",tbMenu.getIcon());
                hashMap.put("linkUrl",tbMenu.getUrl());
                hashMap.put("children",findMenuRecursion(tbMenuList,tbMenu.getId()));
                list.add(hashMap);
            }

        }
        return list;
    }
}