package com.mabach.system.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.service.system.entity.TbAdmin;
import com.mabach.system.mapper.TbAdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AdminDao {
    @Autowired
    private TbAdminMapper tbFreightTemplateMapper;

    public PageResult<TbAdmin> findPage(Integer currentPage, Integer size, TbAdmin tbBrand) throws Exception {
        Page<TbAdmin> page = new Page<>(currentPage, size);
        QueryWrapper<TbAdmin> wrapper = new QueryWrapper<>();

        wrapper.like(tbBrand.getLoginName()!=null,"login_name",tbBrand.getLoginName())
                .like(tbBrand.getStatus()!=null,"status",tbBrand.getStatus())
                .like(tbBrand.getPassword()!=null,"password",tbBrand.getPassword());

        IPage<TbAdmin> tbBrandIPage = tbFreightTemplateMapper.selectPage(page,wrapper);

        PageResult<TbAdmin> pageResult = new PageResult<TbAdmin>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbAdmin tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbAdmin findById(Integer id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbAdmin tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbAdmin> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }

    public TbAdmin findByUsername(String username){
        QueryWrapper<TbAdmin> wrapper = new QueryWrapper<>();
        wrapper.eq("login_name",username);
        TbAdmin tbAdmin = tbFreightTemplateMapper.selectOne(wrapper);
        return tbAdmin;
    }

    public List<String> findAuthorityByName(String username){
        List<String> authorityByName = tbFreightTemplateMapper.findAuthorityByName(username);
        return authorityByName;
    }
}
