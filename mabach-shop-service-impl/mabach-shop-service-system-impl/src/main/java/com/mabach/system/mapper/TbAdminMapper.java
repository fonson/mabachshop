package com.mabach.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.service.system.entity.TbAdmin;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
public interface TbAdminMapper extends BaseMapper<TbAdmin> {

    @Select("SELECT r.res_key FROM tb_resource r WHERE r.id IN (" +
            "SELECT rr.resource_id FROM tb_role_resource rr WHERE rr.role_id IN " +
            "(SELECT ar.role_id FROM tb_admin_role ar WHERE ar.admin_id IN " +
            "(SELECT id FROM tb_admin a WHERE a.login_name=#{username})))")
    public List<String> findAuthorityByName(@Param("username") String username);

}
