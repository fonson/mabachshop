package com.mabach.system.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.service.system.entity.TbRole;
import com.mabach.system.mapper.TbRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class RoleDAo {
    @Autowired
    private TbRoleMapper tbFreightTemplateMapper;

    public PageResult<TbRole> findPage(Integer currentPage, Integer size, TbRole tbBrand) throws Exception {
        Page<TbRole> page = new Page<>(currentPage, size);
        QueryWrapper<TbRole> wrapper = new QueryWrapper<>();

        wrapper.like(tbBrand.getName()!=null,"name",tbBrand.getName());

        IPage<TbRole> tbBrandIPage = tbFreightTemplateMapper.selectPage(page,wrapper);

        PageResult<TbRole> pageResult = new PageResult<TbRole>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbRole tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbRole findById(Integer id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbRole tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(Integer id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbRole> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }
}
