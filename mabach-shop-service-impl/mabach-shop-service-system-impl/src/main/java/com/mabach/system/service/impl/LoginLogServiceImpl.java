package com.mabach.system.service.impl;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.service.system.LoginLogService;
import com.mabach.service.system.entity.TbLoginLog;
import com.mabach.system.dao.LoginLogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginLogServiceImpl extends ResponseBase implements LoginLogService {
    @Autowired
    private LoginLogDao loginLogDao;
    @Override
    public ResponseBase<PageResult<TbLoginLog>> findBrandBypage(@RequestParam("page")  Integer page,
                                                                @RequestParam("size") Integer size)  {

        PageResult<TbLoginLog> page1 = null;
        try {
            page1 = loginLogDao.findPage(page, size);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return setResultSuccess(page1);
    }

}
