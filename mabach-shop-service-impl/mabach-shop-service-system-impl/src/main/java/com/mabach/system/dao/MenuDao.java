package com.mabach.system.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.service.system.entity.TbMenu;
import com.mabach.system.mapper.TbMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class MenuDao {
    @Autowired
    private TbMenuMapper tbFreightTemplateMapper;

    public PageResult<TbMenu> findPage(Integer currentPage, Integer size, TbMenu tbBrand) throws Exception {
        Page<TbMenu> page = new Page<>(currentPage, size);
        QueryWrapper<TbMenu> wrapper = new QueryWrapper<>();

        wrapper.like(tbBrand.getName()!=null,"name",tbBrand.getName()).
                like(tbBrand.getIcon()!=null,"icon",tbBrand.getIcon()).
                like(tbBrand.getUrl()!=null,"url",tbBrand.getUrl()).
                like(tbBrand.getParentId()!=null,"parent_id",tbBrand.getParentId());

        IPage<TbMenu> tbBrandIPage = tbFreightTemplateMapper.selectPage(page,wrapper);

        PageResult<TbMenu> pageResult = new PageResult<TbMenu>();
        pageResult.setRows(tbBrandIPage.getRecords());
        pageResult.setTotal(tbBrandIPage.getTotal());


        return pageResult;
    }

    public boolean add(TbMenu tbBrand){
        int i = tbFreightTemplateMapper.insert(tbBrand);
        return i>0?true:false;
    }

    public TbMenu findById(String id){

        return tbFreightTemplateMapper.selectById(id);
    }

    public boolean update(TbMenu tbBrand){
        int i = tbFreightTemplateMapper.updateById(tbBrand);
        return i>0?true:false;
    }
    public boolean delete(String id){
        int i = tbFreightTemplateMapper.deleteById(id);
        return i>0?true:false;
    }

    public List<TbMenu> findAll(){
        return tbFreightTemplateMapper.selectList(null);
    }

    public List<TbMenu> findByParentId(String parentId){
        QueryWrapper<TbMenu> wrapper = new QueryWrapper<>();
        wrapper.eq("parent_id",parentId);
        List<TbMenu> tbMenus = tbFreightTemplateMapper.selectList(wrapper);
        return tbMenus;
    }
}
