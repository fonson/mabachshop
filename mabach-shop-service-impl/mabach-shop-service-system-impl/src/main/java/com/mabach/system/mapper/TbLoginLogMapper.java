package com.mabach.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.service.system.entity.TbLoginLog;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
public interface TbLoginLogMapper extends BaseMapper<TbLoginLog> {

}
