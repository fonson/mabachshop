package com.mabach.member.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.mabach.constants.Constants;
import com.mabach.core.tokenRedis.TokenRedis;
//import com.mabach.core.transaction.TransactionRedisDataSourece;
import com.mabach.core.transaction.RedisDataTransactionSourece;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.core.utils.MD5Util;
import com.mabach.core.utils.TypeCastUtils;
import com.mabach.member.inputDTO.UserLoginInputDTO;
import com.mabach.member.dao.MemberLoginDao;
import com.mabach.member.mapper.entity.User;
import com.mabach.member.mapper.entity.UserToken;
import com.mabach.member.service.MemberLoginService;
import com.mabach.responseBase.ResponseBase;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/*
* 登录实现
* */
@RestController
@Slf4j
public class MemberLoginServiceImpl extends ResponseBase implements MemberLoginService {

    @Autowired
    private TokenRedis tokenRedis;
    @Autowired
    private MemberServiceImpl memberServiceImpl;
    @Autowired
    private MemberLoginDao memberLoginDao;
    @Autowired
    private RedisDataTransactionSourece redisDataTransactionSourece;

    @Override
    public ResponseBase<JSONObject> login(@RequestBody UserLoginInputDTO userLoginInputDTO) {
        if (userLoginInputDTO==null){
            return setResultError("对象不能为空");
        }
        if (StringUtils.isEmpty(userLoginInputDTO.getUserName())){
            return setResultError("账号不能为空");
        }
        if (StringUtils.isEmpty(userLoginInputDTO.getPassword())){
            return setResultError("密码不能为空");
        }
        if (StringUtils.isEmpty(userLoginInputDTO.getLoginType())){
            return setResultError("登录类型不能为空");
        }
        if (StringUtils.isEmpty(userLoginInputDTO.getDeviceInfo())){
            return setResultError("设备信息不能为空");
        }
//根据账号或手机号，和密码去数据库查询用户
        String password = MD5Util.encode(userLoginInputDTO.getPassword());
        User userDO = memberLoginDao.findByAccountAndPassword(userLoginInputDTO.getUserName(), password);
        if (userDO==null){
            return setResultError("账号或密码错误");
        }
//获取userId

        String userId= userId = userDO.getUserId()+"";
        TransactionStatus transactionStatus=null;
//根据userId+loginType 查询当前登陆类型账号之前是否有登陆过，如果登陆过 清除之前redistoken
        String token = null;
        try {
//            开启事务
            transactionStatus =  redisDataTransactionSourece.begin();
            UserToken usertokenDO = memberLoginDao.findByUserIdAndLoginType(userId, userLoginInputDTO.getLoginType());
            if (usertokenDO!=null){
    //            System.out.println(("根据userId+loginType 查询得到用户"));
                token = usertokenDO.getToken();
                tokenRedis.removeToken(token);
                memberLoginDao.updateIsAvailabilityByToken(token);

            }

//重新生成token并存入redis,并将登录信息插入数据库
            token = tokenRedis.createToken(Constants.TOKEN_MEMBER+userLoginInputDTO.getLoginType() +
                            userLoginInputDTO.getLoginType(),
                    userId, Constants.TOKEN_MEMBER_TIME_90day);
            //将登录信息插入数据库
            UserToken newUserTokenDO = BeanUtilsMabach.dtoToDo(userLoginInputDTO, UserToken.class);
            newUserTokenDO.setToken(token);
            newUserTokenDO.setIsAvailability(1);
            newUserTokenDO.setCreateTime(LocalDateTime.now());
            newUserTokenDO.setUserId(TypeCastUtils.toLong(userId));
            memberLoginDao.insertLoginInfo(newUserTokenDO);


//            最后提交事务
        redisDataTransactionSourece.commit(transactionStatus);
        } catch (Exception e) {
            e.printStackTrace();
            try {
//                回滚事务
            redisDataTransactionSourece.rollback(transactionStatus);
            } catch (Exception ex) {
                ex.printStackTrace();
            }finally {
                return setResultError("系统事务错误");
            }


        }

//登陆成功返回token和userid
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(token,userDO.getUserId() + "");
        return setResultSuccess("登陆成功",jsonObject);
    }
}
