package com.mabach.member.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.mabach.constants.Constants;
import com.mabach.core.utils.MD5Util;
import com.mabach.member.outputDTO.UserOutputDTO;
import com.mabach.responseBase.ResponseBase;

import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.member.inputDTO.UserRegisterInputDTO;
import com.mabach.member.feign.VerificaCodeServiceFeign;
import com.mabach.member.mapper.UserMapper;
import com.mabach.member.mapper.entity.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class MemberRegisterServiceImpl extends ResponseBase implements com.mabach.member.service.MemberRegisterService {

    @Autowired
    private VerificaCodeServiceFeign verificaCodeServiceFeign;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private MemberServiceImpl memberServiceImpl;

    @Override
    @Transactional
    public ResponseBase<JSONObject> register(@RequestBody UserRegisterInputDTO user,
                                             @RequestParam("registCode") String registCode) {
        if (user==null){
            return setResultError("对象不能为空");
        }
        if (StringUtils.isEmpty(user.getMobile())){
            return setResultError("手机号不能为空");
        }
        if (StringUtils.isEmpty(user.getUserName())){
            return setResultError("用户名不能为空");
        }
        if (StringUtils.isEmpty(registCode)){
            return setResultError("验证码不能为空");

        }

        //        判断改手机号是否已注册
        ResponseBase<UserOutputDTO> byPhone = memberServiceImpl.findByPhone(user.getMobile());
        if (byPhone.getCode()==200){
            return setResultError("该手机号已被注册");
        }

        //        判断该用户名是否已注册
        ResponseBase<UserOutputDTO> byUserName = memberServiceImpl.findByUserName(user.getUserName());
        if (byUserName.getCode()==200){
            return setResultError("该用户名号已被注册");
        }


//        调用weixin接口，验证redis中的验证码和传进来的验证码是否一致
        ResponseBase<JSONObject> jsonObjectResponseBase = verificaCodeServiceFeign.verificaWeixinCode(user.getMobile(), registCode);
        if(jsonObjectResponseBase.getCode()!=200){
            return jsonObjectResponseBase;
        }



        //注册，插入数据库
        User userDO = BeanUtilsMabach.dtoToDo(user, User.class);
        userDO.setCreateTime(LocalDateTime.now());
        userDO.setPassword(MD5Util.encode(userDO.getPassword()));
        int i = userMapper.insert(userDO);

        //删除 redis中的验证码
        redisTemplate.delete(Constants.WEIXINCODE_KEY+user.getMobile());

        return i>0?setResultSuccess("注册成功"):setResultError("注册失败");
    }
}
