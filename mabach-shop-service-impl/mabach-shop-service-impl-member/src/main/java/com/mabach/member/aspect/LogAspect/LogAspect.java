package com.mabach.member.aspect.LogAspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
@Slf4j
public class LogAspect {
    @Autowired
    private HttpServletRequest request;
    @Pointcut("execution(* com.mabach.member.service.impl.*.*(..))")
    public void pointCut(){ }

    @Before("pointCut()")
    public void doBefore(JoinPoint joinPoint){
        String url = request.getRequestURL().toString();
        String addr = request.getRemoteAddr();
        String classMethod=joinPoint.getSignature().getDeclaringTypeName()+"."
                +joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();
        LogEntity logEntity = new LogEntity(url, addr, classMethod, args);
        log.info("request : {}",logEntity);
    }

    @AfterReturning(pointcut = "pointCut()",returning = "result")
    public void doRetrun(Object result){
        log.info("return : {}",result);
    }
}