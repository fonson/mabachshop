package com.mabach.member.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mabach.member.mapper.UserMapper;
import com.mabach.member.mapper.UserTokenMapper;
import com.mabach.member.mapper.entity.User;
import com.mabach.member.mapper.entity.UserToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
@Slf4j
public class MemberLoginDao {
    @Autowired
    private UserTokenMapper userTokenMapper;
    @Autowired
    private UserMapper userMapper;

    public UserToken findByUserIdAndLoginType(String userId, String LoginType){
        QueryWrapper<UserToken> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",userId).eq("login_type",LoginType).
                eq("is_availability",1);
        UserToken userTokenDO = userTokenMapper.selectOne(wrapper);


        return userTokenDO;
    }

    public void updateIsAvailabilityByToken(String token){
        QueryWrapper<UserToken> wrapper = new QueryWrapper<>();
        wrapper.eq("token",token);
        UserToken userToken = new UserToken();
        userToken.setIsAvailability(0);
        userToken.setUpdateTime(LocalDateTime.now());
        userTokenMapper.update(userToken,wrapper);
    }

    public void insertLoginInfo(UserToken userToken){

        userTokenMapper.insert(userToken);
    }

    public User findByAccountAndPassword(String account,String password) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name", account).
                eq("password", password).or().
                eq("mobile", account).
                eq("password", password);
        User userDO = userMapper.selectOne(wrapper);
        System.out.println(userDO);
        return userDO;
    }

}
