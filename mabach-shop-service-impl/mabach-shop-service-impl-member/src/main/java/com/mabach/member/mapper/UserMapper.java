package com.mabach.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.member.mapper.entity.User;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 用户会员表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-16
 */

@Component
public interface UserMapper extends BaseMapper<User> {

}
