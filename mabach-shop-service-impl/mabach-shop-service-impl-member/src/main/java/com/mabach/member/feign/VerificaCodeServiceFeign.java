package com.mabach.member.feign;

import com.mabach.weixin.service.VerificaCodeService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-weixin")
public interface VerificaCodeServiceFeign extends VerificaCodeService {
}
