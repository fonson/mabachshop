package com.mabach.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.member.mapper.entity.UserToken;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-17
 */

public interface UserTokenMapper extends BaseMapper<UserToken> {

}
