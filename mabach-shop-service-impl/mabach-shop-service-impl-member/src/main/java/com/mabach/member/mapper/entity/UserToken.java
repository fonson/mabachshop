package com.mabach.member.mapper.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-17
 */
@Data
public class UserToken implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID")
    private Long id;

    @TableField("TOKEN")
    private String token;

    @TableField("LOGIN_TYPE")
    private String loginType;

    @TableField("DEVICE_INFO")
    private String deviceInfo;

    /**
     * 0为不可用，1为可用
     */
    @TableField("IS_AVAILABILITY")
    private Integer isAvailability;

    @TableField("USER_ID")
    private Long userId;

    @TableField("CREATE_TIME")
    private LocalDateTime createTime;

    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;


}
