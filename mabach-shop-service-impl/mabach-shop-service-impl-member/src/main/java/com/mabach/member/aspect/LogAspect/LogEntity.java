package com.mabach.member.aspect.LogAspect;

import lombok.Data;

@Data
public class LogEntity {
    private String URL;
    private String ip;
    private String classMethod;
    private Object args[];

    public LogEntity(String URL, String ip, String classMethod, Object[] args) {
        this.URL = URL;
        this.ip = ip;
        this.classMethod = classMethod;
        this.args = args;
    }
}
