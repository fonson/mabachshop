package com.mabach.member.mapper.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户会员表
 * </p>
 *
 * @author jobob
 * @since 2019-11-16
 */
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * user_id
     */
    @TableId(value = "USER_ID")
    private Long userId;

    /**
     * 手机号
     */
    @TableField("MOBILE")
    private String mobile;

    /**
     * 邮箱号
     */
    @TableField("EMAIL")
    private String email;

    /**
     * 密码
     */
    @TableField("PASSWORD")
    private String password;

    /**
     * 用户名
     */
    @TableField("USER_NAME")
    private String userName;

    /**
     * 性别  1男  2女
     */
    @TableField("SEX")
    private Integer sex;

    /**
     * 年龄
     */
    @TableField("AGE")
    private Integer age;

    /**
     * 注册时间
     */
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;

    /**
     * 是否可用 1正常  2冻结
     */
    @TableField("IS_AVALIBLE")
    private Integer isAvalible;

    /**
     * 用户头像
     */
    @TableField("PIC_IMG")
    private String picImg;

    /**
     * QQ联合登陆id
     */
    @TableField("QQ_OPENID")
    private String qqOpenid;

    /**
     * 微信公众号关注id
     */
    @TableField("WX_OPENID")
    private String wxOpenid;

    /**
     * 登录类型
     */
    @TableField("LOGIN_TYPE")
    private String loginType;


}
