package com.mabach.member.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mabach.member.mapper.UserMapper;
import com.mabach.member.mapper.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestParam;

@Repository
public class MemberServiceDao {
    @Autowired
    private UserMapper userMapper;
    public boolean isExist(String phone) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",phone);

        Integer i = userMapper.selectCount(wrapper);

        return i>0?true:false;

    }
}
