package com.mabach.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mabach.constants.Constants;
import com.mabach.core.tokenRedis.TokenRedis;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.core.utils.TypeCastUtils;
import com.mabach.member.outputDTO.UserOutputDTO;
import com.mabach.member.dao.MemberServiceDao;
import com.mabach.member.mapper.UserMapper;
import com.mabach.member.mapper.entity.User;
import com.mabach.member.service.MemberService;
import com.mabach.responseBase.ResponseBase;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberServiceImpl extends ResponseBase implements MemberService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private TokenRedis tokenRedis;
    @Autowired
    private MemberServiceDao memberServiceDao;

    /*
    * 根据手机判断用户是否存在
    * */
    @Override
    public boolean isExist(@RequestParam("phone") String phone) {

//        ResponseBase<List<TbBrand>> brand = goodsServiceFeign.findBrand();
//        System.out.println(brand);


        return memberServiceDao.isExist(phone);

    }

@Test
public void test(){
    memberServiceDao.isExist("17688707959");
    }

    /*
    * 根据手机号查询用户信息
    * */
    @Override
    public ResponseBase<UserOutputDTO> findByPhone(@RequestParam(value = "phone") String phone) {
        if (StringUtils.isEmpty(phone)){
            return setResultError("手机号不能为空");
        }
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",phone);
        User user = userMapper.selectOne(wrapper);
        if (user==null){
            return setResultError(Constants.HTTP_RES_CODE_203,"用户不存在");
        }

        return setResultSuccess(BeanUtilsMabach.doToDto(user,UserOutputDTO.class));
    }

    /*
    * 根据用户名查询用户信息
    * */
    @Override
    public ResponseBase<UserOutputDTO> findByUserName(@RequestParam(value = "userName") String userName) {
        if (StringUtils.isEmpty(userName)){
            return setResultError("手机号不能为空");
        }

        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("user_name",userName);
        User user = userMapper.selectOne(wrapper);
        if (user==null){
            return setResultError(Constants.HTTP_RES_CODE_203,"用户不存在");
        }

        return setResultSuccess(BeanUtilsMabach.doToDto(user,UserOutputDTO.class));
    }

    /*
    * 根据token查询用户信息
    * */
    @Override
    public ResponseBase<UserOutputDTO> findBytoken(@RequestParam("token") String token) {
        if (StringUtils.isEmpty(token)){
            return setResultError("token不能为空");
        }
//        根据token获取redis中的userid
        String redisUserId = tokenRedis.getToken(token);
        if (StringUtils.isEmpty(redisUserId)){
            return setResultError("token错误或已经失效");
        }
        long userId = TypeCastUtils.toLong(redisUserId);
//根据redis中的userid查询user信息
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",userId);
        User user = userMapper.selectOne(wrapper);
        if (user==null){
            return setResultError("用户不存在");
        }

        return setResultSuccess(BeanUtilsMabach.doToDto(user,UserOutputDTO.class));
    }

}
