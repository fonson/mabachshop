package com.mabach;

import com.mabach.member.mapper.UserMapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppMember.class)
public class Test {
    @Autowired
    private UserMapper userMapper;

    @org.junit.Test
    public void test1(){
        System.out.println(userMapper);
    }
}
