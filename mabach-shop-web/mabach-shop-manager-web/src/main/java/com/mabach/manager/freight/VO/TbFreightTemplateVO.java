package com.mabach.manager.freight.VO;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */

public class TbFreightTemplateVO implements Serializable {


    /**
     * 模板名称
     */
    private String name;

    /**
     * 计费方式
     */
    private String type;

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }


    public void setName(String name) {
        this.name = name==""?null:name;
    }

    public void setType(String type) {
        this.type = type==""?null:type;
    }
}
