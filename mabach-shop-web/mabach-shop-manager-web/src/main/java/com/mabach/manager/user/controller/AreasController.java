package com.mabach.manager.user.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.manager.user.VO.TbAddressVO;
import com.mabach.manager.user.VO.TbAreasVO;
import com.mabach.manager.user.feign.AreasServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import com.mabach.user.service.entity.TbAddress;
import com.mabach.user.service.entity.TbAreas;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class AreasController extends ResponseWeb {
    @Autowired
    private AreasServiceFeign freightTemplateFeign;
    @GetMapping("/user/areas.html")
    public String brand(){
        return "user/areas";
    }


    @GetMapping("/areas/findAll.do")
    @ResponseBody
    public List<TbAreas> findAll(){
        ResponseBase<List<TbAreas>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/areas/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbAreas> showBrands(Integer page, Integer size,
                                            @RequestBody TbAreasVO tbBrandSelectVO){


        ResponseBase<PageResult<TbAreas>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbAreas.class));

        PageResult<TbAreas> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/areas/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbAreas tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/areas/findById.do")
    @ResponseBody
    public TbAreas findById(Integer areaid){

        ResponseBase<TbAreas> byId = freightTemplateFeign.findById(areaid);

        return byId.getData();
    }

    @PostMapping("/areas/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbAreas tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/areas/delete.do")
    @ResponseBody
    public ResponseWeb delete(Integer areaid){
        ResponseBase responseBase = freightTemplateFeign.delete(areaid);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}
