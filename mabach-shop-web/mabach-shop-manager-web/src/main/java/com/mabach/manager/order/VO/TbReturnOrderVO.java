package com.mabach.manager.order.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mabach.core.helper.LongJsonDeserializer;
import com.mabach.core.helper.LongJsonSerializer;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */

public class TbReturnOrderVO implements Serializable {


    /**
     * 订单号
     */
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long orderId;

    /**
     * 申请时间
     */
    private LocalDateTime applyTime;

    /**
     * 用户ID
     */
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long userId;

    /**
     * 用户账号
     */
    private String userAccount;

    /**
     * 联系人
     */
    private String linkman;

    /**
     * 联系人手机
     */
    private String linkmanMobile;

    /**
     * 类型
     */
    private String type;

    /**
     * 退款金额
     */
    private Integer returnMoney;

    /**
     * 是否退运费
     */
    private String isReturnFreight;

    /**
     * 申请状态
     */
    private String status;

    /**
     * 处理时间
     */
    private LocalDateTime disposeTime;

    /**
     * 退货退款原因
     */
    private Integer returnCause;

    /**
     * 凭证图片
     */
    private String evidence;

    /**
     * 问题描述
     */
    private String description;

    /**
     * 处理备注
     */
    private String remark;

    /**
     * 管理员id
     */
    private Integer adminId;

    public Long getOrderId() {
        return orderId;
    }

    public LocalDateTime getApplyTime() {
        return applyTime;
    }

    public Long getUserId() {
        return userId;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public String getLinkman() {
        return linkman;
    }

    public String getLinkmanMobile() {
        return linkmanMobile;
    }

    public String getType() {
        return type;
    }

    public Integer getReturnMoney() {
        return returnMoney;
    }

    public String getIsReturnFreight() {
        return isReturnFreight;
    }

    public String getStatus() {
        return status;
    }

    public LocalDateTime getDisposeTime() {
        return disposeTime;
    }

    public Integer getReturnCause() {
        return returnCause;
    }

    public String getEvidence() {
        return evidence;
    }

    public String getDescription() {
        return description;
    }

    public String getRemark() {
        return remark;
    }

    public Integer getAdminId() {
        return adminId;
    }


    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public void setApplyTime(LocalDateTime applyTime) {
        this.applyTime = applyTime;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount==""?null:userAccount;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman==""?null:linkman;
    }

    public void setLinkmanMobile(String linkmanMobile) {
        this.linkmanMobile = linkmanMobile==""?null:linkmanMobile;
    }

    public void setType(String type) {
        this.type = type==""?null:type;
    }

    public void setReturnMoney(Integer returnMoney) {
        this.returnMoney = returnMoney;
    }

    public void setIsReturnFreight(String isReturnFreight) {
        this.isReturnFreight = isReturnFreight==""?null:isReturnFreight;
    }

    public void setStatus(String status) {
        this.status = status==""?null:status;
    }

    public void setDisposeTime(LocalDateTime disposeTime) {
        this.disposeTime = disposeTime;
    }

    public void setReturnCause(Integer returnCause) {
        this.returnCause = returnCause;
    }

    public void setEvidence(String evidence) {
        this.evidence = evidence==""?null:evidence;
    }

    public void setDescription(String description) {
        this.description = description==""?null:description;
    }

    public void setRemark(String remark) {
        this.remark = remark==""?null:remark;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }
}
