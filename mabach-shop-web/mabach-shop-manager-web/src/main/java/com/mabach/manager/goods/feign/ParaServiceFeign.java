package com.mabach.manager.goods.feign;

import com.mabach.goods.service.ParaService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface ParaServiceFeign extends ParaService {
}
