package com.mabach.manager.user.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 行政区域县区信息表
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */

public class TbAreasVO implements Serializable {



    /**
     * 区域名称
     */
    private String area;

    /**
     * 城市ID
     */
    private Integer cityid;

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area==""?null:area;
    }

    public Integer getCityid() {
        return cityid;
    }

    public void setCityid(Integer cityid) {
        this.cityid = cityid;
    }


}
