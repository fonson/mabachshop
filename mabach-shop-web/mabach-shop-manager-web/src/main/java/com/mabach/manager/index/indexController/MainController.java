package com.mabach.manager.index.indexController;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class MainController {
    @GetMapping({"/","/main.html","/admin.html","/admin"})
    public String main(){
        return "main";
    }

    @GetMapping("/statistics.html")
    public String statistics(){
        return "交易统计";
    }

    @GetMapping({"/login.html","/login"})
    public String login(){
        return "login";
    }

    @GetMapping("/login/name.do")
    @ResponseBody
    public Map getAdminInfo(){
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, String> map = new HashMap<>();
        map.put("name",name);
        return map;
    }





}
