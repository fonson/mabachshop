package com.mabach.manager.user.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.manager.user.VO.TbAddressVO;
import com.mabach.manager.user.feign.AddressServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import com.mabach.user.service.entity.TbAddress;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class AddressController extends ResponseWeb {
    @Autowired
private AddressServiceFeign freightTemplateFeign;
    @GetMapping("/user/address.html")
    public String brand(){
        return "user/address";
    }


    @GetMapping("/address/findAll.do")
    @ResponseBody
    public List<TbAddress> findAll(){
        ResponseBase<List<TbAddress>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/address/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbAddress> showBrands(Integer page, Integer size,
                                          @RequestBody TbAddressVO tbBrandSelectVO){


        ResponseBase<PageResult<TbAddress>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbAddress.class));

        PageResult<TbAddress> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/address/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbAddress tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/address/findById.do")
    @ResponseBody
    public TbAddress findById(@RequestParam("id") Integer id){

        ResponseBase<TbAddress> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/address/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbAddress tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/address/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}

