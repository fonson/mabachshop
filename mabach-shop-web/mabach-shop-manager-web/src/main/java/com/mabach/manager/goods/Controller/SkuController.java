package com.mabach.manager.goods.Controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.goods.service.entity.TbSku;
import com.mabach.manager.goods.VO.TbSkuVO;
import com.mabach.manager.goods.feign.SkuServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class SkuController extends ResponseWeb {

    @Autowired
    private SkuServiceFeign skuServiceFeign;
    @GetMapping("/goods/sku.html")
    public String brand(){
        return "goods/sku";
    }

    @PostMapping("/sku/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbSku> findPage(Integer page, Integer size,
                                      @RequestBody TbSkuVO tbParaVO){


        ResponseBase<PageResult<TbSku>> brandBypage = skuServiceFeign.findPage(page, size,
                BeanUtilsMabach.dtoToDo(tbParaVO,TbSku.class));

        PageResult<TbSku> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/sku/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbSku tbBrand){
        ResponseBase responseBase = skuServiceFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/sku/findById.do")
    @ResponseBody
    public TbSku findById(String id){

        ResponseBase<TbSku> byId = skuServiceFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/sku/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbSku tbBrand){
        ResponseBase responseBase = skuServiceFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/sku/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") String id){
        ResponseBase responseBase = skuServiceFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}
