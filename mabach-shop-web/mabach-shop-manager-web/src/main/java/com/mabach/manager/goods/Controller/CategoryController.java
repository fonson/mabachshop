package com.mabach.manager.goods.Controller;

import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.goods.service.entity.TbCategory;
import com.mabach.manager.goods.VO.TbCategorySelectVO;
import com.mabach.manager.goods.feign.CategoryServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@Api(tags="品类服务")
public class CategoryController extends ResponseWeb {
    @Autowired
    private CategoryServiceFeign categoryServiceFeign;

    @GetMapping("/goods/category.html")
    public String brand(){
        return "goods/category";
    }

    @PostMapping("/category/findList.do")
    @ResponseBody
    public List<TbCategory> findList(@RequestBody TbCategorySelectVO tbCategorySelectVO){

        ResponseBase<List<TbCategory>> list = categoryServiceFeign.findList(BeanUtilsMabach.dtoToDo(tbCategorySelectVO,
                TbCategory.class));

        return list.getData();
    }

    @PostMapping("/category/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbCategory tbCategory){


        ResponseBase add = categoryServiceFeign.add(tbCategory);
        if (add.getCode()!=200){
            return setResultError();
        }

        return setResultSuccess() ;
    }

    @PostMapping("/category/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbCategory tbCategory){


        ResponseBase add = categoryServiceFeign.update(tbCategory);
        if (add.getCode()!=200){
            return setResultError();
        }

        return setResultSuccess() ;
    }

    @GetMapping("/category/findById.do")
    @ResponseBody
    public TbCategory findById(Integer id){


        ResponseBase<TbCategory> byId = categoryServiceFeign.findById(id);
        return byId.getData();
    }

    @GetMapping("/category/delete.do")
    @ResponseBody
    public ResponseWeb delete(Integer id){


        ResponseBase delete = categoryServiceFeign.delete(id);
        if (delete.getCode()!=200){
            return setResultError();
        }
        return setResultSuccess();
    }
}
