package com.mabach.manager.goods.feign;

import com.mabach.goods.service.TemplateService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface TemplateServiceFeign extends TemplateService {
}
