package com.mabach.manager.order.controller;

import com.mabach.manager.order.feign.CategoryReportFeign;
import com.mabach.order.service.entity.TbCategoryReport;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.CastUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class CategoryReportController extends ResponseWeb {
    @Autowired
    private CategoryReportFeign categoryReportFeign;
    @GetMapping("/report/categoryReport.html")
    public String categoryReport(){
        return "report/categoryReport";
    }

    @GetMapping("/categoryReport/category1Count.do")
    @ResponseBody
    public List<TbCategoryReport> findAll(@RequestParam("date1") String date1,
                                          @RequestParam("date2") String date2){
        LocalDate localDate1 = LocalDate.parse(date1, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate localDate2 = LocalDate.parse(date2, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        ResponseBase<List<TbCategoryReport>> all = categoryReportFeign.find( localDate1, localDate2);

        return all.getData();
    }
}
