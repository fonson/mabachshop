package com.mabach.manager.system.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.manager.system.VO.TbMenuVO;
import com.mabach.manager.system.feign.MenuFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import com.mabach.service.system.entity.TbMenu;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@Controller
public class MenuController extends ResponseWeb {
    @Autowired
    private MenuFeign freightTemplateFeign;

    @GetMapping("/system/menu.html")
    public String brand(){
        return "system/menu";
    }

    @GetMapping("/menu/findAll.do")
    @ResponseBody
    public List<TbMenu> findAll(){
        ResponseBase<List<TbMenu>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/menu/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbMenu> showBrands(Integer page, Integer size,
                                          @RequestBody TbMenuVO tbBrandSelectVO){


        ResponseBase<PageResult<TbMenu>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbMenu.class));

        PageResult<TbMenu> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/menu/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbMenu tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/menu/findById.do")
    @ResponseBody
    public TbMenu findById(@RequestParam("id") String id){

        ResponseBase<TbMenu> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/menu/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbMenu tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/menu/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") String id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }


    @GetMapping("/menu/findMenu.do")
    @ResponseBody
    public List<Map> findMenu(){
        ResponseBase<List<Map>> menu = freightTemplateFeign.findMenu("0");

        return menu.getData();
    }
}
