package com.mabach.manager.user.feign;

import com.mabach.user.service.AreasService;
import org.springframework.cloud.openfeign.FeignClient;


@FeignClient(name = "app-mabach-user")
public interface AreasServiceFeign extends AreasService {
}