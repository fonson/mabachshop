package com.mabach.manager.user.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.manager.user.VO.TbProvincesVO;
import com.mabach.manager.user.feign.ProvinceidServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import com.mabach.user.service.entity.TbProvinces;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class ProvinceidController extends ResponseWeb {
    @Autowired
    private ProvinceidServiceFeign freightTemplateFeign;
    @GetMapping("/user/provinces.html")
    public String brand(){
        return "user/provinces";
    }


    @GetMapping("/provinces/findAll.do")
    @ResponseBody
    public List<TbProvinces> findAll(){
        ResponseBase<List<TbProvinces>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/provinces/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbProvinces> showBrands(Integer page, Integer size,
                                           @RequestBody TbProvincesVO tbBrandSelectVO){


        ResponseBase<PageResult<TbProvinces>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbProvinces.class));

        PageResult<TbProvinces> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/provinces/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbProvinces tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/provinces/findById.do")
    @ResponseBody
    public TbProvinces findById(@RequestParam("provinceid")  Integer provinceid){

        ResponseBase<TbProvinces> byId = freightTemplateFeign.findById(provinceid);

        return byId.getData();
    }

    @PostMapping("/provinces/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbProvinces tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/provinces/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("provinceid") Integer provinceid){
        ResponseBase responseBase = freightTemplateFeign.delete(provinceid);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}
