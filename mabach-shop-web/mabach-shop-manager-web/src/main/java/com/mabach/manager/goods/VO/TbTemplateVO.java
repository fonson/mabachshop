package com.mabach.manager.goods.VO;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-23
 */

public class TbTemplateVO implements Serializable {


    /**
     * 模板名称
     */
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name==""?null:name;
    }
}
