package com.mabach.manager.goods.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-23
 */

public class TbAlbumPageVO implements Serializable {

    /**
     * 相册名称
     */
    private String title;

    /**
     * 相册封面
     */
    private String image;

    /**
     * 图片列表
     */
    private String imageItems;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title==""?null:title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image==""?null:image;
    }

    public String getImageItems() {
        return imageItems;
    }

    public void setImageItems(String imageItems) {
        this.imageItems = imageItems==""?null:imageItems;
    }

    @Override
    public String toString() {
        return "TbAlbumPageVO{" +
                "title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", imageItems='" + imageItems + '\'' +
                '}';
    }
}
