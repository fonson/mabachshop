package com.mabach.manager.goods.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-23
 */

public class TbParaVO implements Serializable {


    /**
     * 名称
     */
    private String name;

    /**
     * 选项
     */
    private String options;

    /**
     * 排序
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name==""?null:name;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options==""?null:options;
    }
}
