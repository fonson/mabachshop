package com.mabach.manager.order.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;

import com.mabach.manager.order.VO.TbReturnOrderItemVO;
import com.mabach.manager.order.feign.ReturnOrderItemServiceFeign;
import com.mabach.order.service.entity.TbReturnOrderItem;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class ReturnOrderItemController extends ResponseWeb {
    @Autowired
private ReturnOrderItemServiceFeign freightTemplateFeign;
    @GetMapping("/order/returnOrderItem.html")
    public String brand(){
        return "order/returnOrderItem";
    }

    @GetMapping("/returnOrderItem/findAll.do")
    @ResponseBody
    public List<TbReturnOrderItem> findAll(){
        ResponseBase<List<TbReturnOrderItem>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/returnOrderItem/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbReturnOrderItem> showBrands(Integer page, Integer size,
                                          @RequestBody TbReturnOrderItemVO tbBrandSelectVO){


        ResponseBase<PageResult<TbReturnOrderItem>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbReturnOrderItem.class));

        PageResult<TbReturnOrderItem> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/returnOrderItem/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbReturnOrderItem tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/returnOrderItem/findById.do")
    @ResponseBody
    public TbReturnOrderItem findById(Long id){

        ResponseBase<TbReturnOrderItem> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/returnOrderItem/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbReturnOrderItem tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/returnOrderItem/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Long id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}

