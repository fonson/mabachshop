package com.mabach.manager.order.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.manager.order.VO.TbOrderVO;
import com.mabach.manager.order.feign.OrderServiceFeign;
import com.mabach.order.service.entity.TbOrder;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class OrderController extends ResponseWeb {
    @Autowired
private OrderServiceFeign freightTemplateFeign;
    @GetMapping("/order/order.html")
    public String order(){
        return "order/order";
    }

    @GetMapping("/order/orderDeliver.html")
    public String orderDeliver(){
        return "order/orderDeliver";
    }


    @GetMapping("/order/findAll.do")
    @ResponseBody
    public List<TbOrder> findAll(){
        ResponseBase<List<TbOrder>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/order/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbOrder> showBrands(Integer page, Integer size,
                                          @RequestBody TbOrderVO tbBrandSelectVO){


        ResponseBase<PageResult<TbOrder>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbOrder.class));

        PageResult<TbOrder> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/order/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbOrder tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/order/findById.do")
    @ResponseBody
    public TbOrder findById(String id){

        ResponseBase<TbOrder> byId = freightTemplateFeign.findById(id);

        return byId.getData();
//        i
    }

    @PostMapping("/order/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbOrder tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/order/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") String id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @PostMapping("/order/deliverMany.do")
    @ResponseBody
    public ResponseWeb deliverManyOrder(@RequestBody List<TbOrder> domain){
        ResponseBase responseBase = freightTemplateFeign.deliverManyOrder(domain);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

}

