package com.mabach.manager.user.feign;

import com.mabach.freight.service.FreightService;
import com.mabach.user.service.AddressService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-user")
public interface AddressServiceFeign extends AddressService {
}
