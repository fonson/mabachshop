package com.mabach.manager.goods.feign;

import com.mabach.goods.service.SpecService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface SpecServiceFeign extends SpecService {
}
