package com.mabach.manager.order.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;

import com.mabach.manager.order.VO.TbOrderConfigVO;
import com.mabach.manager.order.feign.OrderConfigServiceFeign;
import com.mabach.order.service.entity.TbOrderConfig;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class OrderConfigController extends ResponseWeb {
    @Autowired
private OrderConfigServiceFeign freightTemplateFeign;
    @GetMapping("/order/orderConfig.html")
    public String brand(){
        return "order/orderConfig";
    }

    @GetMapping("/orderConfig/findAll.do")
    @ResponseBody
    public List<TbOrderConfig> findAll(){
        ResponseBase<List<TbOrderConfig>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/orderConfig/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbOrderConfig> showBrands(Integer page, Integer size,
                                          @RequestBody TbOrderConfigVO tbBrandSelectVO){


        ResponseBase<PageResult<TbOrderConfig>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbOrderConfig.class));

        PageResult<TbOrderConfig> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/orderConfig/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbOrderConfig tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/orderConfig/findById.do")
    @ResponseBody
    public TbOrderConfig findById(Long id){

        ResponseBase<TbOrderConfig> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/orderConfig/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbOrderConfig tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/orderConfig/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Long id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}

