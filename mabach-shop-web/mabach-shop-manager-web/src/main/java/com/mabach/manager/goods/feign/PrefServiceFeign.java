package com.mabach.manager.goods.feign;

import com.mabach.goods.service.PrefService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface PrefServiceFeign extends PrefService {
}
