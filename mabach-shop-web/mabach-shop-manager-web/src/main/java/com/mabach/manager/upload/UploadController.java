package com.mabach.manager.upload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    private HttpServletRequest request;

    @RequestMapping("/native.do")
    public String upload(@RequestParam("file") MultipartFile file){
//        String realPath1 = request.getSession().getServletContext().getRealPath("static\\img");
//     用配置文件配置服务器路径
        String realPath="D:\\project\\mabach-shop-parent\\mabach-shop-web\\mabach-shop-manager-web\\src\\main\\resources\\static\\img";

        String fileName=UUID.randomUUID().toString().replace("-","")+ file.getOriginalFilename();;
        String path=realPath+"\\"+ fileName;
        File desFile = new File(path);
        if (!desFile.getParentFile().exists()){
            desFile.mkdirs();
        }
        try {
            file.transferTo(desFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
       String filePath=getLocalIpPort()+"/static/img/"+fileName;
        return filePath;
    }

    private String getLocalIpPort(){
        String url=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
        return url;
    }

}
