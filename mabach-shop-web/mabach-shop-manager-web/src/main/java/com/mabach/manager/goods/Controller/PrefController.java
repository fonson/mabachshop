package com.mabach.manager.goods.Controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.goods.service.entity.TbPref;
import com.mabach.manager.goods.VO.TbPrefVO;
import com.mabach.manager.goods.feign.PrefServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class PrefController extends ResponseWeb{
    @Autowired
    private PrefServiceFeign prefServiceFeign;
    @GetMapping("/goods/pref.html")
    public String brand(){
        return "goods/pref";
    }

    @PostMapping("/pref/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbPref> findPage(Integer page, Integer size,
                                         @RequestBody TbPrefVO tbParaVO){


        ResponseBase<PageResult<TbPref>> brandBypage = prefServiceFeign.findPage(page, size,
                BeanUtilsMabach.dtoToDo(tbParaVO,TbPref.class));

        PageResult<TbPref> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/pref/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbPref tbBrand){
        ResponseBase responseBase = prefServiceFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/pref/findById.do")
    @ResponseBody
    public TbPref findById(Integer id){

        ResponseBase<TbPref> byId = prefServiceFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/pref/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbPref tbBrand){
        ResponseBase responseBase = prefServiceFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/pref/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = prefServiceFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}
