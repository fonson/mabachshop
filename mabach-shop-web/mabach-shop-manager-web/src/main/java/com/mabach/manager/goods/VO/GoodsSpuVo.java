package com.mabach.manager.goods.VO;



import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-24
 */

public class GoodsSpuVo implements Serializable {


    /**
     * 货号
     */
    private String sn;

    /**
     * SPU名
     */
    private String name;

    /**
     * 副标题
     */
    private String caption;

    /**
     * 品牌ID
     */
    private Integer brandId;

    /**
     * 一级分类
     */
    private Integer category1Id;

    /**
     * 二级分类
     */
    private Integer category2Id;

    /**
     * 三级分类
     */
    private Integer category3Id;


    /**
     * 运费模板id
     */
    private Integer freightId;


    /**
     * 售后服务
     */
    private String saleService;

    /**
     * 介绍
     */
    private String introduction;

    /**
     * 规格列表
     */
    private String specItems;

    public String getSn() {
        return sn;
    }

    public String getName() {
        return name;
    }

    public String getCaption() {
        return caption;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public Integer getCategory1Id() {
        return category1Id;
    }

    public Integer getCategory2Id() {
        return category2Id;
    }

    public Integer getCategory3Id() {
        return category3Id;
    }

    public Integer getFreightId() {
        return freightId;
    }

    public String getSaleService() {
        return saleService;
    }

    public String getIntroduction() {
        return introduction;
    }

    public String getSpecItems() {
        return specItems;
    }


    public void setSn(String sn) {
        this.sn = sn==""?null:sn;
    }

    public void setName(String name) {
        this.name = name==""?null:name;
    }

    public void setCaption(String caption) {
        this.caption = caption==""?null:caption;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId==null?null:brandId;
    }

    public void setCategory1Id(Integer category1Id) {
        this.category1Id = category1Id==null?null:category1Id;
    }

    public void setCategory2Id(Integer category2Id) {
        this.category2Id = category2Id==null?null:category2Id;
    }

    public void setCategory3Id(Integer category3Id) {
        this.category3Id = category3Id==null?null:category3Id;
    }

    public void setFreightId(Integer freightId) {
        this.freightId = freightId==null?null:freightId;
    }

    public void setSaleService(String saleService) {
        this.saleService = saleService==""?null:saleService;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction==""?null:introduction;
    }

    public void setSpecItems(String specItems) {
        this.specItems = specItems==""?null:specItems;
    }
}
