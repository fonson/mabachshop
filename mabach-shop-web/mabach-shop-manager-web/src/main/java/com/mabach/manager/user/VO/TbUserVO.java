package com.mabach.manager.user.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */

public class TbUserVO implements Serializable {


    /**
     * 密码，加密存储
     */
    private String password;

    /**
     * 注册手机号
     */
    private String phone;

    /**
     * 注册邮箱
     */
    private String email;

    /**
     * 创建时间
     */
    private LocalDateTime created;

    /**
     * 修改时间
     */
    private LocalDateTime updated;

    /**
     * 会员来源：1:PC，2：H5，3：Android，4：IOS
     */
    private String sourceType;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 真实姓名
     */
    private String name;

    /**
     * 使用状态（1正常 0非正常）
     */
    private String status;

    /**
     * 头像地址
     */
    private String headPic;

    /**
     * QQ号码
     */
    private String qq;

    /**
     * 手机是否验证 （0否  1是）
     */
    private String isMobileCheck;

    /**
     * 邮箱是否检测（0否  1是）
     */
    private String isEmailCheck;

    /**
     * 性别，1男，0女
     */
    private String sex;

    /**
     * 会员等级
     */
    private Integer userLevel;

    /**
     * 积分
     */
    private Integer points;

    /**
     * 经验值
     */
    private Integer experienceValue;

    /**
     * 出生年月日
     */
    private LocalDateTime birthday;

    /**
     * 最后登录时间
     */
    private LocalDateTime lastLoginTime;


    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public String getSourceType() {
        return sourceType;
    }

    public String getNickName() {
        return nickName;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getHeadPic() {
        return headPic;
    }

    public String getQq() {
        return qq;
    }

    public String getIsMobileCheck() {
        return isMobileCheck;
    }

    public String getIsEmailCheck() {
        return isEmailCheck;
    }

    public String getSex() {
        return sex;
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public Integer getPoints() {
        return points;
    }

    public Integer getExperienceValue() {
        return experienceValue;
    }

    public LocalDateTime getBirthday() {
        return birthday;
    }

    public LocalDateTime getLastLoginTime() {
        return lastLoginTime;
    }





    public void setPassword(String password) {
        this.password = password==""?null:password;
    }

    public void setPhone(String phone) {
        this.phone = phone==""?null:phone;
    }

    public void setEmail(String email) {
        this.email = email==""?null:email;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType==""?null:sourceType;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName==""?null:nickName;
    }

    public void setName(String name) {
        this.name = name==""?null:name;
    }

    public void setStatus(String status) {
        this.status = status==""?null:status;
    }

    public void setHeadPic(String headPic) {
        this.headPic = headPic==""?null:headPic;
    }

    public void setQq(String qq) {
        this.qq = qq==""?null:qq;
    }

    public void setIsMobileCheck(String isMobileCheck) {
        this.isMobileCheck = isMobileCheck==""?null:isMobileCheck;
    }

    public void setIsEmailCheck(String isEmailCheck) {
        this.isEmailCheck = isEmailCheck==""?null:isEmailCheck;
    }

    public void setSex(String sex) {
        this.sex = sex==""?null:sex;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public void setExperienceValue(Integer experienceValue) {
        this.experienceValue = experienceValue;
    }

    public void setBirthday(LocalDateTime birthday) {
        this.birthday = birthday;
    }

    public void setLastLoginTime(LocalDateTime lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
}
