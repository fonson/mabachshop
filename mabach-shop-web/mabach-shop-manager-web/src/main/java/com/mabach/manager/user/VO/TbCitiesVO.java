package com.mabach.manager.user.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 行政区域地州市信息表
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */

public class TbCitiesVO implements Serializable {

    private String city;

    /**
     * 省份ID
     */
    private Integer provinceid;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city==""?null:city;
    }

    public Integer getProvinceid() {
        return provinceid;
    }

    public void setProvinceid(Integer provinceid) {
        this.provinceid = provinceid;
    }
}
