package com.mabach.manager.goods.VO;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-24
 */

public class TbSpecVO implements Serializable {


    /**
     * 名称
     */
    private String name;

    /**
     * 规格选项
     */
    private String options;


    private Integer templateId;

    public String getName() {
        return name;
    }

    public String getOptions() {
        return options;
    }

    public void setName(String name) {
        this.name = name==""?null:name;
    }

    public void setOptions(String options) {
        this.options = options==""?null:options;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId==null?null:templateId;
    }
}
