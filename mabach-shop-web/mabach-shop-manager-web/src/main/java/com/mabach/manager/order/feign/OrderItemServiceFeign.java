package com.mabach.manager.order.feign;

import com.mabach.freight.service.FreightService;
import com.mabach.order.service.OrderItemService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-order")
public interface OrderItemServiceFeign extends OrderItemService {
}
