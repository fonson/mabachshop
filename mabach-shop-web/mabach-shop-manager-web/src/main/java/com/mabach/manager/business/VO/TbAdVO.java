package com.mabach.manager.business.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */

public class TbAdVO implements Serializable {


    /**
     * 广告名称
     */
    private String name;

    /**
     * 广告位置
     */
    private String position;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;

    /**
     * 到期时间
     */
    private LocalDateTime endTime;

    /**
     * 状态
     */
    private String status;

    /**
     * 图片地址
     */
    private String image;

    /**
     * URL
     */
    private String url;

    /**
     * 备注
     */
    private String remarks;

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public String getStatus() {
        return status;
    }

    public String getImage() {
        return image;
    }

    public String getUrl() {
        return url;
    }

    public String getRemarks() {
        return remarks;
    }


    public void setName(String name) {
        this.name = name==""?null:name;
    }

    public void setPosition(String position) {
        this.position = position==""?null:position;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public void setStatus(String status) {
        this.status = status==""?null:status;
    }

    public void setImage(String image) {
        this.image = image==""?null:image;
    }

    public void setUrl(String url) {
        this.url = url==""?null:url;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks==""?null:remarks;
    }
}
