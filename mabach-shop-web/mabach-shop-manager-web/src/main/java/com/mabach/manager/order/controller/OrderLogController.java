package com.mabach.manager.order.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.manager.order.VO.TbOrderLogVO;
import com.mabach.manager.order.feign.OrderLogServiceFeign;
import com.mabach.order.service.entity.TbOrderLog;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class OrderLogController extends ResponseWeb {
    @Autowired
private OrderLogServiceFeign freightTemplateFeign;
    @GetMapping("/order/orderLog.html")
    public String brand(){
        return "order/orderLog";
    }

    @GetMapping("/orderLog/findAll.do")
    @ResponseBody
    public List<TbOrderLog> findAll(){
        ResponseBase<List<TbOrderLog>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/orderLog/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbOrderLog> showBrands(Integer page, Integer size,
                                          @RequestBody TbOrderLogVO tbBrandSelectVO){


        ResponseBase<PageResult<TbOrderLog>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbOrderLog.class));

        PageResult<TbOrderLog> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/orderLog/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbOrderLog tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/orderLog/findById.do")
    @ResponseBody
    public TbOrderLog findById(Long id){

        ResponseBase<TbOrderLog> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/orderLog/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbOrderLog tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/orderLog/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Long id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}

