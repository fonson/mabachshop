package com.mabach.manager.system.VO;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */

public class TbAdminVO implements Serializable {


    /**
     * 用户名
     */
    private String loginName;

    /**
     * 密码
     */
    private String password;

    /**
     * 状态
     */
    private String status;

    public String getLoginName() {
        return loginName;
    }

    public String getPassword() {
        return password;
    }

    public String getStatus() {
        return status;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName==""?null:loginName;
    }

    public void setPassword(String password) {
        this.password = password==""?null:password;
    }

    public void setStatus(String status) {
        this.status = status==""?null:status;
    }
}
