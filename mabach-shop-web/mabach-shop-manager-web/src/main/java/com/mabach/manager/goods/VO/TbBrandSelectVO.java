package com.mabach.manager.goods.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 品牌表
 * </p>
 *
 * @author jobob
 * @since 2019-11-21
 */

public class TbBrandSelectVO implements Serializable {
    /**
     * 品牌名称
     */
    private String name;



    /**
     * 品牌的首字母
     */
    private String letter;

    public String getName() {
        return name;
    }
    public String getLetter() {
        return letter;
    }

    public void setName(String name) {
        this.name = name==""?null:name;
    }



    public void setLetter(String letter) {
        this.letter = letter==""?null:letter;
    }

    @Override
    public String toString() {
        return "TbBrandVO{" +
                "name='" + name + '\'' +
                ", letter='" + letter + '\'' +
                '}';
    }
}
