package com.mabach.manager.user.feign;

import com.mabach.user.service.CitiesService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-user")
public interface CitiesServiceFeign extends CitiesService {
}
