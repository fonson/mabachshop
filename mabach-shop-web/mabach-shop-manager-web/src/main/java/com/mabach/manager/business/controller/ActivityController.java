package com.mabach.manager.business.controller;

import com.mabach.business.service.entity.TbActivity;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;

import com.mabach.manager.business.VO.TbActivityVO;
import com.mabach.manager.business.feign.ActivityServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class ActivityController extends ResponseWeb {
    @Autowired
private ActivityServiceFeign freightTemplateFeign;
    @GetMapping("/business/activity.html")
    public String brand(){
        return "business/activity";
    }

    @GetMapping("/activity/findAll.do")
    @ResponseBody
    public List<TbActivity> findAll(){
        ResponseBase<List<TbActivity>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/activity/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbActivity> showBrands(Integer page, Integer size,
                                          @RequestBody TbActivityVO tbBrandSelectVO){


        ResponseBase<PageResult<TbActivity>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbActivity.class));

        PageResult<TbActivity> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/activity/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbActivity tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/activity/findById.do")
    @ResponseBody
    public TbActivity findById(Integer id){

        ResponseBase<TbActivity> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/activity/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbActivity tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/activity/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}

