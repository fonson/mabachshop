package com.mabach.manager.business.feign;

import com.mabach.business.service.ActivityService;
import com.mabach.freight.service.FreightService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface ActivityServiceFeign extends ActivityService {
}
