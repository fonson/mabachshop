package com.mabach.manager.order.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;

import com.mabach.manager.order.VO.TbPreferentialVO;
import com.mabach.manager.order.feign.PreferentialServiceFeign;
import com.mabach.order.service.entity.TbPreferential;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class PreferentialController extends ResponseWeb {
    @Autowired
private PreferentialServiceFeign freightTemplateFeign;
    @GetMapping("/order/preferential.html")
    public String brand(){
        return "order/preferential";
    }

    @GetMapping("/preferential/findAll.do")
    @ResponseBody
    public List<TbPreferential> findAll(){
        ResponseBase<List<TbPreferential>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/preferential/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbPreferential> showBrands(Integer page, Integer size,
                                          @RequestBody TbPreferentialVO tbBrandSelectVO){


        ResponseBase<PageResult<TbPreferential>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbPreferential.class));

        PageResult<TbPreferential> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/preferential/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbPreferential tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/preferential/findById.do")
    @ResponseBody
    public TbPreferential findById(Integer id){

        ResponseBase<TbPreferential> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/preferential/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbPreferential tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/preferential/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}

