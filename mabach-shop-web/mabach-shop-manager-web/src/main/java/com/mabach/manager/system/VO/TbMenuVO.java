package com.mabach.manager.system.VO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
@Data
public class TbMenuVO implements Serializable {


    /**
     * 菜单名称
     */
    private String name;

    /**
     * 图标
     */
    private String icon;

    /**
     * URL
     */
    private String url;

    /**
     * 上级菜单ID
     */
    private String parentId;

    public String getName() {
        return name;
    }

    public String getIcon() {
        return icon;
    }

    public String getUrl() {
        return url;
    }

    public String getParentId() {
        return parentId;
    }


    public void setName(String name) {
        this.name = name==""?null:name;
    }

    public void setIcon(String icon) {
        this.icon = icon==""?null:icon;
    }

    public void setUrl(String url) {
        this.url = url==""?null:url;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId==""?null:parentId;
    }
}
