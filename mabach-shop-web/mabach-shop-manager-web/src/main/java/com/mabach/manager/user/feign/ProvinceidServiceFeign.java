package com.mabach.manager.user.feign;

import com.mabach.user.service.ProvinceService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-user")
public interface ProvinceidServiceFeign extends ProvinceService {
}
