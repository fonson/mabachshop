package com.mabach.manager.goods.Controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.goods.service.entity.TbBrand;
import com.mabach.manager.goods.VO.TbBrandSelectVO;
import com.mabach.manager.goods.feign.BrandServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@Api(tags="品牌服务")
public class BrandController extends ResponseWeb {
    @Autowired
    private BrandServiceFeign brandServiceFeign;


    @GetMapping("/goods/brand.html")
    public String brand(){

        return "goods/brand";
    }

    @GetMapping("/brand/findAll.do")
    @ResponseBody
    public List<TbBrand> findAll(){
        ResponseBase<List<TbBrand>> all = brandServiceFeign.findAllBrand();

        return all.getData();
    }


    @PostMapping("/brand/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbBrand> showBrands(Integer page, Integer size,
                                          @RequestBody TbBrandSelectVO tbBrandSelectVO){


        ResponseBase<PageResult<TbBrand>> brandBypage = brandServiceFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbBrand.class));

        PageResult<TbBrand> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/brand/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbBrand tbBrand){
        ResponseBase responseBase = brandServiceFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/brand/findById.do")
    @ResponseBody
    public TbBrand findById(Integer id){

        ResponseBase<TbBrand> byId = brandServiceFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/brand/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbBrand tbBrand){
        ResponseBase responseBase = brandServiceFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/brand/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = brandServiceFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}
