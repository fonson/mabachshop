package com.mabach.manager.user.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 省份信息表
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */

public class TbProvincesVO implements Serializable {


    /**
     * 省份名称
     */
    private String province;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province==""?null:province;
    }
}
