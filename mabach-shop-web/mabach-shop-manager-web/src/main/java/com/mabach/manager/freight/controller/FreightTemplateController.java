package com.mabach.manager.freight.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.freight.service.entity.TbFreightTemplate;
import com.mabach.manager.freight.VO.TbFreightTemplateVO;
import com.mabach.manager.freight.feign.FreightTemplateFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class FreightTemplateController extends ResponseWeb {
    @Autowired
private FreightTemplateFeign freightTemplateFeign;
    @GetMapping("/config/freightTemplate.html")
    public String brand(){
        return "config/freightTemplate";
    }

    @GetMapping("/freightTemplate/findAll.do")
    @ResponseBody
    public List<TbFreightTemplate> findAll(){
        ResponseBase<List<TbFreightTemplate>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/freightTemplate/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbFreightTemplate> showBrands(Integer page, Integer size,
                                          @RequestBody TbFreightTemplateVO tbBrandSelectVO){


        ResponseBase<PageResult<TbFreightTemplate>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbFreightTemplate.class));

        PageResult<TbFreightTemplate> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/freightTemplate/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbFreightTemplate tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/freightTemplate/findById.do")
    @ResponseBody
    public TbFreightTemplate findById(Integer id){

        ResponseBase<TbFreightTemplate> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/freightTemplate/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbFreightTemplate tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/freightTemplate/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}

