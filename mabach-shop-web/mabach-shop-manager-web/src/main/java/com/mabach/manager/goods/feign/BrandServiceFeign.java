package com.mabach.manager.goods.feign;

import com.mabach.goods.service.BrandService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface BrandServiceFeign extends BrandService {



}
