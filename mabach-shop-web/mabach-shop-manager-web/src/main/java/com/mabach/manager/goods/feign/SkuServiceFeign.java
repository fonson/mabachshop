package com.mabach.manager.goods.feign;

import com.mabach.goods.service.SkuService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface SkuServiceFeign extends SkuService {
}
