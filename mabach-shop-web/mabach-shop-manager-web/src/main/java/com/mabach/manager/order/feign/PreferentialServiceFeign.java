package com.mabach.manager.order.feign;

import com.mabach.freight.service.FreightService;
import com.mabach.order.service.PreferentialService;
import com.mabach.user.service.ProvinceService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-order")
public interface PreferentialServiceFeign extends PreferentialService {
}
