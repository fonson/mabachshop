package com.mabach.manager.goods.feign;

import com.mabach.goods.service.AlbumService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface AlbumServiceFeign extends AlbumService {
}
