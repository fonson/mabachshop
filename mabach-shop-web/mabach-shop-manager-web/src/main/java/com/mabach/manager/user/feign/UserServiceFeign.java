package com.mabach.manager.user.feign;

import com.mabach.user.service.UserService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-user")
public interface UserServiceFeign extends UserService {
}
