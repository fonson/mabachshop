package com.mabach.manager.goods.VO;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-24
 */

public class TbSpuVO implements Serializable {


    /**
     * 货号
     */
    private String sn;

    /**
     * SPU名
     */
    private String name;

    /**
     * 副标题
     */
    private String caption;

    /**
     * 品牌ID
     */
    private Integer brandId;

    /**
     * 一级分类
     */
    private Integer category1Id;

    /**
     * 二级分类
     */
    private Integer category2Id;

    /**
     * 三级分类
     */
    private Integer category3Id;

    /**
     * 模板ID
     */
    private Integer templateId;

    /**
     * 运费模板id
     */
    private Integer freightId;

    /**
     * 图片
     */
    private String image;

    /**
     * 图片列表
     */
    private String images;

    /**
     * 售后服务
     */
    private String saleService;

    /**
     * 介绍
     */
    private String introduction;

    /**
     * 规格列表
     */
    private String specItems;

    /**
     * 参数列表
     */
    private String paraItems;

    /**
     * 销量
     */
    private Integer saleNum;

    /**
     * 评论数
     */
    private Integer commentNum;

    /**
     * 是否上架
     */
    private String isMarketable;

    /**
     * 是否启用规格
     */
    private String isEnableSpec;

    /**
     * 是否删除
     */
    private String isDelete;

    /**
     * 审核状态
     */
    private String status;

    public String getSn() {
        return sn;
    }

    public String getName() {
        return name;
    }

    public String getCaption() {
        return caption;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public Integer getCategory1Id() {
        return category1Id;
    }

    public Integer getCategory2Id() {
        return category2Id;
    }

    public Integer getCategory3Id() {
        return category3Id;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public Integer getFreightId() {
        return freightId;
    }

    public String getImage() {
        return image;
    }

    public String getImages() {
        return images;
    }

    public String getSaleService() {
        return saleService;
    }

    public String getIntroduction() {
        return introduction;
    }

    public String getSpecItems() {
        return specItems;
    }

    public String getParaItems() {
        return paraItems;
    }

    public Integer getSaleNum() {
        return saleNum;
    }

    public Integer getCommentNum() {
        return commentNum;
    }

    public String getIsMarketable() {
        return isMarketable;
    }

    public String getIsEnableSpec() {
        return isEnableSpec;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public String getStatus() {
        return status;
    }


    public void setSn(String sn) {
        this.sn = sn==""?null:sn;
    }

    public void setName(String name) {
        this.name = name==""?null:name;
    }

    public void setCaption(String caption) {
        this.caption = caption==""?null:caption;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId==null?null:brandId;
    }

    public void setCategory1Id(Integer category1Id) {
        this.category1Id = category1Id==null?null:category1Id;
    }

    public void setCategory2Id(Integer category2Id) {
        this.category2Id = category2Id==null?null:category2Id;
    }

    public void setCategory3Id(Integer category3Id) {
        this.category3Id = category3Id==null?null:category3Id;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId==null?null:templateId;
    }

    public void setFreightId(Integer freightId) {
        this.freightId = freightId==null?null:freightId;
    }

    public void setImage(String image) {
        this.image = image==""?null:image;
    }

    public void setImages(String images) {
        this.images = images==""?null:images;
    }

    public void setSaleService(String saleService) {
        this.saleService = saleService==""?null:saleService;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction==""?null:introduction;
    }

    public void setSpecItems(String specItems) {
        this.specItems = specItems==""?null:specItems;
    }

    public void setParaItems(String paraItems) {
        this.paraItems = paraItems==""?null:paraItems;
    }

    public void setSaleNum(Integer saleNum) {
        this.saleNum = saleNum==null?null:saleNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum==null?null:commentNum;
    }

    public void setIsMarketable(String isMarketable) {
        this.isMarketable = isMarketable==""?null:isMarketable;
    }

    public void setIsEnableSpec(String isEnableSpec) {
        this.isEnableSpec = isEnableSpec==""?null:isEnableSpec;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete==""?null:isDelete;
    }

    public void setStatus(String status) {
        this.status = status==""?null:status;
    }
}
