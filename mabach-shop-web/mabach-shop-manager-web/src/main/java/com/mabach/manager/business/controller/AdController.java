package com.mabach.manager.business.controller;

import com.mabach.business.service.entity.TbAd;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.manager.business.feign.AdServiceFeign;

import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class AdController extends ResponseWeb {
    @Autowired
    private AdServiceFeign freightTemplateFeign;
    @GetMapping("/business/ad.html")
    public String brand(){
        return "business/ad";
    }

    @GetMapping("/ad/findAll.do")
    @ResponseBody
    public List<TbAd> findAll(){
        ResponseBase<List<TbAd>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/ad/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbAd> showBrands(Integer page, Integer size,
                                             @RequestBody TbAd tbBrandSelectVO){


        ResponseBase<PageResult<TbAd>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbAd.class));

        PageResult<TbAd> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/ad/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbAd tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/ad/findById.do")
    @ResponseBody
    public TbAd findById(Integer id){

        ResponseBase<TbAd> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/ad/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbAd tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/ad/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}