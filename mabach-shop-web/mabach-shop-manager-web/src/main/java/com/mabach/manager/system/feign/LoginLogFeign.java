package com.mabach.manager.system.feign;

import com.mabach.service.system.LoginLogService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-system")
public interface LoginLogFeign extends LoginLogService {
}
