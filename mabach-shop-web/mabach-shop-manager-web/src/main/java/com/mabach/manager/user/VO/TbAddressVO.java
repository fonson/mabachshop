package com.mabach.manager.user.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */

public class TbAddressVO implements Serializable {


    /**
     * 用户名
     */
    private String username;

    /**
     * 省
     */
    private Integer provinceid;

    /**
     * 市
     */
    private Integer cityid;

    /**
     * 县/区
     */
    private Integer areaid;

    /**
     * 电话
     */
    private String phone;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 是否是默认 1默认 0否
     */
    private String isDefault;

    /**
     * 别名
     */
    private String alias;

    public String getUsername() {
        return username;
    }

    public Integer getProvinceid() {
        return provinceid;
    }

    public Integer getCityid() {
        return cityid;
    }

    public Integer getAreaid() {
        return areaid;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getContact() {
        return contact;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public String getAlias() {
        return alias;
    }


    public void setUsername(String username) {
        this.username = username==""?null:username;
    }

    public void setProvinceid(Integer provinceid) {
        this.provinceid = provinceid;
    }

    public void setCityid(Integer cityid) {
        this.cityid = cityid;
    }

    public void setAreaid(Integer areaid) {
        this.areaid = areaid;
    }

    public void setPhone(String phone) {
        this.phone = phone==""?null:phone;
    }

    public void setAddress(String address) {
        this.address = address==""?null:address;
    }

    public void setContact(String contact) {
        this.contact = contact==""?null:contact;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault==""?null:isDefault;
    }

    public void setAlias(String alias) {
        this.alias = alias==""?null:alias;
    }
}
