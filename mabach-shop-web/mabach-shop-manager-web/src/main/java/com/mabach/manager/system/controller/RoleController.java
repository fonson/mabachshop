package com.mabach.manager.system.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.manager.system.VO.TbRoleVO;
import com.mabach.manager.system.feign.RoleServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import com.mabach.service.system.entity.TbRole;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class RoleController extends ResponseWeb {
    @Autowired
    private RoleServiceFeign freightTemplateFeign;
    @GetMapping("/system/role.html")
    public String brand(){
        return "system/role";
    }

    @GetMapping("/role/findAll.do")
    @ResponseBody
    public List<TbRole> findAll(){
        ResponseBase<List<TbRole>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/role/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbRole> showBrands(Integer page, Integer size,
                                         @RequestBody TbRoleVO tbBrandSelectVO){


        ResponseBase<PageResult<TbRole>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbRole.class));

        PageResult<TbRole> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/role/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbRole tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/role/findById.do")
    @ResponseBody
    public TbRole findById(Integer id){

        ResponseBase<TbRole> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/role/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbRole tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/role/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}