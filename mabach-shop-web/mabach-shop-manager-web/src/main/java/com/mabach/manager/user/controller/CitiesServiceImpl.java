package com.mabach.manager.user.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.manager.user.VO.TbCitiesVO;
import com.mabach.manager.user.feign.CitiesServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import com.mabach.user.service.entity.TbCities;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class CitiesServiceImpl extends ResponseWeb {
    @Autowired
    private CitiesServiceFeign freightTemplateFeign;
    @GetMapping("/user/cities.html")
    public String brand(){
        return "user/cities";
    }


    @GetMapping("/cities/findAll.do")
    @ResponseBody
    public List<TbCities> findAll(){
        ResponseBase<List<TbCities>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/cities/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbCities> showBrands(Integer page, Integer size,
                                          @RequestBody TbCitiesVO tbBrandSelectVO){


        ResponseBase<PageResult<TbCities>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbCities.class));

        PageResult<TbCities> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/cities/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbCities tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/cities/findById.do")
    @ResponseBody
    public TbCities findById(Integer cityid){

        ResponseBase<TbCities> byId = freightTemplateFeign.findById(cityid);

        return byId.getData();
    }

    @PostMapping("/cities/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbCities tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/cities/delete.do")
    @ResponseBody
    public ResponseWeb delete(Integer cityid){
        ResponseBase responseBase = freightTemplateFeign.delete(cityid);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}
