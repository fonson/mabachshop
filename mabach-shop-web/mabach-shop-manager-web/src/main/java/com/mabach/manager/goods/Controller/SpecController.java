package com.mabach.manager.goods.Controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.goods.service.entity.TbSpec;
import com.mabach.manager.goods.VO.TbSpecVO;
import com.mabach.manager.goods.feign.SpecServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class SpecController extends ResponseWeb {
    @Autowired
    private SpecServiceFeign specServiceFeign;

    @GetMapping("/goods/spec.html")
    public String brand(){
        return "goods/spec";
    }

    @PostMapping("/spec/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbSpec> findPage(Integer page, Integer size,
                                       @RequestBody TbSpecVO tbParaVO){


        ResponseBase<PageResult<TbSpec>> brandBypage = specServiceFeign.findPage(page, size,
                BeanUtilsMabach.dtoToDo(tbParaVO,TbSpec.class));

        PageResult<TbSpec> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/spec/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbSpec tbBrand){
        ResponseBase responseBase = specServiceFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/spec/findById.do")
    @ResponseBody
    public TbSpec findById(Integer id){

        ResponseBase<TbSpec> byId = specServiceFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/spec/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbSpec tbBrand){
        ResponseBase responseBase = specServiceFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/spec/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = specServiceFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @PostMapping("/spec/findList.do")
    @ResponseBody
    public List<TbSpec> findList(@RequestBody TbSpecVO tbSpecVO){
        ResponseBase<List<TbSpec>> list = specServiceFeign.findList(BeanUtilsMabach.dtoToDo(tbSpecVO,TbSpec.class));
        return list.getData();
    }
}
