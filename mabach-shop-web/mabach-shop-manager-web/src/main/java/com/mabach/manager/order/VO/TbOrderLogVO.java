package com.mabach.manager.order.VO;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mabach.core.helper.LongJsonDeserializer;
import com.mabach.core.helper.LongJsonSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */

public class TbOrderLogVO implements Serializable {

    /**
     * 操作员
     */
    private String operater;

    /**
     * 操作时间
     */
    private LocalDateTime operateTime;

    /**
     * 订单ID
     */
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long orderId;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 付款状态
     */
    private String payStatus;

    /**
     * 发货状态
     */
    private String consignStatus;

    /**
     * 备注
     */
    private String remarks;

    public String getOperater() {
        return operater;
    }

    public LocalDateTime getOperateTime() {
        return operateTime;
    }

    public Long getOrderId() {
        return orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public String getConsignStatus() {
        return consignStatus;
    }

    public String getRemarks() {
        return remarks;
    }


    public void setOperater(String operater) {
        this.operater = operater==""?null:operater;
    }

    public void setOperateTime(LocalDateTime operateTime) {
        this.operateTime = operateTime;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus==""?null:orderStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus==""?null:payStatus;
    }

    public void setConsignStatus(String consignStatus) {
        this.consignStatus = consignStatus==""?null:consignStatus;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks==""?null:remarks;
    }
}
