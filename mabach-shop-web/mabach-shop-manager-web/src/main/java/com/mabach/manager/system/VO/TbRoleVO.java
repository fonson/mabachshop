package com.mabach.manager.system.VO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
@Data
public class TbRoleVO implements Serializable {


    /**
     * 角色名称
     */
    private String name;


}
