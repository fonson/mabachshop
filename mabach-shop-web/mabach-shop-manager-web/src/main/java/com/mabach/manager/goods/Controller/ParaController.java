package com.mabach.manager.goods.Controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.goods.service.entity.TbPara;
import com.mabach.manager.goods.VO.TbParaVO;
import com.mabach.manager.goods.feign.ParaServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@Api(tags="参数服务")
public class ParaController extends ResponseWeb {
    @Autowired
    private ParaServiceFeign paraServiceFeign;

    @GetMapping("/goods/para.html")
    public String brand(){
        return "goods/para";
    }

    @PostMapping("/para/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbPara> showBrands(Integer page, Integer size,
                                         @RequestBody TbParaVO tbParaVO){


        ResponseBase<PageResult<TbPara>> brandBypage = paraServiceFeign.findPage(page, size,
                BeanUtilsMabach.dtoToDo(tbParaVO,TbPara.class));

        PageResult<TbPara> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/para/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbPara tbBrand){
        ResponseBase responseBase = paraServiceFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/para/findById.do")
    @ResponseBody
    public TbPara findById(Integer id){

        ResponseBase<TbPara> byId = paraServiceFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/para/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbPara tbBrand){
        ResponseBase responseBase = paraServiceFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/para/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = paraServiceFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/para/findAll.do")
    @ResponseBody
    public List<TbPara> findAll(){
        ResponseBase<List<TbPara>> allPara = paraServiceFeign.findAllPara();
        return allPara.getData();
    }
}
