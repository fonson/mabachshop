package com.mabach.manager.user.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.manager.user.VO.TbUserVO;
import com.mabach.manager.user.feign.UserServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import com.mabach.user.service.entity.TbUser;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class UserController extends ResponseWeb {
    @Autowired
    private UserServiceFeign freightTemplateFeign;
    @GetMapping("/user/user.html")
    public String brand(){
        return "user/user";
    }


    @GetMapping("/user/findAll.do")
    @ResponseBody
    public List<TbUser> findAll(){
        ResponseBase<List<TbUser>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/user/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbUser> showBrands(Integer page, Integer size,
                                              @RequestBody TbUserVO tbBrandSelectVO){


        ResponseBase<PageResult<TbUser>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbUser.class));

        PageResult<TbUser> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/user/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbUser tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/user/findById.do")
    @ResponseBody
    public TbUser findById(@RequestParam("username")  String username){

        ResponseBase<TbUser> byId = freightTemplateFeign.findById(username);

        return byId.getData();
    }

    @PostMapping("/user/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbUser tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/user/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("username") String username){
        ResponseBase responseBase = freightTemplateFeign.delete(username);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}
