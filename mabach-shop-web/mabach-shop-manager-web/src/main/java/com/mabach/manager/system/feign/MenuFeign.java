package com.mabach.manager.system.feign;

import com.mabach.service.system.MenuService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-system")
public interface MenuFeign extends MenuService {
}
