package com.mabach.manager.goods.VO;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mabach.core.helper.LongJsonDeserializer;
import com.mabach.core.helper.LongJsonSerializer;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author jobob
 * @since 2019-11-23
 */

public class TbSkuVO implements Serializable {


    /**
     * 商品条码
     */
    private String sn;

    /**
     * SKU名称
     */
    private String name;

    /**
     * 价格（分）
     */
    private Integer price;

    /**
     * 库存数量
     */
    private Integer num;

    /**
     * 库存预警数量
     */
    private Integer alertNum;

    /**
     * 商品图片
     */
    private String image;

    /**
     * 商品图片列表
     */
    private String images;

    /**
     * 重量（克）
     */
    private Integer weight;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * SPUID
     */
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long spuId;

    /**
     * 类目ID
     */
    private Integer categoryId;

    /**
     * 类目名称
     */
    private String categoryName;

    /**
     * 品牌名称
     */
    private String brandName;

    /**
     * 规格
     */
    private String spec;

    /**
     * 销量
     */
    private Integer saleNum;

    /**
     * 评论数
     */
    private Integer commentNum;

    /**
     * 商品状态 1-正常，2-下架，3-删除
     */
    private String status;


    public String getSn() {
        return sn;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getNum() {
        return num;
    }

    public Integer getAlertNum() {
        return alertNum;
    }

    public String getImage() {
        return image;
    }

    public String getImages() {
        return images;
    }

    public Integer getWeight() {
        return weight;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public Long getSpuId() {
        return spuId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getBrandName() {
        return brandName;
    }

    public String getSpec() {
        return spec;
    }

    public Integer getSaleNum() {
        return saleNum;
    }

    public Integer getCommentNum() {
        return commentNum;
    }

    public String getStatus() {
        return status;
    }



    public void setSn(String sn) {
        this.sn = sn==""?null:sn;
    }

    public void setName(String name) {
        this.name = name==""?null:name;
    }

    public void setPrice(Integer price) {
        this.price = price==null?null:price;
    }

    public void setNum(Integer num) {
        this.num = num==null?null:num;
    }

    public void setAlertNum(Integer alertNum) {
        this.alertNum = alertNum==null?null:alertNum;
    }

    public void setImage(String image) {
        this.image = image==""?null:image;
    }

    public void setImages(String images) {
        this.images = images==""?null:images;
    }

    public void setWeight(Integer weight) {
        this.weight = weight==null?null:weight;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime==null?null:createTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime==null?null:updateTime;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId==null?null:spuId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId==null?null:categoryId;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName==""?null:categoryName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName==""?null:brandName;
    }

    public void setSpec(String spec) {
        this.spec = spec==""?null:spec;
    }

    public void setSaleNum(Integer saleNum) {
        this.saleNum = saleNum==null?null:saleNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum==null?null:commentNum;
    }

    public void setStatus(String status) {
        this.status = status==""?null:status;
    }
}
