package com.mabach.manager.goods.Controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.goods.service.entity.TbGoods;
import com.mabach.goods.service.entity.TbSpu;
import com.mabach.manager.goods.VO.TbSpuVO;
import com.mabach.manager.goods.feign.SpuServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class SpuController extends ResponseWeb {
    @Autowired
    private SpuServiceFeign spuServiceFeign;
    @GetMapping("/goods/spu.html")
    public String spu(){
        return "goods/spu";
    }
    @GetMapping("/goods/spuUnMarketable.html")
    public String spuUnMarketable(){
        return "goods/spuUnMarketable";
    }
    @GetMapping("/goods/spuExamine.html")
    public String spuExamine(){
        return "goods/spuExamine";
    }

    @GetMapping("/goods/goods.html")
    public String goods(){
        return "goods/goods";
    }



    @PostMapping("/spu/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbSpu> findPage(Integer page, Integer size,
                                      @RequestBody TbSpuVO tbParaVO){


        ResponseBase<PageResult<TbSpu>> brandBypage = spuServiceFeign.findPage(page, size,
                BeanUtilsMabach.dtoToDo(tbParaVO,TbSpu.class));

        PageResult<TbSpu> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/spu/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbSpu tbBrand){
        ResponseBase responseBase = spuServiceFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/spu/findById.do")
    @ResponseBody
    public TbSpu findById(String id){

        ResponseBase<TbSpu> byId = spuServiceFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/spu/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbSpu tbBrand){
        ResponseBase responseBase = spuServiceFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/spu/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") String id){
        ResponseBase responseBase = spuServiceFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }

    @PostMapping("/spu/save.do")
    @ResponseBody
    public ResponseWeb save(@RequestBody TbGoods tbGoods){
        ResponseBase save = spuServiceFeign.save(tbGoods);
        if (save.getCode()!=200){
            return setResultError();
        }
        return setResultSuccess();
    }
    @GetMapping("/spu/findGoodsById.do")
    @ResponseBody
    public TbGoods findGoodsById(@RequestParam("id") String id){
        ResponseBase<TbGoods> goods = spuServiceFeign.findGoods(id);
        return goods.getData();
    }

    @PostMapping("/spu/examineManySpu.do")
    @ResponseBody
    public ResponseWeb examineManySpu(@RequestBody List<TbSpu> tbSpuList){
        ArrayList<String> list = new ArrayList<>();
        for (TbSpu tbSpu : tbSpuList) {

            list.add(tbSpu.getId());
        }

        ResponseBase<Integer> manySpu = spuServiceFeign.examineManySpu(list);
        if (manySpu.getCode()!=200){
            return setResultError();
        }
        return setResultSuccess("成功审核"+manySpu.getData()+"件商品");
    }

    @PostMapping("/spu/putManySpu.do")
    @ResponseBody
    public ResponseWeb putManySpu(@RequestBody List<TbSpu> tbSpuList){
        ArrayList<String> list = new ArrayList<>();
        for (TbSpu tbSpu : tbSpuList) {

            list.add(tbSpu.getId());
        }

        ResponseBase<Integer> manySpu = spuServiceFeign.putManySpu(list);
        if (manySpu.getCode()!=200){
            return setResultError();
        }
        return setResultSuccess("成功上架"+manySpu.getData()+"件商品");
    }

    @PostMapping("/spu/pullManySpu.do")
    @ResponseBody
    public ResponseWeb pullManySpu(@RequestBody List<TbSpu> tbSpuList){
        ArrayList<String> list = new ArrayList<>();
        for (TbSpu tbSpu : tbSpuList) {

            list.add(tbSpu.getId());
        }

        ResponseBase<Integer> manySpu = spuServiceFeign.pullManySpu(list);
        if (manySpu.getCode()!=200){
            return setResultError();
        }
        return setResultSuccess("成功下架"+manySpu.getData()+"件商品");
    }


}
