package com.mabach.manager.system.feign;

import com.mabach.business.service.AdService;
import com.mabach.service.system.AdminService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-system")
public interface AdminServiceFeign extends AdminService {
}
