package com.mabach.manager.order.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;

import com.mabach.manager.order.VO.TbReturnOrderVO;
import com.mabach.manager.order.feign.ReturnOrderServiceFeign;
import com.mabach.order.service.entity.TbReturnOrder;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class ReturnOrderController extends ResponseWeb {
    @Autowired
private ReturnOrderServiceFeign freightTemplateFeign;
    @GetMapping("/order/returnOrder.html")
    public String brand(){
        return "order/returnOrder";
    }

    @GetMapping("/returnOrder/findAll.do")
    @ResponseBody
    public List<TbReturnOrder> findAll(){
        ResponseBase<List<TbReturnOrder>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/returnOrder/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbReturnOrder> showBrands(Integer page, Integer size,
                                          @RequestBody TbReturnOrderVO tbBrandSelectVO){


        ResponseBase<PageResult<TbReturnOrder>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbReturnOrder.class));

        PageResult<TbReturnOrder> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/returnOrder/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbReturnOrder tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/returnOrder/findById.do")
    @ResponseBody
    public TbReturnOrder findById(Long id){

        ResponseBase<TbReturnOrder> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/returnOrder/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbReturnOrder tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/returnOrder/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Long id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}

