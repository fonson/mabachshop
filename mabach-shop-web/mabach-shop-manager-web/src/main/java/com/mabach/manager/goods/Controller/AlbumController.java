package com.mabach.manager.goods.Controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.goods.service.entity.TbAlbum;
import com.mabach.manager.goods.VO.TbAlbumPageVO;
import com.mabach.manager.goods.feign.AlbumServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@Api(tags="相册服务")
public class AlbumController extends ResponseWeb {
    @Autowired
    private AlbumServiceFeign albumServiceFeign;

    @GetMapping("/goods/album.html")
    public String brand(){
        return "goods/album";
    }

    @PostMapping("/album/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbAlbum> showBrands(Integer page, Integer size,
                                          @RequestBody TbAlbumPageVO tbAlbumPageVO){


        ResponseBase<PageResult<TbAlbum>> brandBypage = albumServiceFeign.findByPage(page, size,
                BeanUtilsMabach.dtoToDo(tbAlbumPageVO, TbAlbum.class));

        PageResult<TbAlbum> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/album/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbAlbum tbAlbum){


        ResponseBase add = albumServiceFeign.add(tbAlbum);
        if (add.getCode()!=200){
            return setResultError();
        }

        return setResultSuccess() ;
    }

    @PostMapping("/album/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbAlbum tbAlbum){

        ResponseBase add = albumServiceFeign.update(tbAlbum);
        if (add.getCode()!=200){
            return setResultError();
        }

        return setResultSuccess() ;
    }

    @GetMapping("/album/findById.do")
    @ResponseBody
    public TbAlbum findById(Long id){


        ResponseBase<TbAlbum> byId = albumServiceFeign.findById(id);
        return byId.getData();
    }

    @GetMapping("/album/delete.do")
    @ResponseBody
    public ResponseWeb delete(Long id){


        ResponseBase delete = albumServiceFeign.delete(id);
        if (delete.getCode()!=200){
            return setResultError();
        }
        return setResultSuccess();
    }

    @GetMapping("/album/findAll.do")
    @ResponseBody
    public List<TbAlbum> findAll(){

        ResponseBase<List<TbAlbum>> allTbAlbum = albumServiceFeign.findAllTbAlbum();

        return allTbAlbum.getData();
    }
}
