package com.mabach.manager.business.feign;

import com.mabach.business.service.AdService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-business")
public interface AdServiceFeign extends AdService {
}
