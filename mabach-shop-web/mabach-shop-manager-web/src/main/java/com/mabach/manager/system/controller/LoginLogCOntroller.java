package com.mabach.manager.system.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.manager.system.feign.LoginLogFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.service.system.entity.TbLoginLog;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller
public class LoginLogCOntroller {
@Autowired
private LoginLogFeign loginLogFeign;

    @GetMapping("/system/loginLog.html")
    public String brand(){
        return "system/loginLog";
    }


    @GetMapping("/loginLog/findPageByLogin.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbLoginLog> showBrands(Integer page, Integer size){


        ResponseBase<PageResult<TbLoginLog>> brandBypage = loginLogFeign.findBrandBypage(page, size);

        PageResult<TbLoginLog> data = brandBypage.getData();

        return data ;
    }
}
