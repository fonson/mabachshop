package com.mabach.manager.order.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */

public class TbOrderConfigVO implements Serializable {

    /**
     * 正常订单超时时间（分）
     */
    private Integer orderTimeout;

    /**
     * 秒杀订单超时时间（分）
     */
    private Integer seckillTimeout;

    /**
     * 自动收货（天）
     */
    private Integer takeTimeout;

    /**
     * 售后期限
     */
    private Integer serviceTimeout;

    /**
     * 自动五星好评
     */
    private Integer commentTimeout;


}
