package com.mabach.manager.goods.feign;

import com.mabach.goods.service.SpuService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface SpuServiceFeign extends SpuService {
}
