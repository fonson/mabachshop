package com.mabach.manager.freight.feign;

import com.mabach.freight.service.FreightService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-freightTemplate")
public interface FreightTemplateFeign extends FreightService {
}
