package com.mabach.manager.order.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;

import com.mabach.manager.order.VO.TbReturnCauseVO;
import com.mabach.order.service.ReturnCauseService;
import com.mabach.order.service.entity.TbReturnCause;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class ReturnCauseController extends ResponseWeb {
    @Autowired
private ReturnCauseService freightTemplateFeign;
    @GetMapping("/order/returnCause.html")
    public String brand(){
        return "order/returnCause";
    }

    @GetMapping("/returnCause/findAll.do")
    @ResponseBody
    public List<TbReturnCause> findAll(){
        ResponseBase<List<TbReturnCause>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/returnCause/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbReturnCause> showBrands(Integer page, Integer size,
                                          @RequestBody TbReturnCauseVO tbBrandSelectVO){


        ResponseBase<PageResult<TbReturnCause>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbReturnCause.class));

        PageResult<TbReturnCause> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/returnCause/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbReturnCause tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/returnCause/findById.do")
    @ResponseBody
    public TbReturnCause findById(Integer id){

        ResponseBase<TbReturnCause> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/returnCause/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbReturnCause tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/returnCause/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}

