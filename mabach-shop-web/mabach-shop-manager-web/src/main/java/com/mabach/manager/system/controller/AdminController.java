package com.mabach.manager.system.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;

import com.mabach.manager.system.VO.TbAdminVO;
import com.mabach.manager.system.feign.AdminServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import com.mabach.service.system.entity.TbAdmin;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class AdminController extends ResponseWeb {
    @Autowired
    private AdminServiceFeign freightTemplateFeign;
    @GetMapping("/system/admin.html")
    public String brand(){
        return "system/admin";
    }

    @GetMapping("/admin/findAll.do")
    @ResponseBody
    public List<TbAdmin> findAll(){
        ResponseBase<List<TbAdmin>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/admin/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbAdmin> showBrands(Integer page, Integer size,
                                             @RequestBody TbAdminVO tbBrandSelectVO){


        ResponseBase<PageResult<TbAdmin>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbAdmin.class));

        PageResult<TbAdmin> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/admin/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbAdmin tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/admin/findById.do")
    @ResponseBody
    public TbAdmin findById(Integer id){

        ResponseBase<TbAdmin> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/admin/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbAdmin tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/admin/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}