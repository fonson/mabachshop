package com.mabach.manager.order.controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.manager.order.VO.TbOrderItemVO;
import com.mabach.manager.order.feign.OrderItemServiceFeign;
import com.mabach.order.service.entity.TbOrderItem;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class OrderItemController extends ResponseWeb {
    @Autowired
private OrderItemServiceFeign freightTemplateFeign;
    @GetMapping("/order/orderItem.html")
    public String brand(){
        return "order/orderItem";
    }

    @GetMapping("/orderItem/findAll.do")
    @ResponseBody
    public List<TbOrderItem> findAll(){
        ResponseBase<List<TbOrderItem>> all = freightTemplateFeign.findAll();

        return all.getData();
    }


    @PostMapping("/orderItem/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbOrderItem> showBrands(Integer page, Integer size,
                                          @RequestBody TbOrderItemVO tbBrandSelectVO){


        ResponseBase<PageResult<TbOrderItem>> brandBypage = freightTemplateFeign.findBrandBypage(page, size,
                BeanUtilsMabach.dtoToDo(tbBrandSelectVO,TbOrderItem.class));

        PageResult<TbOrderItem> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/orderItem/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbOrderItem tbBrand){
        ResponseBase responseBase = freightTemplateFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/orderItem/findById.do")
    @ResponseBody
    public TbOrderItem findById(Long id){

        ResponseBase<TbOrderItem> byId = freightTemplateFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/orderItem/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbOrderItem tbBrand){
        ResponseBase responseBase = freightTemplateFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/orderItem/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Long id){
        ResponseBase responseBase = freightTemplateFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }
}

