package com.mabach.manager.system.feign;

import com.mabach.service.system.RoleService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-system")
public interface RoleServiceFeign extends RoleService {
}
