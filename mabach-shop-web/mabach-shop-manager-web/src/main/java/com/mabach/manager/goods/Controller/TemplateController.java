package com.mabach.manager.goods.Controller;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.core.utils.BeanUtilsMabach;
import com.mabach.goods.service.entity.TbTemplate;
import com.mabach.manager.goods.VO.TbTemplateVO;
import com.mabach.manager.goods.feign.TemplateServiceFeign;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@Api(tags="模板服务")
public class TemplateController extends ResponseWeb {
    @Autowired
    private TemplateServiceFeign templateServiceFeign;

    @GetMapping("/goods/template.html")
    public String brand(){
        return "goods/template";
    }

    @GetMapping("/template/findAll.do")
    @ResponseBody
    public List<TbTemplate> findAll(){
        ResponseBase<List<TbTemplate>> all = templateServiceFeign.findAll();
      return all.getData();

    }


    @PostMapping("/template/findPage.do")
    @ApiOperation(value="分页查询")
    @ResponseBody
    public PageResult<TbTemplate> findPage(Integer page, Integer size,
                                      @RequestBody TbTemplateVO tbParaVO){


        ResponseBase<PageResult<TbTemplate>> brandBypage = templateServiceFeign.findPageTemplate(page, size,
                BeanUtilsMabach.dtoToDo(tbParaVO,TbTemplate.class));

        PageResult<TbTemplate> data = brandBypage.getData();

        return data ;
    }

    @PostMapping("/template/add.do")
    @ResponseBody
    public ResponseWeb add(@RequestBody TbTemplate tbBrand){
        ResponseBase responseBase = templateServiceFeign.add(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/template/findById.do")
    @ResponseBody
    public TbTemplate findById(Integer id){

        ResponseBase<TbTemplate> byId = templateServiceFeign.findById(id);

        return byId.getData();
    }

    @PostMapping("/template/update.do")
    @ResponseBody
    public ResponseWeb update(@RequestBody TbTemplate tbBrand){
        ResponseBase responseBase = templateServiceFeign.update(tbBrand);
        if (responseBase.getCode()!=200){
            return setResultError("添加失败");
        }
        return setResultSuccess();
    }

    @GetMapping("/template/delete.do")
    @ResponseBody
    public ResponseWeb delete(@RequestParam("id") Integer id){
        ResponseBase responseBase = templateServiceFeign.delete(id);
        if (responseBase.getCode()!=200){
            return setResultError("删除失败");
        }
        return setResultSuccess();
    }


}
