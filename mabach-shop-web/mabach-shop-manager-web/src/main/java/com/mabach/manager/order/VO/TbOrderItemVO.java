package com.mabach.manager.order.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mabach.core.helper.LongJsonDeserializer;
import com.mabach.core.helper.LongJsonSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */

public class TbOrderItemVO implements Serializable {


    /**
     * 1级分类
     */
    private Integer categoryId1;

    /**
     * 2级分类
     */
    private Integer categoryId2;

    /**
     * 3级分类
     */
    private Integer categoryId3;

    /**
     * SPU_ID
     */
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long spuId;

    /**
     * SKU_ID
     */
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long skuId;

    /**
     * 订单ID
     */
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long orderId;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 单价
     */
    private Integer price;

    /**
     * 数量
     */
    private Integer num;

    /**
     * 总金额
     */
    private Integer money;

    /**
     * 实付金额
     */
    private Integer payMoney;

    /**
     * 图片地址
     */
    private String image;

    /**
     * 重量
     */
    private Integer weight;

    /**
     * 运费
     */
    private Integer postFee;

    /**
     * 是否退货
     */
    private String isReturn;

    public Integer getCategoryId1() {
        return categoryId1;
    }

    public Integer getCategoryId2() {
        return categoryId2;
    }

    public Integer getCategoryId3() {
        return categoryId3;
    }

    public Long getSpuId() {
        return spuId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getNum() {
        return num;
    }

    public Integer getMoney() {
        return money;
    }

    public Integer getPayMoney() {
        return payMoney;
    }

    public String getImage() {
        return image;
    }

    public Integer getWeight() {
        return weight;
    }

    public Integer getPostFee() {
        return postFee;
    }

    public String getIsReturn() {
        return isReturn;
    }

    public void setCategoryId1(Integer categoryId1) {
        this.categoryId1 = categoryId1;
    }

    public void setCategoryId2(Integer categoryId2) {
        this.categoryId2 = categoryId2;
    }

    public void setCategoryId3(Integer categoryId3) {
        this.categoryId3 = categoryId3;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public void setName(String name) {
        this.name = name==""?null:name;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public void setPayMoney(Integer payMoney) {
        this.payMoney = payMoney;
    }

    public void setImage(String image) {
        this.image = image==""?null:image;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public void setPostFee(Integer postFee) {
        this.postFee = postFee;
    }

    public void setIsReturn(String isReturn) {
        this.isReturn = isReturn==""?null:isReturn;
    }
}
