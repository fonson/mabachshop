package com.mabach.manager.goods.VO;

public class TbCategorySelectVO {
    private Integer parentId;

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId==null?null:parentId;
    }
}
