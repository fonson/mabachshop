package com.mabach.manager.order.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */

public class TbOrderVO implements Serializable {


    /**
     * 数量合计
     */
    private Integer totalNum;

    /**
     * 金额合计
     */
    private Integer totalMoney;

    /**
     * 优惠金额
     */
    private Integer preMoney;

    /**
     * 邮费
     */
    private Integer postFee;

    /**
     * 实付金额
     */
    private Integer payMoney;

    /**
     * 支付类型，1、在线支付、0 货到付款
     */
    private String payType;

    /**
     * 订单创建时间
     */
    private LocalDateTime createTime;

    /**
     * 订单更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 付款时间
     */
    private LocalDateTime payTime;

    /**
     * 发货时间
     */
    private LocalDateTime consignTime;

    /**
     * 交易完成时间
     */
    private LocalDateTime endTime;

    /**
     * 交易关闭时间
     */
    private LocalDateTime closeTime;

    /**
     * 物流名称
     */
    private String shippingName;

    /**
     * 物流单号
     */
    private String shippingCode;

    /**
     * 用户名称
     */
    private String username;

    /**
     * 买家留言
     */
    private String buyerMessage;

    /**
     * 是否评价
     */
    private String buyerRate;

    /**
     * 收货人
     */
    private String receiverContact;

    /**
     * 收货人手机
     */
    private String receiverMobile;

    /**
     * 收货人地址
     */
    private String receiverAddress;

    /**
     * 订单来源：1:web，2：app，3：微信公众号，4：微信小程序  5 H5手机页面
     */
    private String sourceType;

    /**
     * 交易流水号
     */
    private String transactionId;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 支付状态
     */
    private String payStatus;

    /**
     * 发货状态
     */
    private String consignStatus;

    /**
     * 是否删除
     */
    private String isDelete;

    public Integer getTotalNum() {
        return totalNum;
    }

    public Integer getTotalMoney() {
        return totalMoney;
    }

    public Integer getPreMoney() {
        return preMoney;
    }

    public Integer getPostFee() {
        return postFee;
    }

    public Integer getPayMoney() {
        return payMoney;
    }

    public String getPayType() {
        return payType;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public LocalDateTime getPayTime() {
        return payTime;
    }

    public LocalDateTime getConsignTime() {
        return consignTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public LocalDateTime getCloseTime() {
        return closeTime;
    }

    public String getShippingName() {
        return shippingName;
    }

    public String getShippingCode() {
        return shippingCode;
    }

    public String getUsername() {
        return username;
    }

    public String getBuyerMessage() {
        return buyerMessage;
    }

    public String getBuyerRate() {
        return buyerRate;
    }

    public String getReceiverContact() {
        return receiverContact;
    }

    public String getReceiverMobile() {
        return receiverMobile;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public String getSourceType() {
        return sourceType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public String getConsignStatus() {
        return consignStatus;
    }

    public String getIsDelete() {
        return isDelete;
    }


    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public void setTotalMoney(Integer totalMoney) {
        this.totalMoney = totalMoney;
    }

    public void setPreMoney(Integer preMoney) {
        this.preMoney = preMoney;
    }

    public void setPostFee(Integer postFee) {
        this.postFee = postFee;
    }

    public void setPayMoney(Integer payMoney) {
        this.payMoney = payMoney;
    }

    public void setPayType(String payType) {
        this.payType = payType==""?null:payType;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public void setPayTime(LocalDateTime payTime) {
        this.payTime = payTime;
    }

    public void setConsignTime(LocalDateTime consignTime) {
        this.consignTime = consignTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public void setCloseTime(LocalDateTime closeTime) {
        this.closeTime = closeTime;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName==""?null:shippingName;
    }

    public void setShippingCode(String shippingCode) {
        this.shippingCode = shippingCode==""?null:shippingCode;
    }

    public void setUsername(String username) {
        this.username = username==""?null:username;
    }

    public void setBuyerMessage(String buyerMessage) {
        this.buyerMessage = buyerMessage==""?null:buyerMessage;
    }

    public void setBuyerRate(String buyerRate) {
        this.buyerRate = buyerRate==""?null:buyerRate;
    }

    public void setReceiverContact(String receiverContact) {
        this.receiverContact = receiverContact==""?null:receiverContact;
    }

    public void setReceiverMobile(String receiverMobile) {
        this.receiverMobile = receiverMobile==""?null:receiverMobile;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress==""?null:receiverAddress;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType==""?null:sourceType;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId==""?null:transactionId;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus==""?null:orderStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus==""?null:payStatus;
    }

    public void setConsignStatus(String consignStatus) {
        this.consignStatus = consignStatus==""?null:consignStatus;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete==""?null:isDelete;
    }
}
