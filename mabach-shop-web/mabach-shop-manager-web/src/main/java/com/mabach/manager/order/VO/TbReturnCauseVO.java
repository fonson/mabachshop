package com.mabach.manager.order.VO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */

public class TbReturnCauseVO implements Serializable {


    /**
     * 原因
     */
    private String cause;

    /**
     * 排序
     */
    private Integer seq;

    /**
     * 是否启用
     */
    private String status;

    public String getCause() {
        return cause;
    }

    public Integer getSeq() {
        return seq;
    }

    public String getStatus() {
        return status;
    }

    public void setCause(String cause) {
        this.cause = cause==""?null:cause;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public void setStatus(String status) {
        this.status = status==""?null:status;
    }
}
