package com.mabach.security;

import com.mabach.service.system.entity.TbAdmin;
import com.mabach.system.dao.AdminDao;
import com.mabach.system.mapper.TbAdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AdminDao adminDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        TbAdmin byUsername = adminDao.findByUsername(username);
        if (byUsername==null){
            throw  new UsernameNotFoundException("账号不正确");
        }
        if ("0".equals(byUsername.getStatus())){
            throw  new RuntimeException("账号不可用");
        }
        List<String> authorityByName = adminDao.findAuthorityByName(username);

        ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for (String str : authorityByName) {
        grantedAuthorities.add(new SimpleGrantedAuthority(str));
        }
        System.out.println(grantedAuthorities);
        return new User(username,byUsername.getPassword(),grantedAuthorities );
    }
}
