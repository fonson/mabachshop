package com.mabach.security;

import com.mabach.core.utils.WebUtil;
import com.mabach.service.system.entity.TbLoginLog;
import com.mabach.system.mapper.TbLoginLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

/*
登陆成功处理器,记录登录日志
 */
@Component
public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
   @Autowired
   private TbLoginLogMapper tbLoginLogMapper;

   public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication authentication)
         throws IOException, ServletException {

      String name = authentication.getName();
      String ip = req.getRemoteAddr();
//      String cityByIP = WebUtil.getCityByIP(ip);

      TbLoginLog tbLoginLog = new TbLoginLog();
      tbLoginLog.setLoginName(name);
      tbLoginLog.setLoginTime(LocalDateTime.now());
      tbLoginLog.setIp(ip);
      tbLoginLog.setLocation("");
      tbLoginLog.setBrowserName(WebUtil.getBrowserName(req));
      tbLoginLogMapper.insert(tbLoginLog);
      res.sendRedirect("/");
   }

}
