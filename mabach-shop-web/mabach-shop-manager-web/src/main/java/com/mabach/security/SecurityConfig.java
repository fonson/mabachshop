package com.mabach.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private MyAuthenticationSuccessHandler myAuthenticationSuccessHandler;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.headers().frameOptions().sameOrigin(); //设置同源
        http.authorizeRequests()
                .antMatchers("/error/*","/static/**","/favicon.ico","/login").permitAll()
                .antMatchers("/*/find*.do").fullyAuthenticated()
                .antMatchers("/*/delete*.do").hasAuthority("superAdmin")
                .antMatchers("/**").fullyAuthenticated().and()
                .formLogin().loginPage("/login").successHandler(myAuthenticationSuccessHandler).and()
                .logout().and()
                .csrf().disable()
                .exceptionHandling().accessDeniedPage("/error/403");


    }
}
