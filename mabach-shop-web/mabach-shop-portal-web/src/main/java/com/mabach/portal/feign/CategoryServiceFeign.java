package com.mabach.portal.feign;

import com.mabach.goods.service.CategoryService;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "app-mabach-goods")
public interface CategoryServiceFeign extends CategoryService {
}
