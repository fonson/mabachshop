package com.mabach.portal.controller;


import com.mabach.business.service.entity.TbAd;
import com.mabach.portal.feign.AdServiceFeign;
import com.mabach.portal.feign.CategoryServiceFeign;
import com.mabach.responseBase.ResponseBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Map;

@Controller
public class IndexController {

    @Autowired
    private AdServiceFeign adServiceFeign;
    @Autowired
    private CategoryServiceFeign categoryServiceFeign;

    @GetMapping({"/","index"})
    public String index(Model model){
        ResponseBase<List<TbAd>> all = adServiceFeign.findAll();
        List<TbAd> tbAds = all.getData();
        ResponseBase<List<Map>> categoryTree = categoryServiceFeign.findCategoryTree();
        List<Map> mapList = categoryTree.getData();

        model.addAttribute("ads",tbAds);
        model.addAttribute("categoryTree",mapList);
        return "index";
    }
}
