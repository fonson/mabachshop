package com.mabach.order.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.order.service.entity.TbOrder;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "所有订单详情接口")
public interface OrderService {

    @PostMapping("/findPageOrder")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbOrder>> findBrandBypage(@RequestParam("page") Integer page,
                                                             @RequestParam("size") Integer size,
                                                             @RequestBody TbOrder domain);
    @PostMapping("/addOrder")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbOrder domain);

    @PostMapping("/updateOrder")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbOrder domain);


    @GetMapping("/findByIdOrder")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbOrder> findById(@RequestParam("id") String id);

    @PostMapping("/deleteOrder")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") String id);

    @GetMapping("/findAllOrder")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbOrder>> findAll();

    @PostMapping("/deliverManyOrder")
    @ApiOperation(value = "批量发货")
    public ResponseBase deliverManyOrder(@RequestBody List<TbOrder> domain);



}
