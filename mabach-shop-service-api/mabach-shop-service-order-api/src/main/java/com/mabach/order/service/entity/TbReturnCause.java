package com.mabach.order.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
@Data
public class TbReturnCause implements Serializable {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
     * 原因
     */
    private String cause;

    /**
     * 排序
     */
    private Integer seq;

    /**
     * 是否启用
     */
    private String status;


}
