package com.mabach.order.service;

import com.mabach.common.outputDTO.PageResult;

import com.mabach.order.service.entity.TbReturnOrder;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "退款订单")
public interface ReturnOrderService {

    @PostMapping("/findPageReturnOrder")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbReturnOrder>> findBrandBypage(@RequestParam("page") Integer page,
                                                                   @RequestParam("size") Integer size,
                                                                   @RequestBody TbReturnOrder domain);
    @PostMapping("/addReturnOrder")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbReturnOrder domain);

    @PostMapping("/updateReturnOrder")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbReturnOrder domain);


    @GetMapping("/findByIdReturnOrder")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbReturnOrder> findById(@RequestParam("id") Long id);

    @PostMapping("/deleteReturnOrder")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") Long id);

    @GetMapping("/findAllReturnOrder")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbReturnOrder>> findAll();

}
