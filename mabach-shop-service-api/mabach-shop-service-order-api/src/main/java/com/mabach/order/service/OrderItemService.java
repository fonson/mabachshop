package com.mabach.order.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.order.service.entity.TbOrderItem;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "订单目录接口")
public interface OrderItemService {

    @PostMapping("/findPageOrderItem")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbOrderItem>> findBrandBypage(@RequestParam("page") Integer page,
                                                                 @RequestParam("size") Integer size,
                                                                 @RequestBody TbOrderItem domain);
    @PostMapping("/addOrderItem")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbOrderItem domain);

    @PostMapping("/updateOrderItem")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbOrderItem domain);


    @GetMapping("/findByIdOrderItem")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbOrderItem> findById(@RequestParam("id") Long id);

    @PostMapping("/deleteOrderItem")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") Long id);

    @GetMapping("/findAllOrderItem")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbOrderItem>> findAll();

}
