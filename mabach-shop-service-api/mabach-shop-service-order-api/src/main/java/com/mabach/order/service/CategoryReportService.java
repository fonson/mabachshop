package com.mabach.order.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.order.service.entity.TbCategoryReport;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;

@Api(tags = "报告接口")
public interface CategoryReportService {

    @PostMapping("/category1Count")
    @ApiOperation(value = "查询交易金额")
    public ResponseBase<List<TbCategoryReport>> find(@RequestParam("date1") LocalDate date1,
                                                                      @RequestParam("date2") LocalDate date2);

}
