package com.mabach.order.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mabach.core.helper.LongJsonDeserializer;
import com.mabach.core.helper.LongJsonSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
@Data
@TableName("tb_order_log")
public class TbOrderLog implements Serializable {
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    @TableId(value = "id",type = IdType.ID_WORKER)
    private Long id;
    /**
     * 操作员
     */
    private String operater;

    /**
     * 操作时间
     */
    private LocalDateTime operateTime;

    /**
     * 订单ID
     */
    private Long orderId;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 付款状态
     */
    private String payStatus;

    /**
     * 发货状态
     */
    private String consignStatus;

    /**
     * 备注
     */
    private String remarks;


}
