package com.mabach.order.service;

import com.mabach.common.outputDTO.PageResult;

import com.mabach.order.service.entity.TbOrderLog;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "运费接口")
public interface OrderLogService {

    @PostMapping("/findPageOrderLog")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbOrderLog>> findBrandBypage(@RequestParam("page") Integer page,
                                                                @RequestParam("size") Integer size,
                                                                @RequestBody TbOrderLog domain);
    @PostMapping("/addOrderLog")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbOrderLog domain);

    @PostMapping("/updateOrderLog")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbOrderLog domain);


    @GetMapping("/findByIdOrderLog")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbOrderLog> findById(@RequestParam("id") Long id);

    @PostMapping("/deleteOrderLog")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") Long id);

    @GetMapping("/findAllOrderLog")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbOrderLog>> findAll();

}
