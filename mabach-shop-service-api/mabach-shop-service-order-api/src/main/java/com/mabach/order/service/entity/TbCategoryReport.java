package com.mabach.order.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
@Data
public class TbCategoryReport implements Serializable {


    /**
     * 1级分类
     */
    @TableId(value = "category_id1",type = IdType.AUTO)
    private Integer categoryId1;

    /**
     * 2级分类
     */
    @TableId(value = "category_id2",type = IdType.AUTO)
    private Integer categoryId2;

    /**
     * 3级分类
     */
    @TableId(value = "category_id3",type = IdType.AUTO)
    private Integer categoryId3;

    /**
     * 统计日期
     */
    private LocalDate countDate;

    /**
     * 销售数量
     */
    private Integer num;

    /**
     * 销售额
     */
    private Integer money;


}
