package com.mabach.order.service;

import com.mabach.common.outputDTO.PageResult;

import com.mabach.order.service.entity.TbPreferential;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "优惠活动接口")
public interface PreferentialService {

    @PostMapping("/findPagePreferential")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbPreferential>> findBrandBypage(@RequestParam("page") Integer page,
                                                                    @RequestParam("size") Integer size,
                                                                    @RequestBody TbPreferential domain);
    @PostMapping("/addPreferential")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbPreferential domain);

    @PostMapping("/updatePreferential")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbPreferential domain);


    @GetMapping("/findByIdPreferential")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbPreferential> findById(@RequestParam("id") Integer id);

    @PostMapping("/deletePreferential")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") Integer id);

    @GetMapping("/findAllPreferential")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbPreferential>> findAll();

}
