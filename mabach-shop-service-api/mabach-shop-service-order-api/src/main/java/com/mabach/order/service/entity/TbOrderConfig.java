package com.mabach.order.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mabach.core.helper.LongJsonDeserializer;
import com.mabach.core.helper.LongJsonSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-27
 */
@Data
@TableName("tb_order_config")
public class TbOrderConfig implements Serializable {


    /**
     * ID
     */
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long id;

    /**
     * 正常订单超时时间（分）
     */
    private Integer orderTimeout;

    /**
     * 秒杀订单超时时间（分）
     */
    private Integer seckillTimeout;

    /**
     * 自动收货（天）
     */
    private Integer takeTimeout;

    /**
     * 售后期限
     */
    private Integer serviceTimeout;

    /**
     * 自动五星好评
     */
    private Integer commentTimeout;


}
