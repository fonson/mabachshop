package com.mabach.order.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.order.service.entity.TbOrderConfig;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;



@Api(tags = "订单信息接口")
public interface OrderConfigService {

    @PostMapping("/findPageOOrderConfig")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbOrderConfig>> findBrandBypage(@RequestParam("page") Integer page,
                                                                   @RequestParam("size") Integer size,
                                                                   @RequestBody TbOrderConfig domain);
    @PostMapping("/addOrderConfig")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbOrderConfig domain);

    @PostMapping("/updateOrderConfig")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbOrderConfig domain);


    @GetMapping("/findByIdOrderConfig")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbOrderConfig> findById(@RequestParam("id") Long id);

    @PostMapping("/deleteOrderConfig")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") Long id);

    @GetMapping("/findAllOrderConfig")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbOrderConfig>> findAll();

}
