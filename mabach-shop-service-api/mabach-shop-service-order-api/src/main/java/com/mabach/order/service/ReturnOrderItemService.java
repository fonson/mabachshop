package com.mabach.order.service;

import com.mabach.common.outputDTO.PageResult;

import com.mabach.order.service.entity.TbReturnOrderItem;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "退款订单列表")
public interface ReturnOrderItemService {

    @PostMapping("/findPageReturnOrderItem")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbReturnOrderItem>> findBrandBypage(@RequestParam("page") Integer page,
                                                                       @RequestParam("size") Integer size,
                                                                       @RequestBody TbReturnOrderItem domain);
    @PostMapping("/addReturnOrderItem")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbReturnOrderItem domain);

    @PostMapping("/updateReturnOrderItem")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbReturnOrderItem domain);


    @GetMapping("/findByIdReturnOrderItem")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbReturnOrderItem> findById(@RequestParam("id") Long id);

    @PostMapping("/deleteReturnOrderItem")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") Long id);

    @GetMapping("/findAllReturnOrderItem")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbReturnOrderItem>> findAll();

}
