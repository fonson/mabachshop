package com.mabach.order.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mabach.core.helper.LongJsonDeserializer;
import com.mabach.core.helper.LongJsonSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
@Data
public class TbReturnOrder implements Serializable {

    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    @TableId(value = "id",type = IdType.ID_WORKER)
    private Long id;
    /**
     * 订单号
     */
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long orderId;

    /**
     * 申请时间
     */
    private LocalDateTime applyTime;

    /**
     * 用户ID
     */
    @JsonSerialize(using = LongJsonSerializer.class)
    @JsonDeserialize(using = LongJsonDeserializer.class)
    private Long userId;

    /**
     * 用户账号
     */
    private String userAccount;

    /**
     * 联系人
     */
    private String linkman;

    /**
     * 联系人手机
     */
    private String linkmanMobile;

    /**
     * 类型
     */
    private String type;

    /**
     * 退款金额
     */
    private Integer returnMoney;

    /**
     * 是否退运费
     */
    private String isReturnFreight;

    /**
     * 申请状态
     */
    private String status;

    /**
     * 处理时间
     */
    private LocalDateTime disposeTime;

    /**
     * 退货退款原因
     */
    private Integer returnCause;

    /**
     * 凭证图片
     */
    private String evidence;

    /**
     * 问题描述
     */
    private String description;

    /**
     * 处理备注
     */
    private String remark;

    /**
     * 管理员id
     */
    private Integer adminId;


}
