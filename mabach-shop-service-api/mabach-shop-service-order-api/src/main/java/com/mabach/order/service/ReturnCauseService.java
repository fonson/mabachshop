package com.mabach.order.service;

import com.mabach.common.outputDTO.PageResult;

import com.mabach.order.service.entity.TbReturnCause;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "退款原因")
public interface ReturnCauseService {

    @PostMapping("/findPageReturnCause")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbReturnCause>> findBrandBypage(@RequestParam("page") Integer page,
                                                                   @RequestParam("size") Integer size,
                                                                   @RequestBody TbReturnCause domain);
    @PostMapping("/addReturnCause")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbReturnCause domain);

    @PostMapping("/updateReturnCause")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbReturnCause domain);


    @GetMapping("/findByIdReturnCause")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbReturnCause> findById(@RequestParam("id") Integer id);

    @PostMapping("/deleteReturnCause")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") Integer id);

    @GetMapping("/findAllReturnCause")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbReturnCause>> findAll();

}
