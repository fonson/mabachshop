package com.mabach.freight.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */
@Data
public class TbFreightTemplate implements Serializable {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
     * 模板名称
     */
    private String name;

    /**
     * 计费方式
     */
    private String type;


}
