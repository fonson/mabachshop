package com.mabach.freight.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.freight.service.entity.TbFreightTemplate;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "运费接口")
public interface FreightService {

    @PostMapping("/findPageFreightTemplate")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbFreightTemplate>> findBrandBypage(@RequestParam("page")  Integer page,
                                                                       @RequestParam("size") Integer size,
                                                                       @RequestBody TbFreightTemplate domain);
    @PostMapping("/addFreightTemplate")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbFreightTemplate domain);

    @PostMapping("/updateFreightTemplate")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbFreightTemplate domain);


    @GetMapping("/findByIdFreightTemplate")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbFreightTemplate> findById(@RequestParam("id")  Integer id);

    @PostMapping("/deleteFreightTemplate")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id")  Integer id);

    @GetMapping("/findAllFreightTemplate")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbFreightTemplate>> findAll();

}
