package com.mabach.goods.service.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TbGoods implements Serializable {
    private TbSpu tbSpu;
    private List<TbSku> tbSkuList;
}
