package com.mabach.goods.service;


import com.mabach.common.outputDTO.PageResult;

import com.mabach.goods.service.entity.TbBrand;
import com.mabach.responseBase.ResponseBase;
import com.mabach.responseBase.ResponseWeb;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(tags = "品牌服务接口")
public interface BrandService {

    @PostMapping("/findBrandBypage")
    @ApiOperation(value = "分页查询所有品牌")
    public ResponseBase<PageResult<TbBrand>> findBrandBypage(@RequestParam("page")  Integer page,
                                                             @RequestParam("size") Integer size,
                                                             @RequestBody TbBrand tbBrand);
    @PostMapping("/add")
    @ApiOperation(value = "添加品牌")
    public ResponseBase add(@RequestBody TbBrand tbBrand);

    @PostMapping("/update")
    @ApiOperation(value = "更改品牌")
    public ResponseBase update(@RequestBody TbBrand tbBrand);


    @GetMapping("/findById")
    @ApiOperation(value = "根据ID查找品牌")
    public ResponseBase<TbBrand> findById(@RequestParam("id")  Integer id);

    @PostMapping("/delete")
    @ApiOperation(value = "根据ID删除品牌")
    public ResponseBase delete(@RequestParam("id")  Integer id);

    @GetMapping("/findAllBrand")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbBrand>> findAllBrand();



}
