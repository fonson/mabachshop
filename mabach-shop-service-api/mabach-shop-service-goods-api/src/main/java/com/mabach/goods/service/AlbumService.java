package com.mabach.goods.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.service.entity.TbAlbum;
import com.mabach.goods.service.entity.TbBrand;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-11-23
 */
@Api(tags = "相册服务接口")
public interface AlbumService {

    @PostMapping("/findByPage")
    @ApiOperation(value = "分页查询")
    public ResponseBase<PageResult<TbAlbum>> findByPage(@RequestParam("page") Integer page, @RequestParam("size") Integer size,
                                                        @RequestBody TbAlbum tbAlbum);

    @PostMapping("/addTbAlbum")
    @ApiOperation(value = "添加相册")
    public ResponseBase add(@RequestBody TbAlbum tbAlbum);

    @PostMapping("/updateTbAlbum")
    @ApiOperation(value = "更改相册")
    public ResponseBase update(@RequestBody TbAlbum tbAlbum);

    @GetMapping("/findByIdTbAlbum")
    @ApiOperation(value = "根据id查找相册")
    public ResponseBase<TbAlbum>  findById(@RequestParam("id") Long id);

    @GetMapping("/deleteTbAlbum")
    @ApiOperation(value = "删除相册")
    public ResponseBase  delete(@RequestParam("id") Long  id);

    @GetMapping("/findAllTbAlbum")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbAlbum>>  findAllTbAlbum();

}


