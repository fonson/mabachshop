package com.mabach.goods.service;


import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.service.entity.TbAlbum;
import com.mabach.goods.service.entity.TbCategory;
import com.mabach.goods.service.entity.TbTemplate;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Api(tags = "品类服务接口")
public interface CategoryService {


    @PostMapping("/findListCategory")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbCategory>> findList(@RequestBody TbCategory tbCategory);

    @PostMapping("/addCategory")
    @ApiOperation(value = "增加品类")
    public ResponseBase add(@RequestBody TbCategory tbCategory);

    @PostMapping("/updateCategory")
    @ApiOperation(value = "更改品类")
    public ResponseBase update(@RequestBody TbCategory tbCategory);

    @GetMapping("/findByIdCategory")
    @ApiOperation(value = "根据id查找品类")
    public ResponseBase<TbCategory>  findById(@RequestParam("id")Integer id);

    @GetMapping("/deleteCategory")
    @ApiOperation(value = "删除品类")
    public ResponseBase  delete(@RequestParam("id")Integer id);

    @GetMapping("/findCategoryTree")
    @ApiOperation(value = "查询返回分类树")
    public ResponseBase<List<Map>> findCategoryTree();

}

