package com.mabach.goods.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */
@Data
public class TbPref implements Serializable {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    /**
     * 分类ID
     */
    private Integer cateId;

    /**
     * 消费金额
     */
    private Integer buyMoney;

    /**
     * 优惠金额
     */
    private Integer preMoney;

    /**
     * 活动开始日期
     */
    private LocalDate startTime;

    /**
     * 活动截至日期
     */
    private LocalDate endTime;

    /**
     * 类型
     */
    private String type;

    /**
     * 状态
     */
    private String state;


}
