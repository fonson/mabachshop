package com.mabach.goods.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-12-01
 */
@Data
public class TbCategoryBrand implements Serializable {


    /**
     * 分类ID
     */


    @TableId(value = "category_id",type = IdType.AUTO)
    private Integer categoryId;

    /**
     * 品牌ID
     */
    @TableId(value = "brand_id",type = IdType.AUTO)
    private Integer brandId;


}
