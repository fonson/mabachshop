package com.mabach.goods.service;

import com.mabach.common.outputDTO.PageResult;

import com.mabach.goods.service.entity.TbSku;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Api(tags = "库存管理接口")
public interface SkuService {
    @PostMapping("/findPageSku")
    @ApiOperation(value = "分页查询")
    public ResponseBase<PageResult<TbSku>> findPage(@RequestParam("page")  Integer page,
                                                    @RequestParam("size") Integer size,
                                                    @RequestBody TbSku tbPref);
    @PostMapping("/addSku")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbSku tbPref);

    @PostMapping("/updateSku")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbSku tbPref);


    @GetMapping("/findByIdSku")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbSku> findById(@RequestParam("id")  String id);

    @PostMapping("/deleteSku")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id")  String id);



}
