package com.mabach.goods.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.service.entity.TbGoods;
import com.mabach.goods.service.entity.TbSpu;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "产品信息接口")
public interface SpuService {
    @PostMapping("/findPageSpu")
    @ApiOperation(value = "分页查询")
    public ResponseBase<PageResult<TbSpu>> findPage(@RequestParam("page")  Integer page,
                                                    @RequestParam("size") Integer size,
                                                    @RequestBody TbSpu tbPref);
    @PostMapping("/addSpu")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbSpu tbPref);

    @PostMapping("/updateSpu")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbSpu tbPref);


    @GetMapping("/findByIdSpu")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbSpu> findById(@RequestParam("id")  String id);

    @PostMapping("/deleteSpu")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id")  String id);

    @PostMapping("/saveGoods")
    @ApiOperation(value = "保存商品")
    public ResponseBase save(@RequestBody TbGoods tbGoods);

    @GetMapping("/findGoodsByIdSpu")
    @ApiOperation(value = "根据spuID查找商品")
    public ResponseBase<TbGoods> findGoods(@RequestParam("id")  String id);

    @PostMapping("/examineManySpu")
    @ApiOperation(value = "批量审核")
    public ResponseBase<Integer> examineManySpu(@RequestBody List<String> ids);

    @PostMapping("/putManySpu")
    @ApiOperation(value = "批量上架")
    public ResponseBase<Integer> putManySpu(@RequestBody List<String> ids);

    @PostMapping("/pullManySpu")
    @ApiOperation(value = "批量下架")
    public ResponseBase<Integer> pullManySpu(@RequestBody List<String> ids);

}
