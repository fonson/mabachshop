package com.mabach.goods.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.service.entity.TbAlbum;
import com.mabach.goods.service.entity.TbTemplate;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "模板接口")
public interface TemplateService {
    @GetMapping("/findAllTemplate")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbTemplate>> findAll();

    @PostMapping("/findPageTemplate")
    @ApiOperation(value = "查询所有")
    public ResponseBase<PageResult<TbTemplate>> findPageTemplate(@RequestParam("page")  Integer page,
                                                           @RequestParam("size") Integer size,
                                                           @RequestBody TbTemplate tbTemplate);

    @PostMapping("/addTemplate")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbTemplate tbTemplate);

    @PostMapping("/updateTemplate")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbTemplate tbTemplate);


    @GetMapping("/findByIdTemplate")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbTemplate> findById(@RequestParam("id")  Integer id);

    @PostMapping("/deleteTemplate")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id")  Integer id);
}
