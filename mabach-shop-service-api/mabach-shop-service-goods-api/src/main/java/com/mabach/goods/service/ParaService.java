package com.mabach.goods.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.service.entity.TbPara;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "参数服务接口")
public interface ParaService {
    @PostMapping("/findPagePara")
    @ApiOperation(value = "分页查询")
    public ResponseBase<PageResult<TbPara>> findPage(@RequestParam("page") Integer page, @RequestParam("size") Integer size,
                                                       @RequestBody TbPara tbPara);

    @PostMapping("/addPara")
    @ApiOperation(value = "添加参数")
    public ResponseBase add(@RequestBody TbPara tbPara);

    @PostMapping("/updatePara")
    @ApiOperation(value = "更改参数")
    public ResponseBase update(@RequestBody TbPara tbPara);

    @GetMapping("/findByIdPara")
    @ApiOperation(value = "根据id查找相册")
    public ResponseBase<TbPara>  findById(@RequestParam("id") Integer id);

    @GetMapping("/deletePara")
    @ApiOperation(value = "删除参数")
    public ResponseBase  delete(@RequestParam("id") Integer id);

    @GetMapping("/findAllPara")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbPara>>  findAllPara();

}
