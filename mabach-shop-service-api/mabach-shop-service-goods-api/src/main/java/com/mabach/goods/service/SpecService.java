package com.mabach.goods.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.service.entity.TbSpec;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "规格接口")
public interface SpecService {
    @PostMapping("/findPageSpec")
    @ApiOperation(value = "分页查询")
    public ResponseBase<PageResult<TbSpec>> findPage(@RequestParam("page")  Integer page,
                                                     @RequestParam("size") Integer size,
                                                     @RequestBody TbSpec tbPref);
    @PostMapping("/addSpec")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbSpec tbPref);

    @PostMapping("/updateSpec")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbSpec tbPref);


    @GetMapping("/findByIdSpec")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbSpec> findById(@RequestParam("id")  Integer id);

    @PostMapping("/deleteSpec")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id")  Integer id);

    @PostMapping("/findListSpec")
    @ApiOperation(value = "条件查询")
    public ResponseBase<List<TbSpec>> findList(@RequestBody  TbSpec tbSpec);
}
