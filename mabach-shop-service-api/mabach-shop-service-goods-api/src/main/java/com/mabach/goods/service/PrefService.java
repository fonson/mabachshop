package com.mabach.goods.service;


import com.mabach.common.outputDTO.PageResult;
import com.mabach.goods.service.entity.TbPref;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Api(tags = "活动管理接口")
public interface PrefService {

    @PostMapping("/findPagePref")
    @ApiOperation(value = "分页查询")
    public ResponseBase<PageResult<TbPref>> findPage(@RequestParam("page")  Integer page,
                                                            @RequestParam("size") Integer size,
                                                            @RequestBody TbPref tbPref);
    @PostMapping("/addPref")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbPref tbPref);

    @PostMapping("/updatePref")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbPref tbPref);


    @GetMapping("/findByIdPref")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbPref> findById(@RequestParam("id")  Integer id);

    @PostMapping("/deletePref")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id")  Integer id);


}
