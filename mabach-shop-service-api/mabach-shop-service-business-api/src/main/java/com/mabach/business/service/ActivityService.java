package com.mabach.business.service;

import com.mabach.business.service.entity.TbActivity;
import com.mabach.common.outputDTO.PageResult;

import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api(tags = "文章接口")
public interface ActivityService {

    @PostMapping("/findPageActivi")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbActivity>> findBrandBypage(@RequestParam("page") Integer page,
                                                                @RequestParam("size") Integer size,
                                                                @RequestBody TbActivity domain);
    @PostMapping("/addActivi")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbActivity domain);

    @PostMapping("/updateActivi")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbActivity domain);


    @GetMapping("/findByIdActivi")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbActivity> findById(@RequestParam("id") Integer id);

    @PostMapping("/deleteActivi")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") Integer id);

    @GetMapping("/findAllActivi")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbActivity>> findAll();

}
