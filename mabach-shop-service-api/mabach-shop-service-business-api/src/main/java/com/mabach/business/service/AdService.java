package com.mabach.business.service;

import com.mabach.business.service.entity.TbAd;
import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Api(tags = "广告接口")
public interface AdService {

    @PostMapping("/findPageAd")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbAd>> findBrandBypage(@RequestParam("page") Integer page,
                                                          @RequestParam("size") Integer size,
                                                          @RequestBody TbAd domain);
    @PostMapping("/addAd")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbAd domain);

    @PostMapping("/updateAd")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbAd domain);


    @GetMapping("/findByIdAd")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbAd> findById(@RequestParam("id") Integer id);

    @PostMapping("/deleteAd")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") Integer id);

    @GetMapping("/findAllAd")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbAd>> findAll();

}
