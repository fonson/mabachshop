package com.mabach.business.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
@Data
public class TbAd implements Serializable {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
     * 广告名称
     */
    private String name;

    /**
     * 广告位置
     */
    private String position;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;

    /**
     * 到期时间
     */
    private LocalDateTime endTime;

    /**
     * 状态
     */
    private String status;

    /**
     * 图片地址
     */
    private String image;

    /**
     * URL
     */
    private String url;

    /**
     * 备注
     */
    private String remarks;


}
