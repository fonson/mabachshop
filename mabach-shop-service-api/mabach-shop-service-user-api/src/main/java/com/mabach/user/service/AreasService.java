package com.mabach.user.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.user.service.entity.TbAreas;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
@Api(tags = "用户区域接口")
public interface AreasService {
    @PostMapping("/findPageAreas")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbAreas>> findBrandBypage(@RequestParam("page")  Integer page,
                                                             @RequestParam("size") Integer size,
                                                             @RequestBody TbAreas domain);
    @PostMapping("/addAreas")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbAreas domain);

    @PostMapping("/updateAreas")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbAreas domain);


    @GetMapping("/findByIdAreas")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbAreas> findById(@RequestParam("areaid")  Integer areaid);

    @PostMapping("/deleteAreas")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("areaid")  Integer areaid);

    @GetMapping("/findAllAreas")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbAreas>> findAll();
}
