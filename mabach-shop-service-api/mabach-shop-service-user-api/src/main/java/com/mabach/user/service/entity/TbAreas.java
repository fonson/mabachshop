package com.mabach.user.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 行政区域县区信息表
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */
@Data
public class TbAreas implements Serializable {


    /**
     * 区域ID
     */

    @TableId(value = "areaid",type = IdType.AUTO)
    private Integer areaid;

    /**
     * 区域名称
     */
    private String area;

    /**
     * 城市ID
     */
    private Integer cityid;


}
