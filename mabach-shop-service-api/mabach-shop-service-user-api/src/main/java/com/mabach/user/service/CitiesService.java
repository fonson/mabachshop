package com.mabach.user.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.user.service.entity.TbCities;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Api(tags = "用户城市接口")
public interface CitiesService {
    @PostMapping("/findPageCities")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbCities>> findBrandBypage(@RequestParam("page")  Integer page,
                                                              @RequestParam("size") Integer size,
                                                              @RequestBody TbCities domain);
    @PostMapping("/addCities")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbCities domain);

    @PostMapping("/updateCities")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbCities domain);


    @GetMapping("/findByIdCities")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbCities> findById(@RequestParam("cityid")  Integer cityid);

    @PostMapping("/deleteCities")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("cityid")  Integer cityid);

    @GetMapping("/findAllCities")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbCities>> findAll();
}