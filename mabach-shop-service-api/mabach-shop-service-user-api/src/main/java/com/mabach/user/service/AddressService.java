package com.mabach.user.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.user.service.entity.TbAddress;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Api(tags = "用户地址接口")
public interface AddressService {

    @PostMapping("/findPageAddress")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbAddress>> findBrandBypage(@RequestParam("page")  Integer page,
                                                               @RequestParam("size") Integer size,
                                                               @RequestBody TbAddress domain);
    @PostMapping("/addAddress")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbAddress domain);

    @PostMapping("/updateAddress")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbAddress domain);


    @GetMapping("/findByIdAddress")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbAddress> findById(@RequestParam("id")  Integer id);

    @PostMapping("/deleteAddress")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id")  Integer id);

    @GetMapping("/findAllAddress")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbAddress>> findAll();

}