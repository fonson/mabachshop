package com.mabach.user.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 省份信息表
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */
@Data
public class TbProvinces implements Serializable {



    /**
     * 省份ID
     */
    @TableId(value = "provinceid",type = IdType.AUTO)
    private Integer provinceid;

    /**
     * 省份名称
     */
    private String province;


}
