package com.mabach.user.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 行政区域地州市信息表
 * </p>
 *
 * @author jobob
 * @since 2019-11-25
 */
@Data
public class TbCities implements Serializable {


    /**
     * 城市ID
     */
    @TableId(value = "cityid",type = IdType.AUTO)
    private Integer cityid;

    /**
     * 城市名称
     */
    private String city;

    /**
     * 省份ID
     */
    private Integer provinceid;


}
