package com.mabach.user.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.user.service.entity.TbProvinces;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Api(tags = "用户省份接口")
public interface ProvinceService {
    @PostMapping("/findPageProvince")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbProvinces>> findBrandBypage(@RequestParam("page")  Integer page,
                                                                 @RequestParam("size") Integer size,
                                                                 @RequestBody TbProvinces domain);
    @PostMapping("/addProvince")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbProvinces domain);

    @PostMapping("/updateProvince")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbProvinces domain);


    @GetMapping("/findByIdProvince")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbProvinces> findById(@RequestParam("provinceid")  Integer provinceid);

    @PostMapping("/deleteProvince")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("provinceid")  Integer provinceid);

    @GetMapping("/findAllProvince")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbProvinces>> findAll();
}
