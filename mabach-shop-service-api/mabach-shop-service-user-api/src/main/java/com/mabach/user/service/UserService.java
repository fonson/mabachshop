package com.mabach.user.service;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.user.service.entity.TbUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Api(tags = "用户信息接口")
public interface UserService {
    @PostMapping("/findPageUser")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbUser>> findBrandBypage(@RequestParam("page")  Integer page,
                                                            @RequestParam("size") Integer size,
                                                            @RequestBody TbUser domain);
    @PostMapping("/addUser")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbUser domain);

    @PostMapping("/updateUser")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbUser domain);


    @GetMapping("/findByIdUser")
    @ApiOperation(value = "根据用户名查找")
    public ResponseBase<TbUser> findById(@RequestParam("username")  String username);

    @PostMapping("/deleteUser")
    @ApiOperation(value = "根据用户名删除")
    public ResponseBase delete(@RequestParam("username")  String username);

    @GetMapping("/findAllUser")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbUser>> findAll();
}