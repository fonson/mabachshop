package com.mabach.service.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
@Data
public class TbLoginLog implements Serializable {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    private String loginName;

    private String ip;

    private String browserName;

    /**
     * 地区
     */
    private String location;

    /**
     * 登录时间
     */
    private LocalDateTime loginTime;


}
