package com.mabach.service.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2019-11-26
 */
@Data
public class TbRole implements Serializable {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
     * 角色名称
     */
    private String name;


}
