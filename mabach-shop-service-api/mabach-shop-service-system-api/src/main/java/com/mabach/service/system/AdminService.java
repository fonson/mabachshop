package com.mabach.service.system;


import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.service.system.entity.TbAdmin;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Api(tags = "管理员接口")
public interface AdminService {

    @PostMapping("/findPageAdmin")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbAdmin>> findBrandBypage(@RequestParam("page") Integer page,
                                                             @RequestParam("size") Integer size,
                                                             @RequestBody TbAdmin domain);
    @PostMapping("/addAdmin")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbAdmin domain);

    @PostMapping("/updateAdmin")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbAdmin domain);


    @GetMapping("/findByIdAdmin")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbAdmin> findById(@RequestParam("id") Integer id);

    @PostMapping("/deleteAdmin")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") Integer id);

    @GetMapping("/findAllAdmin")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbAdmin>> findAll();

}
