package com.mabach.service.system;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.service.system.entity.TbLoginLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Api(tags = "管理登录日志色接口")
public interface LoginLogService {

    @PostMapping("/findPageLoginLog")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbLoginLog>> findBrandBypage(@RequestParam("page") Integer page,
                                                                @RequestParam("size") Integer size);

}
