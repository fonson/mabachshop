package com.mabach.service.system;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.service.system.entity.TbRole;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Api(tags = "角色接口")
public interface RoleService {

    @PostMapping("/findPageRole")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbRole>> findBrandBypage(@RequestParam("page") Integer page,
                                                            @RequestParam("size") Integer size,
                                                            @RequestBody TbRole domain);
    @PostMapping("/addRole")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbRole domain);

    @PostMapping("/updateRole")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbRole domain);


    @GetMapping("/findByIdRole")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbRole> findById(@RequestParam("id") Integer id);

    @PostMapping("/deleteRole")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") Integer id);

    @GetMapping("/findAllRole")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbRole>> findAll();

}
