package com.mabach.service.system;

import com.mabach.common.outputDTO.PageResult;
import com.mabach.responseBase.ResponseBase;
import com.mabach.service.system.entity.TbMenu;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;


@Api(tags = "管理菜单接口")
public interface MenuService {

    @PostMapping("/findPageMenu")
    @ApiOperation(value = "分页查询所有")
    public ResponseBase<PageResult<TbMenu>> findBrandBypage(@RequestParam("page") Integer page,
                                                            @RequestParam("size") Integer size,
                                                            @RequestBody TbMenu domain);
    @PostMapping("/addMenu")
    @ApiOperation(value = "添加")
    public ResponseBase add(@RequestBody TbMenu domain);

    @PostMapping("/updateMenu")
    @ApiOperation(value = "更改")
    public ResponseBase update(@RequestBody TbMenu domain);


    @GetMapping("/findByIdMenu")
    @ApiOperation(value = "根据ID查找")
    public ResponseBase<TbMenu> findById(@RequestParam("id") String id);

    @PostMapping("/deleteMenu")
    @ApiOperation(value = "根据ID删除")
    public ResponseBase delete(@RequestParam("id") String id);

    @GetMapping("/findAllMenu")
    @ApiOperation(value = "查询所有")
    public ResponseBase<List<TbMenu>> findAll();

    @GetMapping("findMenu")
    @ApiOperation(value = "查询菜单")
    public ResponseBase<List<Map>> findMenu(@RequestParam("parentId") String parentId);

}