package com.mabach.member.service;

import com.mabach.responseBase.ResponseBase;
import com.mabach.member.inputDTO.UserRegisterInputDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "会员注册接口")
public interface MemberRegisterService {
	/**
	 * 用户注册接口
	 *
	 */
	@PostMapping("/register")
	@ApiOperation(value = "会员用户注册信息接口")
	ResponseBase<JSONObject> register(@RequestBody UserRegisterInputDTO user,
									  @RequestParam("registCode") String registCode);

}
