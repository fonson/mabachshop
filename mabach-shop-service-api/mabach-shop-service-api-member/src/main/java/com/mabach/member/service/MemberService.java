package com.mabach.member.service;

import com.mabach.member.outputDTO.UserOutputDTO;
import com.mabach.responseBase.ResponseBase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Api(tags = "判断会员是否存在接口")
public interface MemberService {

    @ApiOperation(value = "根据手机号判断用户是否存在")
    @PostMapping("/memberIsExist")
    boolean isExist(@RequestParam("phone") String phone);


    @ApiOperation(value = "根据手机号查询用户信息")
    @PostMapping("/findByPhone")
    ResponseBase<UserOutputDTO> findByPhone(@RequestParam("phone") String phone);

    @ApiOperation(value = "根据用户名查询用户信息")
    @PostMapping("/findByUserName")
    ResponseBase<UserOutputDTO> findByUserName(@RequestParam("userName") String userName);

    @ApiOperation(value = "根据用户名查询用户信息")
    @PostMapping("/findBytoken")
    ResponseBase<UserOutputDTO> findBytoken(@RequestParam("token") String token);

}
