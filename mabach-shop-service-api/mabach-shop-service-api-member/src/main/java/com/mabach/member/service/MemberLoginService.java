package com.mabach.member.service;

import com.mabach.member.inputDTO.UserLoginInputDTO;
import com.mabach.responseBase.ResponseBase;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.alibaba.fastjson.JSONObject;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**

 * @description:用户登陆接口服务

 */
@Api(tags = "用户登陆接口")
public interface MemberLoginService {
	/**
	 * 用户登陆接口
	 *
	 */
	@PostMapping("/login")
	@ApiOperation(value = "会员用户登陆信息接口")
	ResponseBase<JSONObject> login(@RequestBody UserLoginInputDTO userLoginInputDTO);

}
