# 分布式高并发电商曼巴商城

#### 介绍
曼巴商城基于Springboot2.x+SpringCloud+Mybatis,thymeleaf+bootstrap+Swagger2 B2c商城+微信商城+综合管理后台+APP平台+第三方平台 = 综合一体化平台.前后端分离,使用阿波罗配置中心，分布式敏捷开发系统架构，提供整套公共微服务服务模块：内容管理、支付中心、用户管理（包括第三方）、微信平台、存储系统、配置中心、日志分析、任务和通知等，支持服务治理、监控和追踪.

#### 软件架构
springCloud爱好者，前端web静态页面取自京东；目前只搭建了基础模块，功能逐渐完善中....

![输入图片说明](https://images.gitee.com/uploads/images/2019/1207/012334_4d0a599d_5045493.jpeg "项目架构流程.jpg")













####项目截图

![输入图片说明](https://images.gitee.com/uploads/images/2019/1207/013516_5c3ccb94_5045493.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1207/013527_2997775e_5045493.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1207/013544_e835a575_5045493.png "3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1207/013601_bd982d97_5045493.png "5.png")














####后台截图


![输入图片说明](https://images.gitee.com/uploads/images/2019/1207/013609_2fc7344b_5045493.png "6.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1207/013618_d31804bb_5045493.png "7.png")
#### 功能介绍

    统一认证功能
        支持oauth2的四种模式登录
        支持用户名、密码加图形验证码登录
        支持手机号加密码登录
        支持openId登录
        支持第三方系统单点登录

    分布式系统基础支撑
        服务注册发现、路由与负载均衡
        服务降级与熔断
        服务限流(url/方法级别)
        统一配置中心
        统一日志中心
        统一分布式缓存操作类、cacheManager配置扩展
        分布式锁
        分布式任务调度器
        支持CI/CD持续集成(包括前端和后端)
        分布式高性能Id生成器
        分布式事务

    系统监控功能
        服务调用链监控
        应用拓扑图
        慢服务检测
        服务Metric监控
        应用监控(应用健康、JVM、内存、线程)
        错误日志查询
        慢查询SQL监控
        应用吞吐量监控(qps、rt)
        服务降级、熔断监控
        服务限流监控
        分库分表、读写分离

    业务基础功能支撑
        高性能方法级幂等性支持
        RBAC权限管理，实现细粒度控制(方法、url级别)
        快速实现导入、导出功能
        数据库访问层自动实现crud操作
        代码生成器
        基于Hutool的各种便利开发工具
        网关聚合所有服务的Swagger接口文档
        统一跨域处理
        统一异常处理

 

#### 技术选型

1.  xxxx
2.  xxxx
3.  xxxx

#### 模块说明




mabach-shop-parent-- 父项目，公共依赖

─mabach-shop-api-DTO 

─mabach-shop-basics -- 通用工具一级工程

- ----mabach-shop-basics-springcloud-eureka -- eureka注册中心
- ----mabach-shop-basics-springcloud-zuul -- zuul网关集成Swagger统一管理

  
─mabach-shop-common -- 公共模块
 
─mabach-shop-XXLjob -- 分布式任务调度
    
─mabach-shop-txlcn -- 分布式事务管理
    
─mabach-shop-XXLjob -- ELK分布式日志采集中心
   
─mabach-shop-service-api--api提供服务接口
- ----mabach-shop-service-api-member -- 会员接口
- ----mmabach-shop-service-api-weixin -- 微信接口
- ----mabach-shop-service-business-api -- 商城业务接口
- ----mabach-shop-service-freight-api -- 商城物流模块接口
- ----mabach-shop-service-goods-api -- 商城商品模块接口
- ----mabach-shop-service-order-apil -- 商城订单接口
- ----mabach-shop-service-system-api -- 系统服务接口
- ----mabach-shop-service-user-api -- 商城用户接口


─mabach-shop-service-impl -- 商城服务
 
─mabach-shop-web -- 商城web工程

- ----mabach-shop-manager-web -- 后台web管理集成shiro+RBAC权限控制
- ----mabach-shop-portal-web -- 前台web工程
- 





继续完善中........
#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
