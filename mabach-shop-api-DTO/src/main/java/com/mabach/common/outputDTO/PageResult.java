package com.mabach.common.outputDTO;

import lombok.Data;

import java.util.List;
@Data
public class PageResult<T> {
    private Long total;//返回记录数
    private List<T> rows;//结果

    public PageResult() {
    }

    public PageResult(Long total, List<T> rows) {
        this.total = total;
        this.rows = rows;
    }
}
