package com.mabach.goods.outputDTO;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "分页返回实体类")
public class PageOutputDTO {
    private Long total;
    private List<TbBrandOutputDTO> rows;

    public PageOutputDTO(Long total, List<TbBrandOutputDTO> rows) {
        this.total = total;
        this.rows = rows;
    }
}
