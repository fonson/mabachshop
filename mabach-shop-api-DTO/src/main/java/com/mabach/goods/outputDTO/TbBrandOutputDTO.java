package com.mabach.goods.outputDTO;


import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 品牌表
 * </p>
 *
 * @author jobob
 * @since 2019-11-21
 */
@Data
public class TbBrandOutputDTO implements Serializable {


    /**
     * 品牌id
     */

    private Integer id;

    /**
     * 品牌名称
     */
    private String name;

    /**
     * 品牌图片地址
     */
    private String image;

    /**
     * 品牌的首字母
     */
    private String letter;

    /**
     * 排序
     */
    private Integer seq;


}
